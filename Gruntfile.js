"use strict";

module.exports = function(grunt) {

	var pngquant = require('imagemin-pngquant'),
		jpegRecompress = require('imagemin-jpeg-recompress');

	var appConfig = {
		app: 'app',
		dist: 'dist',
		packagesJS:  [
			'app/bower_components/modernizr/modernizr.js',
			'app/bower_components/jquery-address/src/jquery.address.js',
			'app/bower_components/underscore/underscore-min.js',
			// 'app/bower_components/greensock/src/minified/TweenMax.min.js',
			// 'app/js/lib/gs/plugins/ThrowPropsPlugin.min.js',
			// 'app/js/lib/gs/utils/Draggable.min.js',
			// 'app/bower_components/slick-carousel/slick/slick.min.js',
			// 'app/bower_components/iscroll/build/iscroll.js',
			'app/bower_components/PxLoader/PxLoader.js',
			'app/bower_components/PxLoader/PxLoaderImage.js',
			'app/bower_components/PxLoader/PxLoaderVideo.js',
			'app/bower_components/SoundJS/lib/soundjs-0.6.1.min.js'
		]
	};

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			scripts: {
				files: [
					appConfig.app + '/js/lib/**/*.js',
					appConfig.app + '/js/app/**/*.js',
				],
				tasks: ['uglify:dev']
			},
			devPackages: {
				files: [ appConfig.packagesJS],
				tasks: ['uglify:packages']
			},
			css: {
				files: [appConfig.app + '/css/scss/**/*.scss'],
				tasks: ['compass:dev']
			},
			options: {
				atBegin: true
			}
		},
		copy: {
			dist: {
				files: [
					{expand: true, cwd: appConfig.app + '/', src: ['./js/settings*.js', './json/*.json', './*.php', './*.html', './*.png'], dest: appConfig.dist + '/', filter: 'isFile'},
					{expand: true, cwd: appConfig.app, src: ['audio/**', 'videos/**', 'fonts/**', 'includes/**', 'favicons/**', 'sounds/**'], dest: appConfig.dist + '/'} // includes files in path and its subdirs
			   ]
			}
		},
		postcss: {
		   options: {
		     map: {
		         inline: false, // save all sourcemaps as separate files...
		         annotation: appConfig.dist + '/css/maps/' // ...to the specified directory
		     }/*,
		     processors: [
		       require('autoprefixer')(),
		     ]*/
		   },
		   dist: {
		     src: 'css/*.css'
		   }
		 },
		uglify: {
			dev: {
				options: {
					beautify: true,
					sourceMap: true,
					sourceMapRoot: appConfig.app,
					preserveComments: 'all'
				},

				files: {
					'app/js/main.js': [ 'app/js/app/**/*.js', 'app/js/lib/**/*.js'  ]
				}
			},
			packages: {
				options: {
					beautify: true,
					sourceMap: true,
					sourceMapRoot: appConfig.app,
					preserveComments: 'all'

				},

				files: {
					'app/js/packages.js': appConfig.packagesJS,
				}
			},
			dist: {
				options: {
					sourceMap: false,
					compress: true,
					preserveComments: false,
					drop_console: true
				},

				files: {
					'dist/js/main.js': [ 'app/js/app/**/*.js', 'app/js/lib/**/*.js' ],
					'dist/js/packages.js': appConfig.packagesJS,
					'dist/js/jquery.min.js': 'app/js/jquery.min.js'
				}
			}
		},
		processhtml: {
			options: {
				// Task-specific options go here.
			},
			dist: {
				files: {
					'dist/index.html': ['app/index.html']
				}
			}
		},
		imagemin: {
			dynamic: {
				options: {                       // Target options
					optimizationLevel: 0,
					use: [
							pngquant()
						]
				},
				files: [{
					expand: true,                  // Enable dynamic expansion
					cwd: 'app/images/',            // Src matches are relative to this path
					src: ['**/*.{png,jpg,gif,JPG}'],   // Actual patterns to match
					dest: 'dist/images/'           // Destination path prefix
				},
				{
					expand: true,                  // Enable dynamic expansion
					cwd: 'app/models/',            // Src matches are relative to this path
					src: ['**/*.{png,jpg,gif,JPG}'],   // Actual patterns to match
					dest: 'dist/models/'           // Destination path prefix
				}]
			}
		},
		compass: {                  // Task
			dist: {                   // Target
				options: {              // Target options
					sassDir: appConfig.app + '/css/scss',
					cssDir: appConfig.dist + '/css',
					outputStyle: 'compressed'
				}
			},
			dev: {                // Target
				options: {              // Target options
					sassDir: appConfig.app + '/css/scss',
					cssDir: appConfig.app + '/css'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.loadNpmTasks('grunt-concat-sourcemap');

	grunt.registerTask('build', ['imagemin', 'compass:dist', 'uglify:dist', 'copy:dist']);

};
