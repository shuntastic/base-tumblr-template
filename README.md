This project was built using Node JS in a Ruby environment.

To make edits to any CSS or Javascript OTHER than js/settings.js, it requires Ruby, NodeJS, RequireJS, Grunt, Compass & Bower all be set up properly to execute development.

Once the environment is setup, run npm install to download the Node packages & bower install to download the js packages for the site development.

grunt watch can be used to work on a development version of the site in the /app folder.
grunt build can be used to publish the endpoint files in the /dist folder.

index.html is the main theme file for this project.
phone.html should be added as a "CUSTOM LAYOUT" page to the theme under the path /iphone-theme. Ensure that "Use default mobile theme" is enabled to use this mobile layout under the advanced theme customization options.

Once the main theme is installed, edit the "Asset URL" in the Theme Options to point to your asset location. This path should end in a '/'

js/settings.js contains most of the localization options, though a few such as the site meta data are in the Tumblr custom Theme Options.