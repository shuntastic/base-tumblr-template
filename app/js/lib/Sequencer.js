;(function (root, factory) {
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'utils/Tracer'
			],
			function () {
				var o = factory($);
				if (!window.sequencer)
					window.sequencer = o;
				return (root.utils.sequencer = o);
			}
		);
	} else {
		var o = factory($);
		if (!window.sequencer)
			window.sequencer = o;
		root.utils.sequencer = o;
	}
}(window.AJS9 = window.AJS9 || {}, function () {
	var tasks = [],
		delay = 60;

	function Sequencer() {
    }

	function add(arr, autorun)
	{
		var tasksL = tasks.length;
		var i = arr.length;
		tracer.disable('SEQUENCER');
		tracer.trace({ scope: "SEQUENCER", args: "adding "+i+" items." });
		while (i)
		{
			i--;
			tasks.unshift(arr[i]);
		}
		if (typeof autorun !== undefined || autorun)
			executeNextStep();
	}
	function executeNextStep()
	{
		if (tasks.length === 0)
        {
            tracer.trace({ scope: "SEQUENCER", args: "end of list." });
			return;
        }
		var step = tasks.shift();
		if (step === null)
        {
            tracer.trace({ scope: "SEQUENCER", args: "Next step is NULL." });
			return;
        }

        if (typeof step.id !=='undefined')
            tracer.trace({ scope: "SEQUENCER", args: "executing next item.  ID: "+step.id});
        else if ( typeof step.func.name !== 'undefined')
            tracer.trace({ scope: "SEQUENCER", args: "executing next item. No ID. "+step.func.name});
		else
            tracer.trace({ scope: "SEQUENCER", args: "executing next item. No ID. Function name undefined"});


		var func = step.func;
		if (!func)
			tracer.trace({ scope: "SEQUENCER", args: "Error: function does not exist"});
		if (typeof step.vars !== 'undefined')
			func(step.vars);
		else
			func();
	}

	function next(delay)
	{
		if (tasks.length)
			setTimeout(executeNextStep, 60);
	}

	function clear()
	{
		tasks = [];
	}

	function setDelay(value)
	{
		delay = value;
	}

	Sequencer.prototype.add = add;
	Sequencer.prototype.next = next;
	Sequencer.prototype.clear = clear;
	Sequencer.prototype.executeNextStep = executeNextStep;
	Sequencer.prototype.setDelay = setDelay;
	return new Sequencer();

}));
