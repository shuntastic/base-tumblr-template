/*jslint browser:true */
/*globals define, $, _ */
;(function (root, factory) {
	"use strict";
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'jquery',
				'underscore'
			],
			function () {
				var o = factory($);
				if (!window.tracer) {
					window.tracer = o;
				}
				return (root.utils.tracer = o);
			}
		);
	} else {
		var o = factory($);
		if (!window.tracer) {
			window.tracer = o;
		}
		root.utils.tracer = o;
	}
}(window.AJS9 = window.AJS9 || {}, function () {
	var
		CAN_TRACE = 0,
		CANNOT_TRACE = 1,
		LIMITED_TRACE = 2,
		DEFAULT_SCOPE = 'tracerDefaultScope',

		enabled = true,
		scopes = {},
		inited = false,
		capabilities = CAN_TRACE;

	function Tracer () {
		inited = true;
		switch (true)
		{
			case !window.console:
			case typeof window.console === 'undefined':
			case !window.console.log:
				capabilities = CANNOT_TRACE;
				break;

			case typeof console.log === 'object':
				capabilities = LIMITED_TRACE;
				break;
			default:
				capabilities = CAN_TRACE;
		}
	}

	function limitCapabilities (value) {
		capabilities = value;
	}

	function traceScope(scope) {
		if (!enabled) {
			return;
		}
		trace({ scope: scope, args: _.without(arguments, 0) });
	}

	function trace() {
		if (!enabled) {
			return;
		}

		if (arguments.length === 0) {
			trace("TRACER: Trace called with no arguments.");
		}

		var args = arguments,
			firstArg = args[0],
			scope = DEFAULT_SCOPE;

		if (_.isObject(firstArg) && _.has(firstArg, 'scope') && _.has(firstArg, 'args') && _.isString(firstArg.scope)) {
			scope = firstArg.scope;
			args = ([scope + " >> "]).concat(firstArg.args);

			if (!checkScope(scope)) {
				return;
			}

		}

		switch (capabilities)
		{
			case CAN_TRACE:
				console.log.apply(console, args);
				break;
			case CANNOT_TRACE:
				break;
			case LIMITED_TRACE:
				console.log('--');
				_.each(args, function (o) {
					console.log(o);
				});
				console.log('--');
				break;
		}
	}

	function checkScope(scope) {
		if (_.has(scopes, scope)) {
			return scopes[scope];
		} else {
			scopes[scope] = true;
			return true;
		}
	}

	function enable() {
		if (arguments.length === 0) {
			enabled = true;
		} else if (_.isString(arguments[0])) {
			checkScope(arguments[0]);
		}
	}

	function disable() {
		if (arguments.length === 0) {
			enabled = false;
		} else if (_.isString(arguments[0]) && checkScope(arguments[0])) {
			scopes[arguments[0]] = false;
		}
	}

	Tracer.prototype.limitCapabilities = limitCapabilities;
	Tracer.prototype.trace = trace;
	Tracer.prototype.t = trace;
	Tracer.prototype.traceScope = traceScope;
	Tracer.prototype.s = traceScope;
	Tracer.prototype.enable = enable;
	Tracer.prototype.disable = disable;
	return new Tracer();
}));
