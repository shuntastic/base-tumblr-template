/*jslint browser:true */
/*globals define, $, _, PxLoader, PxLoaderImage, AJS9 */
;(function (root, factory) {
	"use strict";
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'jquery',
				'underscore',
				'utils/Tracer',
				'pxloader/dist/pxloader-all.min'
			],
			function () {
				var o = factory($);
				if (!window.preloader) {
					window.preloader = o;
				}
				return (root.utils.preloader = o);
			}
		);
	} else {
		var o = factory($);
		if (!window.preloader) {
			window.preloader = o;
		}
		root.utils.preloader = o;
	}
}(window.AJS9 = window.AJS9 || {}, function () {

	var dict,
		loader,
		progressHandle = null,
		completeHandle = null;

	function Preloader() {
	}

	function init(options) {
		dict = {};
		loader = new PxLoader();
		loader.addProgressListener(onProgress);
		loader.addCompletionListener(onComplete);
		if (!_.isUndefined(options)) {
			if (!_.isUndefined(options.onComplete)) {
				completeHandle = options.onComplete;
			}
			if (!_.isUndefined(options.onProgress)) {
				progressHandle = options.onProgress;
			}
		}
	}

	function add() {
		_.each(arguments, function (list) {
			_.each(list, function (o) {
				switch(typeof o) {
					case 'string':
						loader.addImage(o);
						break;

					case 'object':
						var ldr;
						switch (o.type) {
							case 'image':
								ldr = new PxLoaderImage(o.url);
								break;
							case 'video':
								AJS9.utils.tracer.s('Preloader', 'videos not ready yet');
								break;
							case 'sound':
								AJS9.utils.tracer.s('Preloader', 'sound not ready yet');
								break;
						}
						if (!_.isUndefined(o.id)) {
							dict[o.id] = {
								loader: ldr,
								url: o.url
							};

						}
						loader.add(ldr);
						break;
				}
			});

		});
	}

	function onProgress(e) {
		if (progressHandle) {
			progressHandle(e, loader);
		}

	}

	function onComplete(e) {
		if (completeHandle) {
			completeHandle(e, loader);
		}
	}

	function start() {
		loader.start();
	}

	function getLoader(id) {
		return dict[id].loader;
	}

	function getObject(id) {
		return dict[id];
	}

	Preloader.prototype.add = add;
	Preloader.prototype.init = init;
	Preloader.prototype.getLoader = getLoader;
	Preloader.prototype.getObject = getObject;
	Preloader.prototype.start = start;
	return new Preloader();
}));