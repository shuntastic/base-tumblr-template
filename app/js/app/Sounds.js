/* THIS SHOULD BE IN REQUIRE FORMAT BUT ITS NOT
 * Skip down to about line 160 to see our functions.
 **/

/*jshint undef:false */

/**
 * FORCEMUTE -- for development only to turn off all sounds period. This should only be used during development for coder sanity
 */
var forceMute = false;
var snds; 

/**
 * Interface for SoundJS
 */
var SSSoundJSAudio = (function () {
    'use strict';

    // private
    var assetPath = settings.soundPath,
        sjs = createjs.Sound,
        manifest,
        sources = {},
        instances = {},
        isPaused = false,
        musicActive = false,
        isMuted = false;

    function startMusicLoop() {
        if (isMuted === true) {
            return;
        }

        if (instances.music === undefined || instances.music === null) {
            (function () {
                var source = sources.music,
                    delay = 0,
                    inTime = 0,
                    loopCount  = 99999,
                    volume = 1.0,
                    panPosition = 0.0001;
                if (source !== undefined && source !== null) {
                    instances.music = sjs.play(
                        source,
                        sjs.INTERRUPT_ANY,
                        delay,
                        inTime,
                        loopCount,
                        volume,
                        panPosition
                    );
                }
            }());
        } else {
            var source = sources.music,
                delay = 0,
                inTime = 0,
                loopCount  = 99999,
                volume = 1.0,
                panPosition = 0.0001;
            instances.music.play(
                sjs.INTERRUPT_ANY,
                delay,
                inTime,
                loopCount,
                volume,
                panPosition
            );
        }
    }

    function stopMusicLoop() {
        if (instances.music !== undefined && instances.music !== null) {
            instances.music.stop();
        }
    }
    function handleLoad(event) {
        sources[event.id] = event.src;
    }

    function initSoundJS() {
        if (!sjs.initializeDefaultPlugins()) {
            return 'Error: could not initialize soundJS.';
        }
        manifest = settings.sounds;
        sjs.alternateExtensions = ['ogg', 'mp3'];

        sjs.addEventListener('fileload', handleLoad);
        sjs.registerSounds(manifest, assetPath);
//        sjs.registerManifest(manifest, assetPath);
        sequencer.next();
    }

    function playOneShot(name) {
        if (isMuted === true) {
            return;
        }

        var source = sources[name],
            delay = 0,
            inTime = 0,
            loopCount  = 0,
            volume = 1.0,
            panPosition = 0.0001;

        if (source !== undefined && source !== null) {
            sjs.play(
                source,
                sjs.INTERRUPT_ANY,
                delay,
                inTime,
                loopCount,
                volume,
                panPosition
            );
        }
    }

    function disableAudio() {
        if (isMuted === true) {
            return;
        }

        sjs.stop();
        isMuted = true;
        stopMusicLoop();
    }

    function enableAudio() {
        if (isMuted === false) {
            return;
        }

        isMuted = false;

        if (musicActive) {
            startMusicLoop();
        }
    }



    // public
    return {
        init: function () {
            initSoundJS();
        },
        startMusicTrack: function () {
            if (!musicActive) {
                musicActive = true;
                startMusicLoop();
            }
        },
        stopMusicTrack: function () {
            musicActive = false;
            stopMusicLoop();
        },
        playOneShot: function (name) {
            playOneShot(name);
        },
        isMuted: function () {
            return isMuted;
        },
        enable: function () {
            enableAudio();
        },
        disable: function () {
            disableAudio();
        }
    };
}());



/* old sound controls merged --------------------- */
var firstSoundClick = true,    // used to start sounds on the first user click if desired for tablets
    autoMuted = false       // whether the site has been automatically muted internally --- like when a user is playing a video or exits the site
;

/**
 * Init the sound function - this should be called as early as possible in the build
 * It will register all the sounds from the JSON. It also disables sounds for mobile (but not tablet!)
 * @param {Type}
 */
function initSounds() {

    // NO SOUND FOR MOBILE DEVICES --- EVER!
    // disable sound & hide the sound button
    if (categorizr() != 'desktop') {
        forceMute = true;
        $('body').addClass('soundDisabled');
    }
    if (forceMute) {
        sequencer.next();
        return;
    }
    // initializes our sound object
    snds = SSSoundJSAudio;
    snds.init();
    if (categorizr() == 'tablet')
        muteSounds();
}


/**
 * Sometimes the sound button is not present when we init
 * so this is a separate call
 * @param {Type}
 */
function initSoundBtn() {
    $('#sound').click(toggleSound);
}

/**
 * plays a sound by its id.
 * Music will loop eternally.
 * Anything else will play once.
 *
 * @param {Type} value
 */
function playSound(value) {
    if (forceMute) return;
    if (value == 'music') {
        snds.startMusicTrack();
    }
    else
        snds.playOneShot(value);
}

/**
 * Since tablets do not auto start sound, we have a function that can be
 * called anytime by any click to start the sounds by the user
 * Precautions are taken to ensure it can only be called once.
 * @param {Type}
 */
function mobileSoundStart() {
    if (forceMute) return;
    if (categorizr() == 'tablet' && firstSoundClick) {
        snds.startMusicTrack();
    }
    firstSoundClick = false;
}

/**
 * toggler all sounds on or off opposite of what
 * it currently is
 */
function toggleSound()
{
    if (forceMute) return;
    if (!snds.isMuted())
        muteSounds();
    else
        unmuteSounds();
    return false;
}

function isMuted() {
    if (forceMute)
        return true;
    return snds.isMuted();
}

/**
 * Mute all sounds
 */
function muteSounds()
{
    if (forceMute) return;
    snds.disable();
    $('body').addClass('muted');

}

/**
 * unmute all sounds
 */
function unmuteSounds()
{
    if (forceMute) return;
    snds.enable();
    $('body').removeClass('muted');
}

/**
 * automute is for when we enter something like a video and we want to mute AND
 * flag if the site was muted already. This way if the user has already muted the site
 * we do not unmute it after
 */
function autoMute()
{
    if (forceMute) return;
    autoMuted = true;
    if (!$('body').hasClass('muted')) {
        snds.disable();
    }
}

/**
 * when we exit the thing that muted the site, this can be called to
 * unmute. It is polite though. If the mute was user initiated BEFORE automute was
 * called, it will keep the site muted
 */

function autoUnmute()
{
    if (forceMute) return;
    if (!autoMuted)
        return;
    autoMuted = false;
    if (!$('body').hasClass('muted'))
    {
        snds.enable();
    }
}

/**
 * just mute the music
 * @param {Type}
 */
function muteMusic() {
    if (forceMute) return;
    snds.stopMusicTrack();
}

/**
 * just unmute the music
 * @param {Type}
 */
function unmuteMusic() {
    if (forceMute) return;
    snds.startMusicTrack();
}
