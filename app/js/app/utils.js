var utils = {
    launchExc:'',
    mobileThreshold:1020,
    mobileCheck: function(){

        var mobileCon = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ? ((/IEMobile/i.test(navigator.userAgent)) ? 'mobile ieMobile' : 'mobile') : 'desktopmode';
        mobileCon = (window.innerWidth>utils.mobileThreshold) ? 'desktopmode' : mobileCon;
        $('body').removeClass('mobile').removeClass('desktopmode').addClass(mobileCon);
        sequencer.next();

    },
    upgradeCheck: function() {

        // use data-useragent to target specific browser versions
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);

        if ($('html').hasClass('lt-ie9') || /(iPhone|iPad|iPod)\sOS\s6/.test(navigator.userAgent)) {
            utils.forceUpgrade();
            return true;
        }

        sequencer.next();
    },
    forceUpgrade: function() {

        $('.preload-browser .preText, .preload-browser .loadWrapper').remove();
        $('.preload-browser').addClass('upgrade-browser').detach('.loader').append('<p>'+settings.upgrade+'</p>');
        TweenLite.to($('.blocker'),3,{autoAlpha: 0,ease:Power4.easeOut});

    },
    loadScript: function(url, callback) {
        // Adding the script tag to the head as suggested before
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    },
    resizeHandler: {
        pageWidth:0,
        pageHeight:0,
        resizeCheck: function() {

            utils.resizeHandler.pageWidth = window.innerWidth;
            utils.resizeHandler.pageHeight = window.innerHeight;

            // console.log('RESIZE:',utils.resizeHandler.pageWidth,utils.resizeHandler.pageHeight);
            
            if(utils.resizeHandler.pageHeight > utils.resizeHandler.pageWidth){
                $('body').addClass('vertical').removeClass('horizontal');
            } else{
                $('body').removeClass('vertical').addClass('horizontal');
            }

            if($('body').hasClass('mobile') && $('body').hasClass('horizontal')){

                 utils.resizeHandler.showLandcapePrompt();

            } else {

                utils.resizeHandler.hideLandcapePrompt();

            }
            var mainCan = document.getElementById('soundcan');
            if(mainCan){
              mainCan.width = utils.resizeHandler.pageWidth;
              mainCan.height = utils.resizeHandler.pageHeight;
            }

            if (fgCanvas) {
                fgCanvas.width = utils.resizeHandler.pageWidth;
                fgCanvas.height = utils.resizeHandler.pageHeight;
                fgCtx.translate(fgCanvas.width/2,fgCanvas.height/2);

            }

            if(camera){
              camera.aspect = utils.resizeHandler.pageWidth / utils.resizeHandler.pageHeight;
              camera.updateProjectionMatrix();

              renderer.setSize( utils.resizeHandler.pageWidth, utils.resizeHandler.pageHeight );

            }


        },
        showLandcapePrompt: function(){
            // TRIGGER LANDSCAPE PROMPT
            $('body').addClass('landscape');
            TweenMax.set($('.preload-browser'),0,{autoAlpha:1, ease:Power4.easeOut});

        },
        hideLandcapePrompt: function(){
            // HIDE LANDSCAPE PROMPT
            $('body').removeClass('landscape');
            TweenMax.set($('.preload-browser'),0,{autoAlpha:0, ease:Power4.easeOut});

        },
        init: function(){
            utils.resizeHandler.resizeCheck();
            window.addEventListener('orientationchange', utils.resizeHandler.resizeCheck);

            $(window).resize(function(){
                utils.resizeHandler.resizeCheck();
            });
            sequencer.next();
        }
    },
    visibility: {
        getHiddenProp: function(){
            var prefixes = ['webkit','moz','ms','o'];
            if ('hidden' in document) return 'hidden';
            for (var i = 0; i < prefixes.length; i++){
                if ((prefixes[i] + 'Hidden') in document) 
                    return prefixes[i] + 'Hidden';
            }
            return null;
        },
        isHidden: function() {
            var prop = utils.visibility.getHiddenProp();
            if (!prop) return false;
            return document[prop];
        },
        init: function(){
            var visProp = utils.visibility.getHiddenProp();
            if (visProp) {
              var evtname = visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
              document.addEventListener(evtname, visChange);
            }
            function visChange() {
                if (utils.visibility.isHidden()){
                    utils.soundHandler.onWindowBlur();
                    if(utils.vid.vidPlaying){
                        utils.vid.player.pauseVideo();
                    }

                } else{
                    utils.soundHandler.onWindowFocus();

                    if(utils.vid.vidPlaying){
                        utils.vid.player.playVideo();
                    }
                }
            }
        }
    },
    vid: {
        player:null,
        done: false,
        vidPlaying:false,
        oldID:'',
        onYouTubeIframeAPIReady: function() {
            //console.log('onYouTubeIframeAPIReady');
        },
        onPlayerAdded: function(curID,event){

            console.log('onPlayerAdded',curID,event);

            if($('.mediaItem .post #player').length<1){
                $('.mediaItem .post iframe').remove();
                $('.mediaItem .post').append('<div id="player"></div>');
                utils.vid.player = new YT.Player('player', {
                    videoId: curID,
                    playerVars:{'showinfo':0},
                    events: {
                        'onReady': utils.vid.onPlayerReady/*,
                        'onStateChange': utils.vid.onPlayerStateChange*/
                      }
                });

            }

            utils.vid.oldID = curID;
        },
        onPlayerReady: function(event) {
            console.log('onPlayerReady');
            event.target.addEventListener('onStateChange',utils.vid.onPlayerStateChange);
        },
        onPlayerStateChange: function (event) {
            console.log('onPlayerStateChange',event.data);
          if (event.data == YT.PlayerState.PLAYING) {
                           autoMute();
            utils.vid.vidPlaying = true;

          }
          if ((event.data == YT.PlayerState.PAUSED)||(event.data == YT.PlayerState.ENDED)) {
            utils.vid.vidPlaying = utils.visibility.isHidden();
           if(!utils.visibility.isHidden()) autoUnmute();

          }
        },
        loadYT: function(){
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/player_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            sequencer.next();
        }
    },

    soundHandler: {
        exitWasMuted: false,
        init: function(){
            if(utils.visibility.isHidden()){
                utils.soundHandler.onWindowBlur();
            } else{
                utils.soundHandler.onWindowFocus();
            }
            utils.visibility.init();
            sequencer.next();
        },
        onWindowBlur: function(e) {
            // console.log('onWindowBlur');
            utils.soundHandler.exitWasMuted = isMuted();
            if (!utils.soundHandler.exitWasMuted && !utils.vid.vidPlaying) {
                autoMute();
                utils.soundHandler.exitWasMuted = true;
            }
        },
        onWindowFocus: function() {
            // console.log('onWindowFocus');
            if (utils.soundHandler.exitWasMuted && !utils.vid.vidPlaying) {
                autoUnmute();
                utils.soundHandler.exitWasMuted = false;
            }
        }
    }
};

// YOUTUBE CAPTURE
function onYouTubeIframeAPIReady(){
    utils.vid.onYouTubeIframeAPIReady();
}
function onPlayerReady(event){
    utils.vid.onPlayerReady(event.target);
}
