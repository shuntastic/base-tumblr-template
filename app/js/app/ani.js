/*
    SITE TRANSITIONS
    */
    var ani = {
        currentTL:null,
        scrollControl:-1,
        showPosts: function(postObj){
            //
        },
        runTransition: function(trans,callback,args){
            var params = (typeof args !== 'undefined') ? args : null;
            trans(callback,params);
        },
        showPreloader: function(callback){

            function cleanUp(){
                callback();
                $('.preText h1, .preText h2, .preText p').css('color','').css('text-shadow','');
            }

            //ANI SETUP
            // TweenLite.to($('.blocker'),3,{autoAlpha: 0,ease:Power4.easeOut});
            // $('.preText h1, .preText h2, .preText p').css('color','transparent');

            (new TimelineMax({ onComplete: cleanUp }))
            // .fromTo($('.preText h1'), .5,{textShadow:"0px 0px 30px rgba(255,255,255, 0.9)",opacity:"0",scaleX:2,scaleY:2},{textShadow:"0px 0px 0px rgba(255,255,255, 0.9)",opacity:"1",scaleX:1,scaleY:1, ease:Power4.easeOut}, 1.5)
            // .fromTo($('.preText h2'), .5,{textShadow:"0px 0px 30px rgba(255,255,255, 0.9)",opacity:"0",scaleX:2,scaleY:2},{textShadow:"0px 0px 0px rgba(255,255,255, 0.9)",opacity:"1",scaleX:1,scaleY:1, ease:Power4.easeOut}, "-=.25")
            // .fromTo($('.preText p'), .5,{textShadow:"0px 0px 30px rgba(255,255,255, 0.9)",opacity:"0",scaleX:2,scaleY:2},{textShadow:"0px 0px 0px rgba(255,255,255, 0.9)",opacity:"1",scaleX:1,scaleY:1, ease:Power4.easeOut}, "-=.25");
        },
        hidePreloader: function(startSlide){
            var startingPost = (typeof startSlide !== 'undefined') ? startSlide : 0;

            function cleanUp(){
                $('.preload-browser').css('display','none');
            }

            //ANI SETUP

            
            (new TimelineMax({ onComplete: cleanUp }))
            // .to($('.preload-browser .loader, .preload-browser .theaterStatus'),.5,{autoAlpha:0,ease:Power4.easeOut},0)
            // .to($('.preload-browser'),1.25,{top:-$('.preload-browser').height(), ease:Power4.easeOut})
            // .addCallback(function(){$('body').removeClass('loading');},.5)
            // .to($('#mainContent, #topMenu'),.25,{opacity:"1"},0)
            // .from($('#bgSlider'),1.25,{top: "100%",ease:Power4.easeOut},"-=1.25")
            // .fromTo($('#bgSlider .slick-current div'),1.25,{webkitFilter:"grayscale(100%)"},{webkitFilter:"grayscale(0%)",ease:Power4.linear},"-=.05");
        },
        showModal: function(){

        },
        hideModal: function(){

        },
        showSection: function(){

        },
        hideSection: function(){

        }
    }
