
/*jshint undef: false */
(function (window) {
    var me,
        inited = false,
        isOpen = false,
        menuBtns,
        initH = 0,
        widestB = 0,
        enterTL,
        exitTL,
        closeCurtain = false
    ;

    function settingsRoute() {
        me = this;
    }

    function init() {
        inited = true;

        //SETS THE TEXT IN EACH ELEMENT FOR INITIAL SITE LOAD
        setElement(settings.topMenu);
        // createElement(settings.decadeIndicator);
        setElement(settings.mainMenu);
        setElement(settings.modalText);
        setDateElement(settings.dates);
        setElement(settings.footerMenu.ticketsBtn);
        setElement(settings.landscapeModePrompt);
        setElement(settings.footerMenu.legalBtn);
        setElement(settings.footerMenu.promo);
        setElement(settings.legal.legalLine);
        setElement(settings.legal.legalLinks);
        setElement(settings.filterTitle);
        setElement(settings.filterMenu);
        setElement(settings.socialMenu.links);

        sequencer.next();
    }

    function enter(){

    }

    function exit(){

        return false;
    }

    function setElement(data){
        $.each(data,function(index){
            var curObj = data[index];
            if(curObj.label && curObj.label_mobile){
                $(curObj.el).html('<span class="desktop-label">'+curObj.label+'</span><span class="mobile-label">'+curObj.label_mobile+'</span>');
            } else if(curObj.label){
                $(curObj.el).html(curObj.label);
            }
            if(curObj.url){
                $(curObj.el).attr('href',curObj.url);
            }
            if (!_.isUndefined(curObj.style)) {
                _.each(curObj.style, function (o,i) {
                    if (i == 'font-size' && o == 'default') {
                        return;
                    }
                    $(curObj.el).css(i, o);
                });
            }
            if (!_.isUndefined(curObj.mobileStyle)) {
                _.each(curObj.mobileStyle, function (o,i) {
                    if (i == 'font-size' && o == 'default') {
                        return;
                    }
                    $('.mobile '+curObj.el).css(i, o);
                });
            }
        });

    }
    function setDateElement(data){
        var today = new Date((new Date()).setHours(0, 0, 0, 0));

        $.each(data,function(index){
            
            var curObj = data[index];
            var dSplit, d;

            if(curObj.date == 'default'){
                setElement([curObj]);
                return false;
            }

            var dSplit = curObj.date.split('-'),
            userMonth = parseInt(dSplit[1], 10) - 1,
            userDate = new Date(dSplit[0], userMonth, dSplit[2], 0, 0, 0, 0);

            if(userDate <= today) {
                setElement([curObj]);
                return false;
            }
            return true;
        });

    }

    function setMobileElement(data){
        $.each(data,function(index){
            var curObj = data[index];
            if(curObj.label_mobile){
                $(curObj.el).html(curObj.label_mobile);
            }
            if(curObj.url){
                $(curObj.el).attr('href',curObj.url);
            }
            if (!_.isUndefined(curObj.style)) {
                _.each(curObj.style, function (o,i) {
                    if (i == 'font-size' && o == 'default') {
                        return;
                    }
                    $(curObj.el).css(i, o);
                });
            }
        });

    }

    function setQuote(data){
        var curObj = data;
        var optText = (curObj.line2) ? '<span class="line2">'+(curObj.line2)+'</span>' : '';
        var quote = (curObj.line1) ? curObj.line1+optText : optText;
        var byline = (curObj.byline) ? '— '+curObj.byline : '';
        var newQuote = '<div class="line1">'+quote+'</div><div class="byline">'+byline+'</div>';
        $('.quote').html(newQuote);
    }

    function createElement(data){

        var curObj = data;
        var newCont = '';
        var curTag = (curObj.tag) ? curObj.tag : 'div';


        $.each(curObj.content,function(index){
            newCont += '<'+curTag+' class="'+curObj.newEl+'">'+curObj.content[index]+'</'+curTag+'>';

        });
        $(curObj.el).html(newCont);
        if (!_.isUndefined(curObj.visible) && !curObj.visible) {
            $(curObj.el).css({
                // display: 'none',
                visibility: 'hidden',
                opacity: 0
            });
            // $(curObj.el).remove();
        }
        if (!_.isUndefined(curObj.style)) {
            _.each(curObj.style, function (o,i) {
                if (i == 'font-size' && o == 'default') {
                    return;
                }
                $(curObj.el).css(i, o);
            });
        }

    }


    window.settingsRoute = settingsRoute;
    settingsRoute.prototype.init = init;
    settingsRoute.prototype.enter = enter;
    settingsRoute.prototype.exit = exit;
    settingsRoute.prototype.setQuote = setQuote;
    settingsRoute.prototype.createElement = createElement;
    settingsRoute.prototype.setElement = setElement;
    settingsRoute.prototype.setMobileElement = setMobileElement;
    window.settingsRoute = new settingsRoute();

}(window));
