var context = new (window.AudioContext || window.webkitAudioContext)();
// function init(){
//     main.init();
// }
var audioList,videoList,drawVisual,analyser,fgCanvas,container,fgCtx,scene,camera,renderer,audioObj;
var main = {
    allData:[],
    // initPosts:5,
    firsttime:false,
    mainBG:null,
    audioPlayer:null,
    // audioObj:null,
    audioIndex:0,
    mainMenuContentSlider:null,
    currentSceneIndex:-1,
    visInit:function(){
        var visProp = utils.visibility.getHiddenProp();
        if (visProp) {
          var evtname = visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
          document.addEventListener(evtname, visChange);
        }
        function visChange() {
            if (utils.visibility.isHidden()){
                main.audioPlayer.pause();
            } else{
                main.audioPlayer.play();
            }
        }

    },
    init: function() {
    
    /* see window.onload */



    },
    initButtonActions: function(){

        // ARROW NAVIGATION
        $('.arrowNav .arrow_left').on('click',function(){
            // main.prevScene();
        });
        $('.arrowNav .arrow_right').on('click',function(){
            // main.nextScene();
        });

        // KEYBOARD NAVIGATION
        // $(document).unbind('keydown').keydown(function(e) {
        //     if(!$('#decadeOverview').hasClass('active')){
        //         switch(e.which) {
        //             case 37: // left
        //             main.prevScene();
        //             break;

        //             case 39: // right
        //             main.nextScene();
        //             break;

        //             default: return;
        //         }
        //         e.preventDefault();
        //     }
        // });



        // $('.legalToggle a, #legal .btnLegalClose').on('click',function(){
        //     if($('#legal').hasClass('active')){
        //         $('#legal').removeClass('active');
        //     } else{
        //         $('#legal').addClass('active');

        //     }
        //     return false;

        // });


        $('a.follow').on('click',function(){
            if($(this).hasClass('un')){
                $(this).html(settings.topMenu[3].label);
                $(this).removeClass('un');
            } else{
                $(this).addClass('un');
                $(this).html(settings.topMenu[3].selected_label);
            }
            // return false;

        });

        $('.btn-audio').on('click',toggleSound);

        $('#topMenu a.share, #shareDrawer .icon-btn_close_overlay').on('click',function(){
            if($('#shareDrawer').hasClass('active')){
                $('#shareDrawer').removeClass('active');
            } else{
                $('#shareDrawer').addClass('active');
                $('#connectDrawer').removeClass('active');
            }
            return false;

        });


        //CLICK ANYWHERE TO CLOSE
        $(document).on('click', function(event) {
          if (!$(event.target).closest('#topMenu,#legal,#shareDrawer,#connectDrawer').length) {
            $('#legal').removeClass('active');
            $('#shareDrawer').removeClass('active');
            $('#connectDrawer').removeClass('active');
        }
    });

        ani.initCarouselBns();


        //OVERLAY MENU FILTER RESULTS
        // $('a.filterby').each(function(index){

        //     $(this).on('click',function(){
        //         if($(this).hasClass('active')){
        //             $('a.filterby').each(function(){$(this).removeClass('active');});
        //             utils.tumblr.hideFilter();
        //         } else{
        //             $('a.filterby').each(function(){$(this).removeClass('active');});
        //             $(this).addClass('active');
        //             $('.overlayMenu h1').html(settings.filterMenu[index].selected_label);
        //             var filterCategory = ($(this).hasClass('image')) ? 'image':($(this).hasClass('gifs')) ? 'gif': ($(this).hasClass('video')) ? 'video' :'video';
        //             utils.tumblr.loadPosts(filterCategory+' '+main.statusArray[main.currentSceneIndex], ani.showOverviewGrid);
        //         }
        //         return false;
        //     });

        // });

        sequencer.next();

    },
    callScene: function(sceneRef) {
        // var prevScene = main.currentSceneIndex;

        // main.currentSceneIndex = sceneRef;

        // ani.changeQuote(prevScene,main.currentSceneIndex);
        // if(main.currentSceneIndex !== -1){
        //     $('.decadeMenu a').each(function(){$(this).removeClass('active')});
        //     $('.decadeMenu a.decNav'+main.statusArray[main.currentSceneIndex]).addClass('active');
        // }
        // $('.launchHolder .cta').html(settings.clickPrompt+main.statusArray[main.currentSceneIndex]+'s');
        // if(main.mainBG){
        //     main.mainBG.slick('slickGoTo',sceneRef,false);
        // } else{
        //     if (tumblrLoadProg + preloadProg == 2)
        //         ani.initMainCarousels(sceneRef);
        // }


    },
    nextScene: function(){
        // var callRef = (main.currentSceneIndex == 3) ? 0 : (main.currentSceneIndex+1);
        // $.address.value('/the'+main.statusArray[callRef]+'s');  
    },
    prevScene:function(){
        // var callRef = ((main.currentSceneIndex == 0)||(main.currentSceneIndex == -1)) ? 3 : (main.currentSceneIndex-1);
        // $.address.value('/the'+main.statusArray[callRef]+'s');  
    },
    addressHandler: function(evt){
        var nextSlide = -1;
        console.log('addressHandler evt',evt);
    }
}

/**
 * PRELOADER -----------------------------------------------------------
 */

 var preloadProg = 0,
 tumblrLoadProg = 0,
 preloadExited = false;

 function loadSite () {
    // console.log('loadSite');
    preloader.init({
        onProgress: function(evt) {
              // console.log(evt);
              var sitePercent = (evt.completedCount/evt.totalCount)*100+'%';
              $('.sitePrompt a').html(sitePercent);
              $('.loader span').css('width',sitePercent);
          },
          onComplete: onPreloadComplete
    });

    // inject the asset path location into the url for everything we want to preload
    _.each(settings.desktopPreloadFiles, function(o,i) {
        if (_.isString(o)) {
            settings.desktopPreloadFiles[i] = presets.assetPath + o;
        } else {
            settings.desktopPreloadFiles[i].url = presets.assetPath + o.url;
        }
    });
    preloader.add(settings.desktopPreloadFiles);
    preloader.start();

    // our tumblr content
    utils.tumblr.loadAllPosts();
    // $('#decadeOverview .mediaItem').imagesLoaded().done(onTumblrLoadComplete);
}


function onTumblrLoadComplete() {
    tumblrLoadProg = 1;
    updatePreloader();
}

/**
 * on complete, fade out the preload content but keep our curtain up a moment longer
 */
 function onPreloadComplete() {
     preloadProg = 1;
     updatePreloader();
 }

 function updatePreloader() {
    if (tumblrLoadProg +  preloadProg == 2) {
       sequencer.next();
       playSound('music');

       // if(utils.launchExc !=''){
       //  console.log('unique post')
       //  ani.hidePreloader();
       //  ani.openOverview(utils.launchExc)
       // } else {
        ani.showModalPrompt();
       // }
       // ani.hidePreloader();
   }
}



// $(document).ready(main.init);

window.onload = function init(){
    settings.browser = categorizr();
    var tumblr = new tumblrHandler();
    // var visualizer = new soundVizOrb();
    
    audioList = tumblr.getPostsByType({type:'audio'}); 
    videoList = tumblr.getPostsByType({type:'video'}); 

    // var loader = new SoundcloudLoader(player,uiUpdater);


    $(document).keydown(function(e) {
        var keyHit = false;
        switch(e.which) {
            case 37: // left
            if(main.audioIndex >0){
                main.audioIndex -=1;
                keyHit = true;

            }
            break;

            // case 38: // up
            // break;

            case 39: // right
            if(main.audioIndex < audioList.length){
                main.audioIndex +=1;
                keyHit = true;
            }

            break;

            // case 40: // down
            // break;

            default: return; // exit this handler for other keys
        }
        if(keyHit){

        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });


    audioObj = (audioList.length) ? audioList[main.audioIndex]:null;
    if(audioObj){
        // 'http://api.soundcloud.com/resolve.json?url='
        utils.loadScript('https://connect.soundcloud.com/sdk/sdk-3.0.0.js',function(){


            var tUrl = decodeURIComponent($(audioObj.audio.audioEmbed).attr('src').split('https://w.soundcloud.com/player/?url=')[1].split('&')[0]);
            // console.log('tUrl',tUrl);

            var player =  document.getElementById('player');
            var audioSource = new scContext(player);
            analyser = context.createAnalyser();

            var loader = new scLoader(player);
            loader.loadStream(tUrl,
                function() {
                    console.log('obj',loader.streamUrl());
                    audioSource.playStream(loader.streamUrl());
                },
                function() {
                    console.log('error',loader.getErrorMessage)
                    // uiUpdater.displayMessage("Error", loader.errorMessage);
                }
            );


            function render() { 
                requestAnimationFrame(render); 
                var val = audioSource.volume/1000;
                // console.log('val',val);
                // var bucket = Math.ceil(audioSource.streamData.length);
                // analyser.getByteTimeDomainData(dataArray);

                // if(val>0)console.log('dataArray',dataArray);
                // cube.rotation.x += val; 
                // cube.rotation.y += val; 
                cube.rotation.x += 0.075; 
                cube.rotation.y += 0.075; 
                cube.scale.x = val/10;
                cube.scale.y = val/10;
                cube.scale.z = val/10;

                // fgCtx.clearRect(0, 0, fgCanvas.width, fgCanvas.height);
                // fgCtx.rect(0, 0, fgCanvas.width, fgCanvas.height);
                // fgCtx.font = "bold 20px sans-serif";
                // fgCtx.fillStyle = 'grey';
                // fgCtx.fillText(("val: " + val + ' bucket: '+bucket), 30, 30);

                renderer.render(scene, camera);
            }; 
            
            scene = new THREE.Scene();
            camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
            renderer = new THREE.WebGLRenderer(); renderer.setSize( window.innerWidth, window.innerHeight );
            container = document.getElementById('soundcan');

            container.appendChild( renderer.domElement );
            // var geometry = new THREE.BoxGeometry( 2, 2, 2 ); 
            var geometry = new THREE.DodecahedronGeometry(2); 

            var ambientLight = new THREE.AmbientLight( 0x000000 );
            scene.add( ambientLight );

            var lights = [];
            lights[0] = new THREE.PointLight( 0xffffff, 1, 0 );
            lights[1] = new THREE.PointLight( 0xffffff, 1, 0 );
            lights[2] = new THREE.PointLight( 0xffffff, 1, 0 );
            
            lights[0].position.set( 0, 200, 0 );
            lights[1].position.set( 100, 200, 100 );
            lights[2].position.set( -100, -200, -100 );

            scene.add( lights[0] );
            scene.add( lights[1] );
            scene.add( lights[2] );

            var material = new THREE.MeshPhongMaterial({
                color: 0x62e3ff,
                emissive: 0x000088,
                side: THREE.DoubleSide,
                shading: THREE.FlatShading
            })

            // var material = new THREE.MeshBasicMaterial( { color: 0x440000 } ); 
            var cube = new THREE.Mesh( geometry, material );
            scene.add(cube);
            scene.add(cube);

            fgCanvas = document.createElement('canvas');
            fgCtx = fgCanvas.getContext("2d");

            fgCanvas.setAttribute('style', 'position: fixed;top:0px; z-index: 10');
            container.appendChild(fgCanvas);

            camera.position.z = 5;
            render();

            utils.resizeHandler.init();


        });
    }

}

