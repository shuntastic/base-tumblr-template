    //HANDLER FOR TUMBLR POSTS
    var tumblrHandler = function() {
        var self = this;
        this.postData = {};
        this.postsPerPage = 10;
        this.index = -1;
        this.currentPosts = '';
        this.filterGrid = null;
        this.callData = function(dataPath,callback){
            console.log('callData',dataPath);
            $.ajax({
                url: dataPath,
                type: "post",
                dataType: "html",
                context:document.body
            }).done(function(evt){
                var newData = $(evt);
                // newData = $(newData).find('#datacon').detach();
                $('#datacon').html(newData.find('#datacon').detach());
                callback(evt);

            }).error(function(evt){
                console.log('called error',evt);
            });
        };

        this.getPosts = function(args){
            args.count = (typeof args.count !== 'undefined') ? args.count : utils.tumblr.postsPerPage;
            // args.type = (typeof args.type !== 'undefined') ? args.type : 'default';
            var output = (typeof postData !== 'undefined') ? postData : [];

            function loadNext(){
               var callString = (pageInfo && pageInfo.nextURL!=='') ? pageInfo.nextURL : null;
                if((output.length<args.count) || (args.count==-1)){
                    if(callString){
                        utils.tumblr.callData(callString,loadNext);
                    } else {
                      console.log('all results returned',output);
                      // postData = output;
                      return output;
                    }
                } else {
                    console.log('requested results',output);
                    // postData = output;
                    return output;
                }
            }
            if(args.count > postData.length){
                loadNext();
            } else{
                return output.splice(0,args.count);
            }
       };
       this.getPostsByType = function(args){
        args.type = (typeof args.type !== 'undefined') ? args.type : '';
        var output = (postData.length&&args.type.length)?$.map(postData, function(v,i){if(v.type===args.type) return v}):[];
        return output;
       };
       this.loadIndividualPost = function(postVal,callback){
           console.log('loadIndividualPost',postVal,callback)

            function parsePost(){


            }

                utils.tumblr.callForData(postVal,curTag,parsePost);

       };
        this.hideFilter = function(){
            utils.tumblr.loadPosts(main.statusArray[main.currentSceneIndex],ani.showOverviewContent);

        };
        this.setSocialBtns = function(postRef){
            console.log('setSocialBtns',postRef);
            var mediaItemInfo = $(postRef);
            // var mediaItemInfo = $(utils.tumblr.postData[utils.tumblr.currentPosts].postsItemsArr[postRef]);

            var curFB = $(mediaItemInfo).find('a.facebook').attr('href');
            // var curFB = $(mediaItemInfo).find('a.facebook').attr('href')+'&p[images][0]='+;
            var curTwitter = $(mediaItemInfo).find('a.twitter').attr('href');
            var curPinterest = $(mediaItemInfo).find('a.pinterest').attr('href');
            var curGoogle = $(mediaItemInfo).find('a.googleplus').attr('href');
            var curReblog = $(mediaItemInfo).find('.reblog-control a').attr('href');
            var likeID = $(mediaItemInfo).find('.like-control').data('post-id');
            
            // var likeID = $(mediaItemInfo).data('post-id');
            $('.shareThis .icon-fb').attr('href',curFB);
            $('.shareThis .icon-twitter').attr('href',curTwitter);
            $('.shareThis .icon-pinterest').attr('href',curPinterest);
            $('.shareThis .icon-g').attr('href',curGoogle);
            $('.mediaactions .icon-icon_reblog').attr('href',curReblog);

            // $('.likeBn').removeClass('liked');
           

            var curLike = $.extend(true, {},mediaItemInfo.find('.like_button'));

            console.log('curLike',curLike);
            /*.detach();*/
            // $('.mediaactions .likeBn').html(curLike);
            // $('.icon-icon_like').remove();
            // if(curLike)
            $('.mediaactions .likeBn').html(curLike).append('<a href="javascript:void(0);" class="icon-icon_like">'+settings.overlaySocial[0].label+'</a>');
            Tumblr.LikeButton.get_status_by_post_ids([likeID])

            // $('.mediaactions .icon-icon_like .standLike').append('<a href="javascript:void(0);" class="customLike">'+settings.overlaySocial[0].label+'</a>');
            // $('.icon-icon_like').unbind('click').on('click', function(){
            //     $(this).addClass('liked');
            // });

        }
    };
