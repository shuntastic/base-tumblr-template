/*!
 * Modernizr v2.8.3
 * www.modernizr.com
 *
 * Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
 * Available under the BSD and MIT licenses: www.modernizr.com/license/
 */
/*
 * Modernizr tests which native CSS3 and HTML5 features are available in
 * the current UA and makes the results available to you in two ways:
 * as properties on a global Modernizr object, and as classes on the
 * <html> element. This information allows you to progressively enhance
 * your pages with a granular level of control over the experience.
 *
 * Modernizr has an optional (not included) conditional resource loader
 * called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
 * To get a build that includes Modernizr.load(), as well as choosing
 * which tests to include, go to www.modernizr.com/download/
 *
 * Authors        Faruk Ates, Paul Irish, Alex Sexton
 * Contributors   Ryan Seddon, Ben Alman
 */
/*global PxLoader: true, define: true */
// PxLoader plugin to load images
function PxLoaderImage(a, b, c) {
    var d = this, e = null;
    this.img = new Image(), this.tags = b, this.priority = c;
    var f = function() {
        "complete" === d.img.readyState && (i(), e.onLoad(d));
    }, g = function() {
        i(), e.onLoad(d);
    }, h = function() {
        i(), e.onError(d);
    }, i = function() {
        d.unbind("load", g), d.unbind("readystatechange", f), d.unbind("error", h);
    };
    this.start = function(b) {
        // we need the loader ref so we can notify upon completion
        e = b, // NOTE: Must add event listeners before the src is set. We
        // also need to use the readystatechange because sometimes
        // load doesn't fire when an image is in the cache.
        d.bind("load", g), d.bind("readystatechange", f), d.bind("error", h), d.img.src = a;
    }, // called by PxLoader to check status of image (fallback in case
    // the event listeners are not triggered).
    this.checkStatus = function() {
        d.img.complete && (i(), e.onLoad(d));
    }, // called by PxLoader when it is no longer waiting
    this.onTimeout = function() {
        i(), d.img.complete ? e.onLoad(d) : e.onTimeout(d);
    }, // returns a name for the resource that can be used in logging
    this.getName = function() {
        return a;
    }, // cross-browser event binding
    this.bind = function(a, b) {
        d.img.addEventListener ? d.img.addEventListener(a, b, !1) : d.img.attachEvent && d.img.attachEvent("on" + a, b);
    }, // cross-browser event un-binding
    this.unbind = function(a, b) {
        d.img.removeEventListener ? d.img.removeEventListener(a, b, !1) : d.img.detachEvent && d.img.detachEvent("on" + a, b);
    };
}

/*global PxLoader: true, define: true, Video: true */
// PxLoader plugin to load video elements
function PxLoaderVideo(a, b, c) {
    var d = this, e = null;
    this.readyEventName = "canplaythrough";
    try {
        this.vid = new Video();
    } catch (f) {
        this.vid = document.createElement("video");
    }
    this.tags = b, this.priority = c;
    var g = function() {
        4 === d.vid.readyState && (j(), e.onLoad(d));
    }, h = function() {
        j(), e.onLoad(d);
    }, i = function() {
        j(), e.onError(d);
    }, j = function() {
        d.unbind("load", h), d.unbind(d.readyEventName, g), d.unbind("error", i);
    };
    this.start = function(b) {
        // we need the loader ref so we can notify upon completion
        e = b, // NOTE: Must add event listeners before the src is set. We
        // also need to use the readystatechange because sometimes
        // load doesn't fire when an video is in the cache.
        d.bind("load", h), d.bind(d.readyEventName, g), d.bind("error", i), // sometimes the browser will intentionally stop downloading
        // the video. In that case we'll consider the video loaded
        d.bind("suspend", h), d.vid.src = a, d.vid.load();
    }, // called by PxLoader to check status of video (fallback in case
    // the event listeners are not triggered).
    this.checkStatus = function() {
        4 === d.vid.readyState && (j(), e.onLoad(d));
    }, // called by PxLoader when it is no longer waiting
    this.onTimeout = function() {
        j(), 4 !== d.vid.readyState ? e.onLoad(d) : e.onTimeout(d);
    }, // returns a name for the resource that can be used in logging
    this.getName = function() {
        return a;
    }, // cross-browser event binding
    this.bind = function(a, b) {
        d.vid.addEventListener ? d.vid.addEventListener(a, b, !1) : d.vid.attachEvent && d.vid.attachEvent("on" + a, b);
    }, // cross-browser event un-binding
    this.unbind = function(a, b) {
        d.vid.removeEventListener ? d.vid.removeEventListener(a, b, !1) : d.vid.detachEvent && d.vid.detachEvent("on" + a, b);
    };
}

window.Modernizr = function(a, b, c) {
    /**
     * setCss applies given styles to the Modernizr DOM node.
     */
    function d(a) {
        t.cssText = a;
    }
    /**
     * setCssAll extrapolates all vendor-specific css strings.
     */
    function e(a, b) {
        return d(x.join(a + ";") + (b || ""));
    }
    /**
     * is returns a boolean for if typeof obj is exactly type.
     */
    function f(a, b) {
        return typeof a === b;
    }
    /**
     * contains returns a boolean for if substr is found within str.
     */
    function g(a, b) {
        return !!~("" + a).indexOf(b);
    }
    /*>>testprop*/
    // testProps is a generic CSS / DOM property test.
    // In testing support for a given CSS property, it's legit to test:
    //    `elem.style[styleName] !== undefined`
    // If the property is supported it will return an empty string,
    // if unsupported it will return undefined.
    // We'll take advantage of this quick test and skip setting a style
    // on our modernizr element, but instead just testing undefined vs
    // empty string.
    // Because the testing of the CSS property names (with "-", as
    // opposed to the camelCase DOM properties) is non-portable and
    // non-standard but works in WebKit and IE (but not Gecko or Opera),
    // we explicitly reject properties with dashes so that authors
    // developing in WebKit or IE first don't end up with
    // browser-specific content by accident.
    function h(a, b) {
        for (var d in a) {
            var e = a[d];
            if (!g(e, "-") && t[e] !== c) return "pfx" == b ? e : !0;
        }
        return !1;
    }
    /*>>testprop*/
    // TODO :: add testDOMProps
    /**
     * testDOMProps is a generic DOM property test; if a browser supports
     *   a certain property, it won't return undefined for it.
     */
    function i(a, b, d) {
        for (var e in a) {
            var g = b[a[e]];
            if (g !== c) // return the property name as a string
            // return the property name as a string
            // let's bind a function
            return d === !1 ? a[e] : f(g, "function") ? g.bind(d || b) : g;
        }
        return !1;
    }
    /*>>testallprops*/
    /**
     * testPropsAll tests a list of DOM properties we want to check against.
     *   We specify literally ALL possible (known and/or likely) properties on
     *   the element including the non-vendor prefixed one, for forward-
     *   compatibility.
     */
    function j(a, b, c) {
        var d = a.charAt(0).toUpperCase() + a.slice(1), e = (a + " " + z.join(d + " ") + d).split(" ");
        // did they call .prefixed('boxSizing') or are we just testing a prop?
        // did they call .prefixed('boxSizing') or are we just testing a prop?
        return f(b, "string") || f(b, "undefined") ? h(e, b) : (e = (a + " " + A.join(d + " ") + d).split(" "), 
        i(e, b, c));
    }
    /*>>webforms*/
    // input features and input types go directly onto the ret object, bypassing the tests loop.
    // Hold this guy to execute in a moment.
    function k() {
        /*>>input*/
        // Run through HTML5's new input attributes to see if the UA understands any.
        // We're using f which is the <input> element created early on
        // Mike Taylr has created a comprehensive resource for testing these attributes
        //   when applied to all input types:
        //   miketaylr.com/code/input-type-attr.html
        // spec: www.whatwg.org/specs/web-apps/current-work/multipage/the-input-element.html#input-type-attr-summary
        // Only input placeholder is tested while textarea's placeholder is not.
        // Currently Safari 4 and Opera 11 have support only for the input placeholder
        // Both tests are available in feature-detects/forms-placeholder.js
        o.input = function(c) {
            for (var d = 0, e = c.length; e > d; d++) E[c[d]] = !!(c[d] in u);
            // safari false positive's on datalist: webk.it/74252
            // see also github.com/Modernizr/Modernizr/issues/146
            return E.list && (E.list = !(!b.createElement("datalist") || !a.HTMLDataListElement)), 
            E;
        }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), 
        /*>>input*/
        /*>>inputtypes*/
        // Run through HTML5's new input types to see if the UA understands any.
        //   This is put behind the tests runloop because it doesn't return a
        //   true/false like all the other tests; instead, it returns an object
        //   containing each input type with its corresponding true/false value
        // Big thanks to @miketaylr for the html5 forms expertise. miketaylr.com/
        o.inputtypes = function(a) {
            for (var d, e, f, g = 0, h = a.length; h > g; g++) u.setAttribute("type", e = a[g]), 
            d = "text" !== u.type, // We first check to see if the type we give it sticks..
            // If the type does, we feed it a textual value, which shouldn't be valid.
            // If the value doesn't stick, we know there's input sanitization which infers a custom UI
            d && (u.value = v, u.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(e) && u.style.WebkitAppearance !== c ? (q.appendChild(u), 
            f = b.defaultView, // Safari 2-4 allows the smiley as a value, despite making a slider
            d = f.getComputedStyle && "textfield" !== f.getComputedStyle(u, null).WebkitAppearance && // Mobile android web browser has false positive, so must
            // check the height to see if the widget is actually there.
            0 !== u.offsetHeight, q.removeChild(u)) : /^(search|tel)$/.test(e) || (// Real url and email support comes with prebaked validation.
            d = /^(url|email)$/.test(e) ? u.checkValidity && u.checkValidity() === !1 : u.value != v)), 
            D[a[g]] = !!d;
            return D;
        }("search tel url email datetime date month week time datetime-local number range color".split(" "));
    }
    var l, m, n = "2.8.3", o = {}, /*>>cssclasses*/
    // option for enabling the HTML classes to be added
    p = !0, /*>>cssclasses*/
    q = b.documentElement, /**
     * Create our "modernizr" element that we do most feature tests on.
     */
    r = "modernizr", s = b.createElement(r), t = s.style, /**
     * Create the input element for various Web Forms feature tests.
     */
    u = b.createElement("input"), /*>>smile*/
    v = ":)", /*>>smile*/
    w = {}.toString, // TODO :: make the prefixes more granular
    /*>>prefixes*/
    // List of property values to set for css tests. See ticket #21
    x = " -webkit- -moz- -o- -ms- ".split(" "), /*>>prefixes*/
    /*>>domprefixes*/
    // Following spec is to expose vendor-specific style properties as:
    //   elem.style.WebkitBorderRadius
    // and the following would be incorrect:
    //   elem.style.webkitBorderRadius
    // Webkit ghosts their properties in lowercase but Opera & Moz do not.
    // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
    //   erik.eae.net/archives/2008/03/10/21.48.10/
    // More here: github.com/Modernizr/Modernizr/issues/issue/21
    y = "Webkit Moz O ms", z = y.split(" "), A = y.toLowerCase().split(" "), /*>>domprefixes*/
    /*>>ns*/
    B = {
        svg: "http://www.w3.org/2000/svg"
    }, /*>>ns*/
    C = {}, D = {}, E = {}, F = [], G = F.slice, // used in testing loop
    /*>>teststyles*/
    // Inject element with style element and some CSS rules
    H = function(a, c, d, e) {
        var f, g, h, i, j = b.createElement("div"), // After page load injecting a fake body doesn't work so check if body exists
        k = b.body, // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
        l = k || b.createElement("body");
        if (parseInt(d, 10)) // In order not to give false positives we create a node for each test
        // This also allows the method to scale for unspecified uses
        for (;d--; ) h = b.createElement("div"), h.id = e ? e[d] : r + (d + 1), j.appendChild(h);
        // <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
        // when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
        // with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
        // msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
        // Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
        // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
        // Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
        //avoid crashing IE8, if background image is used
        //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
        // If this is done after page load we don't want to remove the body so check if body exists
        return f = [ "&#173;", '<style id="s', r, '">', a, "</style>" ].join(""), j.id = r, 
        (k ? j : l).innerHTML += f, l.appendChild(j), k || (l.style.background = "", l.style.overflow = "hidden", 
        i = q.style.overflow, q.style.overflow = "hidden", q.appendChild(l)), g = c(j, a), 
        k ? j.parentNode.removeChild(j) : (l.parentNode.removeChild(l), q.style.overflow = i), 
        !!g;
    }, /*>>teststyles*/
    /*>>mq*/
    // adapted from matchMedia polyfill
    // by Scott Jehl and Paul Irish
    // gist.github.com/786768
    I = function(b) {
        var c = a.matchMedia || a.msMatchMedia;
        if (c) return c(b) && c(b).matches || !1;
        var d;
        return H("@media " + b + " { #" + r + " { position: absolute; } }", function(b) {
            d = "absolute" == (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle).position;
        }), d;
    }, /*>>mq*/
    /*>>hasevent*/
    //
    // isEventSupported determines if a given element supports the given event
    // kangax.github.com/iseventsupported/
    //
    // The following results are known incorrects:
    //   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
    //   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
    //   ...
    J = function() {
        function a(a, e) {
            e = e || b.createElement(d[a] || "div"), a = "on" + a;
            // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and "resize", whereas `in` "catches" those
            var g = a in e;
            // If it has no `setAttribute` (i.e. doesn't implement Node interface), try generic element
            // If property was created, "remove it" (by setting value to `undefined`)
            return g || (e.setAttribute || (e = b.createElement("div")), e.setAttribute && e.removeAttribute && (e.setAttribute(a, ""), 
            g = f(e[a], "function"), f(e[a], "undefined") || (e[a] = c), e.removeAttribute(a))), 
            e = null, g;
        }
        var d = {
            select: "input",
            change: "input",
            submit: "form",
            reset: "form",
            error: "img",
            load: "img",
            abort: "img"
        };
        return a;
    }(), /*>>hasevent*/
    // TODO :: Add flag for hasownprop ? didn't last time
    // hasOwnProperty shim by kangax needed for Safari 2.0 support
    K = {}.hasOwnProperty;
    m = f(K, "undefined") || f(K.call, "undefined") ? function(a, b) {
        /* yes, this can give false positives/negatives, but most of the time we don't care about those */
        return b in a && f(a.constructor.prototype[b], "undefined");
    } : function(a, b) {
        return K.call(a, b);
    }, // Adapted from ES5-shim https://github.com/kriskowal/es5-shim/blob/master/es5-shim.js
    // es5.github.com/#x15.3.4.5
    Function.prototype.bind || (Function.prototype.bind = function(a) {
        var b = this;
        if ("function" != typeof b) throw new TypeError();
        var c = G.call(arguments, 1), d = function() {
            if (this instanceof d) {
                var e = function() {};
                e.prototype = b.prototype;
                var f = new e(), g = b.apply(f, c.concat(G.call(arguments)));
                return Object(g) === g ? g : f;
            }
            return b.apply(a, c.concat(G.call(arguments)));
        };
        return d;
    }), /*>>testallprops*/
    /**
     * Tests
     * -----
     */
    // The *new* flexbox
    // dev.w3.org/csswg/css3-flexbox
    C.flexbox = function() {
        return j("flexWrap");
    }, // The *old* flexbox
    // www.w3.org/TR/2009/WD-css3-flexbox-20090723/
    C.flexboxlegacy = function() {
        return j("boxDirection");
    }, // On the S60 and BB Storm, getContext exists, but always returns undefined
    // so we actually have to call getContext() to verify
    // github.com/Modernizr/Modernizr/issues/issue/97/
    C.canvas = function() {
        var a = b.createElement("canvas");
        return !(!a.getContext || !a.getContext("2d"));
    }, C.canvastext = function() {
        return !(!o.canvas || !f(b.createElement("canvas").getContext("2d").fillText, "function"));
    }, // webk.it/70117 is tracking a legit WebGL feature detect proposal
    // We do a soft detect which may false positive in order to avoid
    // an expensive context creation: bugzil.la/732441
    C.webgl = function() {
        return !!a.WebGLRenderingContext;
    }, /*
     * The Modernizr.touch test only indicates if the browser supports
     *    touch events, which does not necessarily reflect a touchscreen
     *    device, as evidenced by tablets running Windows 7 or, alas,
     *    the Palm Pre / WebOS (touch) phones.
     *
     * Additionally, Chrome (desktop) used to lie about its support on this,
     *    but that has since been rectified: crbug.com/36415
     *
     * We also test for Firefox 4 Multitouch Support.
     *
     * For more info, see: modernizr.github.com/Modernizr/touch.html
     */
    C.touch = function() {
        var c;
        return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : H([ "@media (", x.join("touch-enabled),("), r, ")", "{#modernizr{top:9px;position:absolute}}" ].join(""), function(a) {
            c = 9 === a.offsetTop;
        }), c;
    }, // geolocation is often considered a trivial feature detect...
    // Turns out, it's quite tricky to get right:
    //
    // Using !!navigator.geolocation does two things we don't want. It:
    //   1. Leaks memory in IE9: github.com/Modernizr/Modernizr/issues/513
    //   2. Disables page caching in WebKit: webk.it/43956
    //
    // Meanwhile, in Firefox < 8, an about:config setting could expose
    // a false positive that would throw an exception: bugzil.la/688158
    C.geolocation = function() {
        return "geolocation" in navigator;
    }, C.postmessage = function() {
        return !!a.postMessage;
    }, // Chrome incognito mode used to throw an exception when using openDatabase
    // It doesn't anymore.
    C.websqldatabase = function() {
        return !!a.openDatabase;
    }, // Vendors had inconsistent prefixing with the experimental Indexed DB:
    // - Webkit's implementation is accessible through webkitIndexedDB
    // - Firefox shipped moz_indexedDB before FF4b9, but since then has been mozIndexedDB
    // For speed, we don't test the legacy (and beta-only) indexedDB
    C.indexedDB = function() {
        return !!j("indexedDB", a);
    }, // documentMode logic from YUI to filter out IE8 Compat Mode
    //   which false positives.
    C.hashchange = function() {
        return J("hashchange", a) && (b.documentMode === c || b.documentMode > 7);
    }, // Per 1.6:
    // This used to be Modernizr.historymanagement but the longer
    // name has been deprecated in favor of a shorter and property-matching one.
    // The old API is still available in 1.6, but as of 2.0 will throw a warning,
    // and in the first release thereafter disappear entirely.
    C.history = function() {
        return !(!a.history || !history.pushState);
    }, C.draganddrop = function() {
        var a = b.createElement("div");
        return "draggable" in a || "ondragstart" in a && "ondrop" in a;
    }, // FF3.6 was EOL'ed on 4/24/12, but the ESR version of FF10
    // will be supported until FF19 (2/12/13), at which time, ESR becomes FF17.
    // FF10 still uses prefixes, so check for it until then.
    // for more ESR info, see: mozilla.org/en-US/firefox/organizations/faq/
    C.websockets = function() {
        return "WebSocket" in a || "MozWebSocket" in a;
    }, // css-tricks.com/rgba-browser-support/
    C.rgba = function() {
        // Set an rgba() color and check the returned value
        return d("background-color:rgba(150,255,150,.5)"), g(t.backgroundColor, "rgba");
    }, C.hsla = function() {
        // Same as rgba(), in fact, browsers re-map hsla() to rgba() internally,
        //   except IE9 who retains it as hsla
        return d("background-color:hsla(120,40%,100%,.5)"), g(t.backgroundColor, "rgba") || g(t.backgroundColor, "hsla");
    }, C.multiplebgs = function() {
        // If the UA supports multiple backgrounds, there should be three occurrences
        //   of the string "url(" in the return value for elemStyle.background
        // Setting multiple images AND a color on the background shorthand property
        //  and then querying the style.background property value for the number of
        //  occurrences of "url(" is a reliable method for detecting ACTUAL support for this!
        return d("background:url(https://),url(https://),red url(https://)"), /(url\s*\(.*?){3}/.test(t.background);
    }, // this will false positive in Opera Mini
    //   github.com/Modernizr/Modernizr/issues/396
    C.backgroundsize = function() {
        return j("backgroundSize");
    }, C.borderimage = function() {
        return j("borderImage");
    }, // Super comprehensive table about all the unique implementations of
    // border-radius: muddledramblings.com/table-of-css3-border-radius-compliance
    C.borderradius = function() {
        return j("borderRadius");
    }, // WebOS unfortunately false positives on this test.
    C.boxshadow = function() {
        return j("boxShadow");
    }, // FF3.0 will false positive on this test
    C.textshadow = function() {
        return "" === b.createElement("div").style.textShadow;
    }, C.opacity = function() {
        // The non-literal . in this regex is intentional:
        //   German Chrome returns this value as 0,55
        // github.com/Modernizr/Modernizr/issues/#issue/59/comment/516632
        // Browsers that actually have CSS Opacity implemented have done so
        //  according to spec, which means their return values are within the
        //  range of [0.0,1.0] - including the leading zero.
        return e("opacity:.55"), /^0.55$/.test(t.opacity);
    }, // Note, Android < 4 will pass this test, but can only animate
    //   a single property at a time
    //   goo.gl/v3V4Gp
    C.cssanimations = function() {
        return j("animationName");
    }, C.csscolumns = function() {
        return j("columnCount");
    }, C.cssgradients = function() {
        /**
         * For CSS Gradients syntax, please see:
         * webkit.org/blog/175/introducing-css-gradients/
         * developer.mozilla.org/en/CSS/-moz-linear-gradient
         * developer.mozilla.org/en/CSS/-moz-radial-gradient
         * dev.w3.org/csswg/css3-images/#gradients-
         */
        var a = "background-image:", b = "gradient(linear,left top,right bottom,from(#9f9),to(white));", c = "linear-gradient(left top,#9f9, white);";
        // legacy webkit syntax (FIXME: remove when syntax not in use anymore)
        // standard syntax             // trailing 'background-image:'
        return d((a + "-webkit- ".split(" ").join(b + a) + x.join(c + a)).slice(0, -a.length)), 
        g(t.backgroundImage, "gradient");
    }, C.cssreflections = function() {
        return j("boxReflect");
    }, C.csstransforms = function() {
        return !!j("transform");
    }, C.csstransforms3d = function() {
        var a = !!j("perspective");
        // Webkit's 3D transforms are passed off to the browser's own graphics renderer.
        //   It works fine in Safari on Leopard and Snow Leopard, but not in Chrome in
        //   some conditions. As a result, Webkit typically recognizes the syntax but
        //   will sometimes throw a false positive, thus we must do a more thorough check:
        // Webkit allows this media query to succeed only if the feature is enabled.
        // `@media (transform-3d),(-webkit-transform-3d){ ... }`
        return a && "webkitPerspective" in q.style && H("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b) {
            a = 9 === b.offsetLeft && 3 === b.offsetHeight;
        }), a;
    }, C.csstransitions = function() {
        return j("transition");
    }, /*>>fontface*/
    // @font-face detection routine by Diego Perini
    // javascript.nwbox.com/CSSSupport/
    // false positives:
    //   WebOS github.com/Modernizr/Modernizr/issues/342
    //   WP7   github.com/Modernizr/Modernizr/issues/538
    C.fontface = function() {
        var a;
        return H('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
            var e = b.getElementById("smodernizr"), f = e.sheet || e.styleSheet, g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
            a = /src/i.test(g) && 0 === g.indexOf(d.split(" ")[0]);
        }), a;
    }, /*>>fontface*/
    // CSS generated content detection
    C.generatedcontent = function() {
        var a;
        return H([ "#", r, "{font:0/0 a}#", r, ':after{content:"', v, '";visibility:hidden;font:3px/1 a}' ].join(""), function(b) {
            a = b.offsetHeight >= 3;
        }), a;
    }, // These tests evaluate support of the video/audio elements, as well as
    // testing what types of content they support.
    //
    // We're using the Boolean constructor here, so that we can extend the value
    // e.g.  Modernizr.video     // true
    //       Modernizr.video.ogg // 'probably'
    //
    // Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
    //                     thx to NielsLeenheer and zcorpan
    // Note: in some older browsers, "no" was a return value instead of empty string.
    //   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
    //   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5
    C.video = function() {
        var a = b.createElement("video"), c = !1;
        // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
        try {
            (c = !!a.canPlayType) && (c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), 
            // Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
            c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""));
        } catch (d) {}
        return c;
    }, C.audio = function() {
        var a = b.createElement("audio"), c = !1;
        try {
            (c = !!a.canPlayType) && (c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), 
            c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), // Mimetypes accepted:
            //   developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
            //   bit.ly/iphoneoscodecs
            c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, ""));
        } catch (d) {}
        return c;
    }, // In FF4, if disabled, window.localStorage should === null.
    // Normally, we could not test that directly and need to do a
    //   `('localStorage' in window) && ` test first because otherwise Firefox will
    //   throw bugzil.la/365772 if cookies are disabled
    // Also in iOS5 Private Browsing mode, attempting to use localStorage.setItem
    // will throw the exception:
    //   QUOTA_EXCEEDED_ERRROR DOM Exception 22.
    // Peculiarly, getItem and removeItem calls do not throw.
    // Because we are forced to try/catch this, we'll go aggressive.
    // Just FWIW: IE8 Compat mode supports these features completely:
    //   www.quirksmode.org/dom/html5.html
    // But IE8 doesn't support either with local files
    C.localstorage = function() {
        try {
            return localStorage.setItem(r, r), localStorage.removeItem(r), !0;
        } catch (a) {
            return !1;
        }
    }, C.sessionstorage = function() {
        try {
            return sessionStorage.setItem(r, r), sessionStorage.removeItem(r), !0;
        } catch (a) {
            return !1;
        }
    }, C.webworkers = function() {
        return !!a.Worker;
    }, C.applicationcache = function() {
        return !!a.applicationCache;
    }, // Thanks to Erik Dahlstrom
    C.svg = function() {
        return !!b.createElementNS && !!b.createElementNS(B.svg, "svg").createSVGRect;
    }, // specifically for SVG inline in HTML, not within XHTML
    // test page: paulirish.com/demo/inline-svg
    C.inlinesvg = function() {
        var a = b.createElement("div");
        return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == B.svg;
    }, // SVG SMIL animation
    C.smil = function() {
        return !!b.createElementNS && /SVGAnimate/.test(w.call(b.createElementNS(B.svg, "animate")));
    }, // This test is only for clip paths in SVG proper, not clip paths on HTML content
    // demo: srufaculty.sru.edu/david.dailey/svg/newstuff/clipPath4.svg
    // However read the comments to dig into applying SVG clippaths to HTML content here:
    //   github.com/Modernizr/Modernizr/issues/213#issuecomment-1149491
    C.svgclippaths = function() {
        return !!b.createElementNS && /SVGClipPath/.test(w.call(b.createElementNS(B.svg, "clipPath")));
    };
    /*>>webforms*/
    // End of test definitions
    // -----------------------
    // Run through all tests and detect their support in the current UA.
    // todo: hypothetically we could be doing an array of tests and use a basic loop here.
    for (var L in C) m(C, L) && (// run the test, throw the return value into the Modernizr,
    //   then based on that boolean, define an appropriate className
    //   and push it into an array of classes we'll join later.
    l = L.toLowerCase(), o[l] = C[L](), F.push((o[l] ? "" : "no-") + l));
    /*>>cssclasses*/
    /*>>webforms*/
    // input tests need to run.
    /*>>webforms*/
    /**
     * addTest allows the user to define their own feature tests
     * the result will be added onto the Modernizr object,
     * as well as an appropriate className set on the html element
     *
     * @param feature - String naming the feature
     * @param test - Function returning true if feature is supported, false if not
     */
    // Reset modElem.cssText to nothing to reduce memory footprint.
    /*>>shiv*/
    // Assign private properties to the return object with prefix
    // expose these for the plugin API. Look in the source for how to join() them against your input
    /*>>prefixes*/
    /*>>prefixes*/
    /*>>domprefixes*/
    /*>>domprefixes*/
    /*>>mq*/
    // Modernizr.mq tests a given media query, live against the current state of the window
    // A few important notes:
    //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
    //   * A max-width or orientation query will be evaluated against the current state, which may change later.
    //   * You must specify values. Eg. If you are testing support for the min-width media query use:
    //       Modernizr.mq('(min-width:0)')
    // usage:
    // Modernizr.mq('only screen and (max-width:768)')
    /*>>mq*/
    /*>>hasevent*/
    // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
    // Modernizr.hasEvent('gesturestart', elem)
    /*>>hasevent*/
    /*>>testprop*/
    // Modernizr.testProp() investigates whether a given style property is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testProp('pointerEvents')
    /*>>testprop*/
    /*>>testallprops*/
    // Modernizr.testAllProps() investigates whether a given style property,
    //   or any of its vendor-prefixed variants, is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testAllProps('boxSizing')
    /*>>testallprops*/
    /*>>teststyles*/
    // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
    // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
    /*>>teststyles*/
    /*>>prefixed*/
    // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
    // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'
    // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
    // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
    //
    //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');
    // If you're trying to ascertain which transition end event to bind to, you might do something like...
    //
    //     var transEndEventNames = {
    //       'WebkitTransition' : 'webkitTransitionEnd',
    //       'MozTransition'    : 'transitionend',
    //       'OTransition'      : 'oTransitionEnd',
    //       'msTransition'     : 'MSTransitionEnd',
    //       'transition'       : 'transitionend'
    //     },
    //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];
    /*>>prefixed*/
    /*>>cssclasses*/
    // Remove "no-js" class from <html> element, if it exists:
    // Add the new classes to the <html> element.
    return o.input || k(), o.addTest = function(a, b) {
        if ("object" == typeof a) for (var d in a) m(a, d) && o.addTest(d, a[d]); else {
            if (a = a.toLowerCase(), o[a] !== c) // we're going to quit if you're trying to overwrite an existing test
            // if we were to allow it, we'd do this:
            //   var re = new RegExp("\\b(no-)?" + feature + "\\b");
            //   docElement.className = docElement.className.replace( re, '' );
            // but, no rly, stuff 'em.
            return o;
            b = "function" == typeof b ? b() : b, "undefined" != typeof p && p && (q.className += " " + (b ? "" : "no-") + a), 
            o[a] = b;
        }
        return o;
    }, d(""), s = u = null, function(a, b) {
        /*--------------------------------------------------------------------------*/
        /**
         * Creates a style sheet with the given CSS text and adds it to the document.
         * @private
         * @param {Document} ownerDocument The document.
         * @param {String} cssText The CSS text.
         * @returns {StyleSheet} The style element.
         */
        function c(a, b) {
            var c = a.createElement("p"), d = a.getElementsByTagName("head")[0] || a.documentElement;
            return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild);
        }
        /**
         * Returns the value of `html5.elements` as an array.
         * @private
         * @returns {Array} An array of shived element node names.
         */
        function d() {
            var a = s.elements;
            return "string" == typeof a ? a.split(" ") : a;
        }
        /**
         * Returns the data associated to the given document
         * @private
         * @param {Document} ownerDocument The document.
         * @returns {Object} An object of data.
         */
        function e(a) {
            var b = r[a[p]];
            return b || (b = {}, q++, a[p] = q, r[q] = b), b;
        }
        /**
         * returns a shived element for the given nodeName and document
         * @memberOf html5
         * @param {String} nodeName name of the element
         * @param {Document} ownerDocument The context document.
         * @returns {Object} The shived element.
         */
        function f(a, c, d) {
            if (c || (c = b), k) return c.createElement(a);
            d || (d = e(c));
            var f;
            // Avoid adding some elements to fragments in IE < 9 because
            // * Attributes like `name` or `type` cannot be set/changed once an element
            //   is inserted into a document/fragment
            // * Link elements with `src` attributes that are inaccessible, as with
            //   a 403 response, will cause the tab/window to crash
            // * Script elements appended to fragments will execute when their `src`
            //   or `text` property is set
            return f = d.cache[a] ? d.cache[a].cloneNode() : o.test(a) ? (d.cache[a] = d.createElem(a)).cloneNode() : d.createElem(a), 
            !f.canHaveChildren || n.test(a) || f.tagUrn ? f : d.frag.appendChild(f);
        }
        /**
         * returns a shived DocumentFragment for the given document
         * @memberOf html5
         * @param {Document} ownerDocument The context document.
         * @returns {Object} The shived DocumentFragment.
         */
        function g(a, c) {
            if (a || (a = b), k) return a.createDocumentFragment();
            c = c || e(a);
            for (var f = c.frag.cloneNode(), g = 0, h = d(), i = h.length; i > g; g++) f.createElement(h[g]);
            return f;
        }
        /**
         * Shivs the `createElement` and `createDocumentFragment` methods of the document.
         * @private
         * @param {Document|DocumentFragment} ownerDocument The document.
         * @param {Object} data of the document.
         */
        function h(a, b) {
            b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, 
            b.frag = b.createFrag()), a.createElement = function(c) {
                //abort shiv
                //abort shiv
                return s.shivMethods ? f(c, a, b) : b.createElem(c);
            }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + // unroll the `createElement` calls
            d().join().replace(/[\w\-]+/g, function(a) {
                return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")';
            }) + ");return n}")(s, b.frag);
        }
        /*--------------------------------------------------------------------------*/
        /**
         * Shivs the given document.
         * @memberOf html5
         * @param {Document} ownerDocument The document to shiv.
         * @returns {Document} The shived document.
         */
        function i(a) {
            a || (a = b);
            var d = e(a);
            // corrects block display not defined in IE6/7/8/9
            return !s.shivCSS || j || d.hasCSS || (d.hasCSS = !!c(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), 
            k || h(a, d), a;
        }
        /*jshint evil:true */
        /** version */
        var j, k, l = "3.7.0", m = a.html5 || {}, n = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i, o = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i, p = "_html5shiv", q = 0, r = {};
        !function() {
            try {
                var a = b.createElement("a");
                a.innerHTML = "<xyz></xyz>", //if the hidden property is implemented we can assume, that the browser supports basic HTML5 Styles
                j = "hidden" in a, k = 1 == a.childNodes.length || function() {
                    // assign a false positive if unable to shiv
                    b.createElement("a");
                    var a = b.createDocumentFragment();
                    return "undefined" == typeof a.cloneNode || "undefined" == typeof a.createDocumentFragment || "undefined" == typeof a.createElement;
                }();
            } catch (c) {
                // assign a false positive if detection fails => unable to shiv
                j = !0, k = !0;
            }
        }();
        /*--------------------------------------------------------------------------*/
        /**
         * The `html5` object is exposed so that more elements can be shived and
         * existing shiving can be detected on iframes.
         * @type Object
         * @example
         *
         * // options can be changed before the script is included
         * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
         */
        var s = {
            /**
           * An array or space separated string of node names of the elements to shiv.
           * @memberOf html5
           * @type Array|String
           */
            elements: m.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
            /**
           * current version of html5shiv
           */
            version: l,
            /**
           * A flag to indicate that the HTML5 style sheet should be inserted.
           * @memberOf html5
           * @type Boolean
           */
            shivCSS: m.shivCSS !== !1,
            /**
           * Is equal to true if a browser supports creating unknown/HTML5 elements
           * @memberOf html5
           * @type boolean
           */
            supportsUnknownElements: k,
            /**
           * A flag to indicate that the document's `createElement` and `createDocumentFragment`
           * methods should be overwritten.
           * @memberOf html5
           * @type Boolean
           */
            shivMethods: m.shivMethods !== !1,
            /**
           * A string to describe the type of `html5` object ("default" or "default print").
           * @memberOf html5
           * @type String
           */
            type: "default",
            // shivs the document according to the specified `html5` object options
            shivDocument: i,
            //creates a shived element
            createElement: f,
            //creates a shived documentFragment
            createDocumentFragment: g
        };
        /*--------------------------------------------------------------------------*/
        // expose html5
        a.html5 = s, // shiv the document
        i(b);
    }(this, b), o._version = n, o._prefixes = x, o._domPrefixes = A, o._cssomPrefixes = z, 
    o.mq = I, o.hasEvent = J, o.testProp = function(a) {
        return h([ a ]);
    }, o.testAllProps = j, o.testStyles = H, o.prefixed = function(a, b, c) {
        return b ? j(a, b, c) : j(a, "pfx");
    }, q.className = q.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (p ? " js " + F.join(" ") : ""), 
    o;
}(this, this.document), /*! jQuery Address v${version} | (c) 2009, 2013 Rostislav Hristov | jquery.org/license */
function(a) {
    a.address = function() {
        var b, c, d = function(b) {
            var c = a.extend(a.Event(b), function() {
                for (var b = {}, c = a.address.parameterNames(), d = 0, e = c.length; e > d; d++) b[c[d]] = a.address.parameter(c[d]);
                return {
                    value: a.address.value(),
                    path: a.address.path(),
                    pathNames: a.address.pathNames(),
                    parameterNames: c,
                    parameters: b,
                    queryString: a.address.queryString()
                };
            }.call(a.address));
            return a(a.address).trigger(c), c;
        }, e = function(a) {
            return Array.prototype.slice.call(a);
        }, f = function() {
            return a().bind.apply(a(a.address), Array.prototype.slice.call(arguments)), a.address;
        }, g = function() {
            return a().unbind.apply(a(a.address), Array.prototype.slice.call(arguments)), a.address;
        }, h = function() {
            return Q.pushState && J.state !== b;
        }, i = function() {
            return ("/" + R.pathname.replace(new RegExp(J.state), "") + R.search + (j() ? "#" + j() : "")).replace(U, "/");
        }, j = function() {
            var a = R.href.indexOf("#");
            return -1 != a ? R.href.substr(a + 1) : "";
        }, k = function() {
            return h() ? i() : j();
        }, l = function() {
            try {
                return top.document !== b && top.document.title !== b && top.jQuery !== b && top.jQuery.address !== b && top.jQuery.address.frames() !== !1 ? top : window;
            } catch (a) {
                return window;
            }
        }, m = function() {
            return "javascript";
        }, n = function(a) {
            return a = a.toString(), (J.strict && "/" != a.substr(0, 1) ? "/" : "") + a;
        }, o = function(a, b) {
            return parseInt(a.css(b), 10);
        }, p = function() {
            if (!$) {
                var a = k(), b = decodeURI(ca) != decodeURI(a);
                b && (N && 7 > L ? R.reload() : (N && !W && J.history && T(s, 50), ca = a, q(I)));
            }
        }, q = function(a) {
            return T(r, 10), d(E).isDefaultPrevented() || d(a ? F : G).isDefaultPrevented();
        }, r = function() {
            if ("null" !== J.tracker && J.tracker !== z) {
                var c = a.isFunction(J.tracker) ? J.tracker : O[J.tracker], d = (R.pathname + R.search + (a.address && !h() ? a.address.value() : "")).replace(/\/\//, "/").replace(/^\/$/, "");
                a.isFunction(c) ? c(d) : (a.isFunction(O.urchinTracker) && O.urchinTracker(d), O.pageTracker !== b && a.isFunction(O.pageTracker._trackPageview) && O.pageTracker._trackPageview(d), 
                O._gaq !== b && a.isFunction(O._gaq.push) && O._gaq.push([ "_trackPageview", decodeURI(d) ]), 
                a.isFunction(O.ga) && O.ga("send", "pageview", d));
            }
        }, s = function() {
            var a = m() + ":" + I + ";document.open();document.writeln('<html><head><title>" + P.title.replace(/\'/g, "\\'") + "</title><script>var " + A + ' = "' + encodeURIComponent(k()).replace(/\'/g, "\\'") + (P.domain != R.hostname ? '";document.domain="' + P.domain : "") + "\";</script></head></html>');document.close();";
            7 > L ? c.src = a : c.contentWindow.location.replace(a);
        }, t = function() {
            if (X && -1 != Y) {
                var a, b, c = X.substr(Y + 1).split("&");
                for (a = 0; a < c.length; a++) b = c[a].split("="), /^(autoUpdate|history|strict|wrap)$/.test(b[0]) && (J[b[0]] = isNaN(b[1]) ? /^(true|yes)$/i.test(b[1]) : 0 !== parseInt(b[1], 10)), 
                /^(state|tracker)$/.test(b[0]) && (J[b[0]] = b[1]);
                X = z;
            }
            ca = k();
        }, u = function() {
            if (!_) {
                if (_ = H, t(), a('a[rel*="address:"]').address(), J.wrap) {
                    {
                        var e = a("body");
                        a("body > *").wrapAll('<div style="padding:' + (o(e, "marginTop") + o(e, "paddingTop")) + "px " + (o(e, "marginRight") + o(e, "paddingRight")) + "px " + (o(e, "marginBottom") + o(e, "paddingBottom")) + "px " + (o(e, "marginLeft") + o(e, "paddingLeft")) + 'px;" />').parent().wrap('<div id="' + A + '" style="height:100%;overflow:auto;position:relative;' + (M && !window.statusbar.visible ? "resize:both;" : "") + '" />');
                    }
                    a("html, body").css({
                        height: "100%",
                        margin: 0,
                        padding: 0,
                        overflow: "hidden"
                    }), M && a('<style type="text/css" />').appendTo("head").text("#" + A + "::-webkit-resizer { background-color: #fff; }");
                }
                if (N && !W) {
                    var f = P.getElementsByTagName("frameset")[0];
                    c = P.createElement((f ? "" : "i") + "frame"), c.src = m() + ":" + I, f ? (f.insertAdjacentElement("beforeEnd", c), 
                    f[f.cols ? "cols" : "rows"] += ",0", c.noResize = H, c.frameBorder = c.frameSpacing = 0) : (c.style.display = "none", 
                    c.style.width = c.style.height = 0, c.tabIndex = -1, P.body.insertAdjacentElement("afterBegin", c)), 
                    T(function() {
                        a(c).bind("load", function() {
                            var a = c.contentWindow;
                            ca = a[A] !== b ? a[A] : "", ca != k() && (q(I), R.hash = ca);
                        }), c.contentWindow[A] === b && s();
                    }, 50);
                }
                T(function() {
                    d("init"), q(I);
                }, 1), h() || (N && L > 7 || !N && W ? O.addEventListener ? O.addEventListener(C, p, I) : O.attachEvent && O.attachEvent("on" + C, p) : S(p, 50)), 
                "state" in window.history && a(window).trigger("popstate");
            }
        }, v = function() {
            decodeURI(ca) != decodeURI(k()) && (ca = k(), q(I));
        }, w = function() {
            O.removeEventListener ? O.removeEventListener(C, p, I) : O.detachEvent && O.detachEvent("on" + C, p);
        }, x = function(a) {
            a = a.toLowerCase();
            var b = /(chrome)[ \/]([\w.]+)/.exec(a) || /(webkit)[ \/]([\w.]+)/.exec(a) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a) || /(msie) ([\w.]+)/.exec(a) || a.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a) || [];
            return {
                browser: b[1] || "",
                version: b[2] || "0"
            };
        }, y = function() {
            var a = {}, b = x(navigator.userAgent);
            return b.browser && (a[b.browser] = !0, a.version = b.version), a.chrome ? a.webkit = !0 : a.webkit && (a.safari = !0), 
            a;
        }, z = null, A = "jQueryAddress", B = "string", C = "hashchange", D = "init", E = "change", F = "internalChange", G = "externalChange", H = !0, I = !1, J = {
            autoUpdate: H,
            history: H,
            strict: H,
            frames: H,
            wrap: I
        }, K = y(), L = parseFloat(K.version), M = K.webkit || K.safari, N = K.msie, O = l(), P = O.document, Q = O.history, R = O.location, S = setInterval, T = setTimeout, U = /\/{2,9}/g, V = navigator.userAgent, W = "on" + C in O, X = a("script:last").attr("src"), Y = X ? X.indexOf("?") : -1, Z = P.title, $ = I, _ = I, aa = H, ba = I, ca = k();
        if (N) {
            L = parseFloat(V.substr(V.indexOf("MSIE") + 4)), P.documentMode && P.documentMode != L && (L = 8 != P.documentMode ? 7 : 8);
            var da = P.onpropertychange;
            P.onpropertychange = function() {
                da && da.call(P), P.title != Z && -1 != P.title.indexOf("#" + k()) && (P.title = Z);
            };
        }
        if (Q.navigationMode && (Q.navigationMode = "compatible"), "complete" == document.readyState) var ea = setInterval(function() {
            a.address && (u(), clearInterval(ea));
        }, 50); else t(), a(u);
        return a(window).bind("popstate", v).bind("unload", w), {
            bind: function() {
                return f.apply(this, e(arguments));
            },
            unbind: function() {
                return g.apply(this, e(arguments));
            },
            init: function() {
                return f.apply(this, [ D ].concat(e(arguments)));
            },
            change: function() {
                return f.apply(this, [ E ].concat(e(arguments)));
            },
            internalChange: function() {
                return f.apply(this, [ F ].concat(e(arguments)));
            },
            externalChange: function() {
                return f.apply(this, [ G ].concat(e(arguments)));
            },
            baseURL: function() {
                var a = R.href;
                return -1 != a.indexOf("#") && (a = a.substr(0, a.indexOf("#"))), /\/$/.test(a) && (a = a.substr(0, a.length - 1)), 
                a;
            },
            autoUpdate: function(a) {
                return a !== b ? (J.autoUpdate = a, this) : J.autoUpdate;
            },
            history: function(a) {
                return a !== b ? (J.history = a, this) : J.history;
            },
            state: function(a) {
                if (a !== b) {
                    J.state = a;
                    var c = i();
                    return J.state !== b && (Q.pushState ? "/#/" == c.substr(0, 3) && R.replace(J.state.replace(/^\/$/, "") + c.substr(2)) : "/" != c && c.replace(/^\/#/, "") != j() && T(function() {
                        R.replace(J.state.replace(/^\/$/, "") + "/#" + c);
                    }, 1)), this;
                }
                return J.state;
            },
            frames: function(a) {
                return a !== b ? (J.frames = a, O = l(), this) : J.frames;
            },
            strict: function(a) {
                return a !== b ? (J.strict = a, this) : J.strict;
            },
            tracker: function(a) {
                return a !== b ? (J.tracker = a, this) : J.tracker;
            },
            wrap: function(a) {
                return a !== b ? (J.wrap = a, this) : J.wrap;
            },
            update: function() {
                return ba = H, this.value(ca), ba = I, this;
            },
            title: function(a) {
                return a !== b ? (T(function() {
                    Z = P.title = a, aa && c && c.contentWindow && c.contentWindow.document && (c.contentWindow.document.title = a, 
                    aa = I);
                }, 50), this) : P.title;
            },
            value: function(a) {
                if (a !== b) {
                    if (a = n(a), "/" == a && (a = ""), ca == a && !ba) return;
                    if (ca = a, J.autoUpdate || ba) {
                        if (q(H)) return this;
                        h() ? Q[J.history ? "pushState" : "replaceState"]({}, "", J.state.replace(/\/$/, "") + ("" === ca ? "/" : ca)) : ($ = H, 
                        M ? J.history ? R.hash = "#" + ca : R.replace("#" + ca) : ca != k() && (J.history ? R.hash = "#" + ca : R.replace("#" + ca)), 
                        N && !W && J.history && T(s, 50), M ? T(function() {
                            $ = I;
                        }, 1) : $ = I);
                    }
                    return this;
                }
                return n(ca);
            },
            path: function(a) {
                if (a !== b) {
                    var c = this.queryString(), d = this.hash();
                    return this.value(a + (c ? "?" + c : "") + (d ? "#" + d : "")), this;
                }
                return n(ca).split("#")[0].split("?")[0];
            },
            pathNames: function() {
                var a = this.path(), b = a.replace(U, "/").split("/");
                return ("/" == a.substr(0, 1) || 0 === a.length) && b.splice(0, 1), "/" == a.substr(a.length - 1, 1) && b.splice(b.length - 1, 1), 
                b;
            },
            queryString: function(a) {
                if (a !== b) {
                    var c = this.hash();
                    return this.value(this.path() + (a ? "?" + a : "") + (c ? "#" + c : "")), this;
                }
                var d = ca.split("?");
                return d.slice(1, d.length).join("?").split("#")[0];
            },
            parameter: function(c, d, e) {
                var f, g;
                if (d !== b) {
                    var h = this.parameterNames();
                    for (g = [], d = d === b || d === z ? "" : d.toString(), f = 0; f < h.length; f++) {
                        var i = h[f], j = this.parameter(i);
                        typeof j == B && (j = [ j ]), i == c && (j = d === z || "" === d ? [] : e ? j.concat([ d ]) : [ d ]);
                        for (var k = 0; k < j.length; k++) g.push(i + "=" + j[k]);
                    }
                    return -1 == a.inArray(c, h) && d !== z && "" !== d && g.push(c + "=" + d), this.queryString(g.join("&")), 
                    this;
                }
                if (d = this.queryString()) {
                    var l = [];
                    for (g = d.split("&"), f = 0; f < g.length; f++) {
                        var m = g[f].split("=");
                        m[0] == c && l.push(m.slice(1).join("="));
                    }
                    if (0 !== l.length) return 1 != l.length ? l : l[0];
                }
            },
            parameterNames: function() {
                var b = this.queryString(), c = [];
                if (b && -1 != b.indexOf("=")) for (var d = b.split("&"), e = 0; e < d.length; e++) {
                    var f = d[e].split("=")[0];
                    -1 == a.inArray(f, c) && c.push(f);
                }
                return c;
            },
            hash: function(a) {
                if (a !== b) return this.value(ca.split("#")[0] + (a ? "#" + a : "")), this;
                var c = ca.split("#");
                return c.slice(1, c.length).join("#");
            }
        };
    }(), a.fn.address = function(b) {
        return a(this).each(function() {
            a(this).data("address") || a(this).on("click", function(c) {
                if (c.shiftKey || c.ctrlKey || c.metaKey || 2 == c.which) return !0;
                var d = c.currentTarget;
                if (a(d).is("a")) {
                    c.preventDefault();
                    var e = b ? b.call(d) : /address:/.test(a(d).attr("rel")) ? a(d).attr("rel").split("address:")[1].split(" ")[0] : void 0 === a.address.state() || /^\/?$/.test(a.address.state()) ? a(d).attr("href").replace(/^(#\!?|\.)/, "") : a(d).attr("href").replace(new RegExp("^(.*" + a.address.state() + "|\\.)"), "");
                    a.address.value(e);
                }
            }).on("submit", function(c) {
                var d = c.currentTarget;
                if (a(d).is("form")) {
                    c.preventDefault();
                    var e = a(d).attr("action"), f = b ? b.call(d) : (-1 != e.indexOf("?") ? e.replace(/&$/, "") : e + "?") + a(d).serialize();
                    a.address.value(f);
                }
            }).data("address", !0);
        }), this;
    };
}(jQuery), //     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
function() {
    function a(a) {
        function b(b, c, d, e, f, g) {
            for (;f >= 0 && g > f; f += a) {
                var h = e ? e[f] : f;
                d = c(d, b[h], h, b);
            }
            return d;
        }
        return function(c, d, e, f) {
            d = t(d, f, 4);
            var g = !A(c) && s.keys(c), h = (g || c).length, i = a > 0 ? 0 : h - 1;
            return arguments.length < 3 && (e = c[g ? g[i] : i], i += a), b(c, d, e, g, i, h);
        };
    }
    function b(a) {
        return function(b, c, d) {
            c = u(c, d);
            for (var e = z(b), f = a > 0 ? 0 : e - 1; f >= 0 && e > f; f += a) if (c(b[f], f, b)) return f;
            return -1;
        };
    }
    function c(a, b, c) {
        return function(d, e, f) {
            var g = 0, h = z(d);
            if ("number" == typeof f) a > 0 ? g = f >= 0 ? f : Math.max(f + h, g) : h = f >= 0 ? Math.min(f + 1, h) : f + h + 1; else if (c && f && h) return f = c(d, e), 
            d[f] === e ? f : -1;
            if (e !== e) return f = b(k.call(d, g, h), s.isNaN), f >= 0 ? f + g : -1;
            for (f = a > 0 ? g : h - 1; f >= 0 && h > f; f += a) if (d[f] === e) return f;
            return -1;
        };
    }
    function d(a, b) {
        var c = F.length, d = a.constructor, e = s.isFunction(d) && d.prototype || h, f = "constructor";
        for (s.has(a, f) && !s.contains(b, f) && b.push(f); c--; ) f = F[c], f in a && a[f] !== e[f] && !s.contains(b, f) && b.push(f);
    }
    var e = this, f = e._, g = Array.prototype, h = Object.prototype, i = Function.prototype, j = g.push, k = g.slice, l = h.toString, m = h.hasOwnProperty, n = Array.isArray, o = Object.keys, p = i.bind, q = Object.create, r = function() {}, s = function(a) {
        return a instanceof s ? a : this instanceof s ? void (this._wrapped = a) : new s(a);
    };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = s), 
    exports._ = s) : e._ = s, s.VERSION = "1.8.3";
    var t = function(a, b, c) {
        if (void 0 === b) return a;
        switch (null == c ? 3 : c) {
          case 1:
            return function(c) {
                return a.call(b, c);
            };

          case 2:
            return function(c, d) {
                return a.call(b, c, d);
            };

          case 3:
            return function(c, d, e) {
                return a.call(b, c, d, e);
            };

          case 4:
            return function(c, d, e, f) {
                return a.call(b, c, d, e, f);
            };
        }
        return function() {
            return a.apply(b, arguments);
        };
    }, u = function(a, b, c) {
        return null == a ? s.identity : s.isFunction(a) ? t(a, b, c) : s.isObject(a) ? s.matcher(a) : s.property(a);
    };
    s.iteratee = function(a, b) {
        return u(a, b, 1 / 0);
    };
    var v = function(a, b) {
        return function(c) {
            var d = arguments.length;
            if (2 > d || null == c) return c;
            for (var e = 1; d > e; e++) for (var f = arguments[e], g = a(f), h = g.length, i = 0; h > i; i++) {
                var j = g[i];
                b && void 0 !== c[j] || (c[j] = f[j]);
            }
            return c;
        };
    }, w = function(a) {
        if (!s.isObject(a)) return {};
        if (q) return q(a);
        r.prototype = a;
        var b = new r();
        return r.prototype = null, b;
    }, x = function(a) {
        return function(b) {
            return null == b ? void 0 : b[a];
        };
    }, y = Math.pow(2, 53) - 1, z = x("length"), A = function(a) {
        var b = z(a);
        return "number" == typeof b && b >= 0 && y >= b;
    };
    s.each = s.forEach = function(a, b, c) {
        b = t(b, c);
        var d, e;
        if (A(a)) for (d = 0, e = a.length; e > d; d++) b(a[d], d, a); else {
            var f = s.keys(a);
            for (d = 0, e = f.length; e > d; d++) b(a[f[d]], f[d], a);
        }
        return a;
    }, s.map = s.collect = function(a, b, c) {
        b = u(b, c);
        for (var d = !A(a) && s.keys(a), e = (d || a).length, f = Array(e), g = 0; e > g; g++) {
            var h = d ? d[g] : g;
            f[g] = b(a[h], h, a);
        }
        return f;
    }, s.reduce = s.foldl = s.inject = a(1), s.reduceRight = s.foldr = a(-1), s.find = s.detect = function(a, b, c) {
        var d;
        return d = A(a) ? s.findIndex(a, b, c) : s.findKey(a, b, c), void 0 !== d && -1 !== d ? a[d] : void 0;
    }, s.filter = s.select = function(a, b, c) {
        var d = [];
        return b = u(b, c), s.each(a, function(a, c, e) {
            b(a, c, e) && d.push(a);
        }), d;
    }, s.reject = function(a, b, c) {
        return s.filter(a, s.negate(u(b)), c);
    }, s.every = s.all = function(a, b, c) {
        b = u(b, c);
        for (var d = !A(a) && s.keys(a), e = (d || a).length, f = 0; e > f; f++) {
            var g = d ? d[f] : f;
            if (!b(a[g], g, a)) return !1;
        }
        return !0;
    }, s.some = s.any = function(a, b, c) {
        b = u(b, c);
        for (var d = !A(a) && s.keys(a), e = (d || a).length, f = 0; e > f; f++) {
            var g = d ? d[f] : f;
            if (b(a[g], g, a)) return !0;
        }
        return !1;
    }, s.contains = s.includes = s.include = function(a, b, c, d) {
        return A(a) || (a = s.values(a)), ("number" != typeof c || d) && (c = 0), s.indexOf(a, b, c) >= 0;
    }, s.invoke = function(a, b) {
        var c = k.call(arguments, 2), d = s.isFunction(b);
        return s.map(a, function(a) {
            var e = d ? b : a[b];
            return null == e ? e : e.apply(a, c);
        });
    }, s.pluck = function(a, b) {
        return s.map(a, s.property(b));
    }, s.where = function(a, b) {
        return s.filter(a, s.matcher(b));
    }, s.findWhere = function(a, b) {
        return s.find(a, s.matcher(b));
    }, s.max = function(a, b, c) {
        var d, e, f = -1 / 0, g = -1 / 0;
        if (null == b && null != a) {
            a = A(a) ? a : s.values(a);
            for (var h = 0, i = a.length; i > h; h++) d = a[h], d > f && (f = d);
        } else b = u(b, c), s.each(a, function(a, c, d) {
            e = b(a, c, d), (e > g || e === -1 / 0 && f === -1 / 0) && (f = a, g = e);
        });
        return f;
    }, s.min = function(a, b, c) {
        var d, e, f = 1 / 0, g = 1 / 0;
        if (null == b && null != a) {
            a = A(a) ? a : s.values(a);
            for (var h = 0, i = a.length; i > h; h++) d = a[h], f > d && (f = d);
        } else b = u(b, c), s.each(a, function(a, c, d) {
            e = b(a, c, d), (g > e || 1 / 0 === e && 1 / 0 === f) && (f = a, g = e);
        });
        return f;
    }, s.shuffle = function(a) {
        for (var b, c = A(a) ? a : s.values(a), d = c.length, e = Array(d), f = 0; d > f; f++) b = s.random(0, f), 
        b !== f && (e[f] = e[b]), e[b] = c[f];
        return e;
    }, s.sample = function(a, b, c) {
        return null == b || c ? (A(a) || (a = s.values(a)), a[s.random(a.length - 1)]) : s.shuffle(a).slice(0, Math.max(0, b));
    }, s.sortBy = function(a, b, c) {
        return b = u(b, c), s.pluck(s.map(a, function(a, c, d) {
            return {
                value: a,
                index: c,
                criteria: b(a, c, d)
            };
        }).sort(function(a, b) {
            var c = a.criteria, d = b.criteria;
            if (c !== d) {
                if (c > d || void 0 === c) return 1;
                if (d > c || void 0 === d) return -1;
            }
            return a.index - b.index;
        }), "value");
    };
    var B = function(a) {
        return function(b, c, d) {
            var e = {};
            return c = u(c, d), s.each(b, function(d, f) {
                var g = c(d, f, b);
                a(e, d, g);
            }), e;
        };
    };
    s.groupBy = B(function(a, b, c) {
        s.has(a, c) ? a[c].push(b) : a[c] = [ b ];
    }), s.indexBy = B(function(a, b, c) {
        a[c] = b;
    }), s.countBy = B(function(a, b, c) {
        s.has(a, c) ? a[c]++ : a[c] = 1;
    }), s.toArray = function(a) {
        return a ? s.isArray(a) ? k.call(a) : A(a) ? s.map(a, s.identity) : s.values(a) : [];
    }, s.size = function(a) {
        return null == a ? 0 : A(a) ? a.length : s.keys(a).length;
    }, s.partition = function(a, b, c) {
        b = u(b, c);
        var d = [], e = [];
        return s.each(a, function(a, c, f) {
            (b(a, c, f) ? d : e).push(a);
        }), [ d, e ];
    }, s.first = s.head = s.take = function(a, b, c) {
        return null == a ? void 0 : null == b || c ? a[0] : s.initial(a, a.length - b);
    }, s.initial = function(a, b, c) {
        return k.call(a, 0, Math.max(0, a.length - (null == b || c ? 1 : b)));
    }, s.last = function(a, b, c) {
        return null == a ? void 0 : null == b || c ? a[a.length - 1] : s.rest(a, Math.max(0, a.length - b));
    }, s.rest = s.tail = s.drop = function(a, b, c) {
        return k.call(a, null == b || c ? 1 : b);
    }, s.compact = function(a) {
        return s.filter(a, s.identity);
    };
    var C = function(a, b, c, d) {
        for (var e = [], f = 0, g = d || 0, h = z(a); h > g; g++) {
            var i = a[g];
            if (A(i) && (s.isArray(i) || s.isArguments(i))) {
                b || (i = C(i, b, c));
                var j = 0, k = i.length;
                for (e.length += k; k > j; ) e[f++] = i[j++];
            } else c || (e[f++] = i);
        }
        return e;
    };
    s.flatten = function(a, b) {
        return C(a, b, !1);
    }, s.without = function(a) {
        return s.difference(a, k.call(arguments, 1));
    }, s.uniq = s.unique = function(a, b, c, d) {
        s.isBoolean(b) || (d = c, c = b, b = !1), null != c && (c = u(c, d));
        for (var e = [], f = [], g = 0, h = z(a); h > g; g++) {
            var i = a[g], j = c ? c(i, g, a) : i;
            b ? (g && f === j || e.push(i), f = j) : c ? s.contains(f, j) || (f.push(j), e.push(i)) : s.contains(e, i) || e.push(i);
        }
        return e;
    }, s.union = function() {
        return s.uniq(C(arguments, !0, !0));
    }, s.intersection = function(a) {
        for (var b = [], c = arguments.length, d = 0, e = z(a); e > d; d++) {
            var f = a[d];
            if (!s.contains(b, f)) {
                for (var g = 1; c > g && s.contains(arguments[g], f); g++) ;
                g === c && b.push(f);
            }
        }
        return b;
    }, s.difference = function(a) {
        var b = C(arguments, !0, !0, 1);
        return s.filter(a, function(a) {
            return !s.contains(b, a);
        });
    }, s.zip = function() {
        return s.unzip(arguments);
    }, s.unzip = function(a) {
        for (var b = a && s.max(a, z).length || 0, c = Array(b), d = 0; b > d; d++) c[d] = s.pluck(a, d);
        return c;
    }, s.object = function(a, b) {
        for (var c = {}, d = 0, e = z(a); e > d; d++) b ? c[a[d]] = b[d] : c[a[d][0]] = a[d][1];
        return c;
    }, s.findIndex = b(1), s.findLastIndex = b(-1), s.sortedIndex = function(a, b, c, d) {
        c = u(c, d, 1);
        for (var e = c(b), f = 0, g = z(a); g > f; ) {
            var h = Math.floor((f + g) / 2);
            c(a[h]) < e ? f = h + 1 : g = h;
        }
        return f;
    }, s.indexOf = c(1, s.findIndex, s.sortedIndex), s.lastIndexOf = c(-1, s.findLastIndex), 
    s.range = function(a, b, c) {
        null == b && (b = a || 0, a = 0), c = c || 1;
        for (var d = Math.max(Math.ceil((b - a) / c), 0), e = Array(d), f = 0; d > f; f++, 
        a += c) e[f] = a;
        return e;
    };
    var D = function(a, b, c, d, e) {
        if (!(d instanceof b)) return a.apply(c, e);
        var f = w(a.prototype), g = a.apply(f, e);
        return s.isObject(g) ? g : f;
    };
    s.bind = function(a, b) {
        if (p && a.bind === p) return p.apply(a, k.call(arguments, 1));
        if (!s.isFunction(a)) throw new TypeError("Bind must be called on a function");
        var c = k.call(arguments, 2), d = function() {
            return D(a, d, b, this, c.concat(k.call(arguments)));
        };
        return d;
    }, s.partial = function(a) {
        var b = k.call(arguments, 1), c = function() {
            for (var d = 0, e = b.length, f = Array(e), g = 0; e > g; g++) f[g] = b[g] === s ? arguments[d++] : b[g];
            for (;d < arguments.length; ) f.push(arguments[d++]);
            return D(a, c, this, this, f);
        };
        return c;
    }, s.bindAll = function(a) {
        var b, c, d = arguments.length;
        if (1 >= d) throw new Error("bindAll must be passed function names");
        for (b = 1; d > b; b++) c = arguments[b], a[c] = s.bind(a[c], a);
        return a;
    }, s.memoize = function(a, b) {
        var c = function(d) {
            var e = c.cache, f = "" + (b ? b.apply(this, arguments) : d);
            return s.has(e, f) || (e[f] = a.apply(this, arguments)), e[f];
        };
        return c.cache = {}, c;
    }, s.delay = function(a, b) {
        var c = k.call(arguments, 2);
        return setTimeout(function() {
            return a.apply(null, c);
        }, b);
    }, s.defer = s.partial(s.delay, s, 1), s.throttle = function(a, b, c) {
        var d, e, f, g = null, h = 0;
        c || (c = {});
        var i = function() {
            h = c.leading === !1 ? 0 : s.now(), g = null, f = a.apply(d, e), g || (d = e = null);
        };
        return function() {
            var j = s.now();
            h || c.leading !== !1 || (h = j);
            var k = b - (j - h);
            return d = this, e = arguments, 0 >= k || k > b ? (g && (clearTimeout(g), g = null), 
            h = j, f = a.apply(d, e), g || (d = e = null)) : g || c.trailing === !1 || (g = setTimeout(i, k)), 
            f;
        };
    }, s.debounce = function(a, b, c) {
        var d, e, f, g, h, i = function() {
            var j = s.now() - g;
            b > j && j >= 0 ? d = setTimeout(i, b - j) : (d = null, c || (h = a.apply(f, e), 
            d || (f = e = null)));
        };
        return function() {
            f = this, e = arguments, g = s.now();
            var j = c && !d;
            return d || (d = setTimeout(i, b)), j && (h = a.apply(f, e), f = e = null), h;
        };
    }, s.wrap = function(a, b) {
        return s.partial(b, a);
    }, s.negate = function(a) {
        return function() {
            return !a.apply(this, arguments);
        };
    }, s.compose = function() {
        var a = arguments, b = a.length - 1;
        return function() {
            for (var c = b, d = a[b].apply(this, arguments); c--; ) d = a[c].call(this, d);
            return d;
        };
    }, s.after = function(a, b) {
        return function() {
            return --a < 1 ? b.apply(this, arguments) : void 0;
        };
    }, s.before = function(a, b) {
        var c;
        return function() {
            return --a > 0 && (c = b.apply(this, arguments)), 1 >= a && (b = null), c;
        };
    }, s.once = s.partial(s.before, 2);
    var E = !{
        toString: null
    }.propertyIsEnumerable("toString"), F = [ "valueOf", "isPrototypeOf", "toString", "propertyIsEnumerable", "hasOwnProperty", "toLocaleString" ];
    s.keys = function(a) {
        if (!s.isObject(a)) return [];
        if (o) return o(a);
        var b = [];
        for (var c in a) s.has(a, c) && b.push(c);
        return E && d(a, b), b;
    }, s.allKeys = function(a) {
        if (!s.isObject(a)) return [];
        var b = [];
        for (var c in a) b.push(c);
        return E && d(a, b), b;
    }, s.values = function(a) {
        for (var b = s.keys(a), c = b.length, d = Array(c), e = 0; c > e; e++) d[e] = a[b[e]];
        return d;
    }, s.mapObject = function(a, b, c) {
        b = u(b, c);
        for (var d, e = s.keys(a), f = e.length, g = {}, h = 0; f > h; h++) d = e[h], g[d] = b(a[d], d, a);
        return g;
    }, s.pairs = function(a) {
        for (var b = s.keys(a), c = b.length, d = Array(c), e = 0; c > e; e++) d[e] = [ b[e], a[b[e]] ];
        return d;
    }, s.invert = function(a) {
        for (var b = {}, c = s.keys(a), d = 0, e = c.length; e > d; d++) b[a[c[d]]] = c[d];
        return b;
    }, s.functions = s.methods = function(a) {
        var b = [];
        for (var c in a) s.isFunction(a[c]) && b.push(c);
        return b.sort();
    }, s.extend = v(s.allKeys), s.extendOwn = s.assign = v(s.keys), s.findKey = function(a, b, c) {
        b = u(b, c);
        for (var d, e = s.keys(a), f = 0, g = e.length; g > f; f++) if (d = e[f], b(a[d], d, a)) return d;
    }, s.pick = function(a, b, c) {
        var d, e, f = {}, g = a;
        if (null == g) return f;
        s.isFunction(b) ? (e = s.allKeys(g), d = t(b, c)) : (e = C(arguments, !1, !1, 1), 
        d = function(a, b, c) {
            return b in c;
        }, g = Object(g));
        for (var h = 0, i = e.length; i > h; h++) {
            var j = e[h], k = g[j];
            d(k, j, g) && (f[j] = k);
        }
        return f;
    }, s.omit = function(a, b, c) {
        if (s.isFunction(b)) b = s.negate(b); else {
            var d = s.map(C(arguments, !1, !1, 1), String);
            b = function(a, b) {
                return !s.contains(d, b);
            };
        }
        return s.pick(a, b, c);
    }, s.defaults = v(s.allKeys, !0), s.create = function(a, b) {
        var c = w(a);
        return b && s.extendOwn(c, b), c;
    }, s.clone = function(a) {
        return s.isObject(a) ? s.isArray(a) ? a.slice() : s.extend({}, a) : a;
    }, s.tap = function(a, b) {
        return b(a), a;
    }, s.isMatch = function(a, b) {
        var c = s.keys(b), d = c.length;
        if (null == a) return !d;
        for (var e = Object(a), f = 0; d > f; f++) {
            var g = c[f];
            if (b[g] !== e[g] || !(g in e)) return !1;
        }
        return !0;
    };
    var G = function(a, b, c, d) {
        if (a === b) return 0 !== a || 1 / a === 1 / b;
        if (null == a || null == b) return a === b;
        a instanceof s && (a = a._wrapped), b instanceof s && (b = b._wrapped);
        var e = l.call(a);
        if (e !== l.call(b)) return !1;
        switch (e) {
          case "[object RegExp]":
          case "[object String]":
            return "" + a == "" + b;

          case "[object Number]":
            return +a !== +a ? +b !== +b : 0 === +a ? 1 / +a === 1 / b : +a === +b;

          case "[object Date]":
          case "[object Boolean]":
            return +a === +b;
        }
        var f = "[object Array]" === e;
        if (!f) {
            if ("object" != typeof a || "object" != typeof b) return !1;
            var g = a.constructor, h = b.constructor;
            if (g !== h && !(s.isFunction(g) && g instanceof g && s.isFunction(h) && h instanceof h) && "constructor" in a && "constructor" in b) return !1;
        }
        c = c || [], d = d || [];
        for (var i = c.length; i--; ) if (c[i] === a) return d[i] === b;
        if (c.push(a), d.push(b), f) {
            if (i = a.length, i !== b.length) return !1;
            for (;i--; ) if (!G(a[i], b[i], c, d)) return !1;
        } else {
            var j, k = s.keys(a);
            if (i = k.length, s.keys(b).length !== i) return !1;
            for (;i--; ) if (j = k[i], !s.has(b, j) || !G(a[j], b[j], c, d)) return !1;
        }
        return c.pop(), d.pop(), !0;
    };
    s.isEqual = function(a, b) {
        return G(a, b);
    }, s.isEmpty = function(a) {
        return null == a ? !0 : A(a) && (s.isArray(a) || s.isString(a) || s.isArguments(a)) ? 0 === a.length : 0 === s.keys(a).length;
    }, s.isElement = function(a) {
        return !(!a || 1 !== a.nodeType);
    }, s.isArray = n || function(a) {
        return "[object Array]" === l.call(a);
    }, s.isObject = function(a) {
        var b = typeof a;
        return "function" === b || "object" === b && !!a;
    }, s.each([ "Arguments", "Function", "String", "Number", "Date", "RegExp", "Error" ], function(a) {
        s["is" + a] = function(b) {
            return l.call(b) === "[object " + a + "]";
        };
    }), s.isArguments(arguments) || (s.isArguments = function(a) {
        return s.has(a, "callee");
    }), "function" != typeof /./ && "object" != typeof Int8Array && (s.isFunction = function(a) {
        return "function" == typeof a || !1;
    }), s.isFinite = function(a) {
        return isFinite(a) && !isNaN(parseFloat(a));
    }, s.isNaN = function(a) {
        return s.isNumber(a) && a !== +a;
    }, s.isBoolean = function(a) {
        return a === !0 || a === !1 || "[object Boolean]" === l.call(a);
    }, s.isNull = function(a) {
        return null === a;
    }, s.isUndefined = function(a) {
        return void 0 === a;
    }, s.has = function(a, b) {
        return null != a && m.call(a, b);
    }, s.noConflict = function() {
        return e._ = f, this;
    }, s.identity = function(a) {
        return a;
    }, s.constant = function(a) {
        return function() {
            return a;
        };
    }, s.noop = function() {}, s.property = x, s.propertyOf = function(a) {
        return null == a ? function() {} : function(b) {
            return a[b];
        };
    }, s.matcher = s.matches = function(a) {
        return a = s.extendOwn({}, a), function(b) {
            return s.isMatch(b, a);
        };
    }, s.times = function(a, b, c) {
        var d = Array(Math.max(0, a));
        b = t(b, c, 1);
        for (var e = 0; a > e; e++) d[e] = b(e);
        return d;
    }, s.random = function(a, b) {
        return null == b && (b = a, a = 0), a + Math.floor(Math.random() * (b - a + 1));
    }, s.now = Date.now || function() {
        return new Date().getTime();
    };
    var H = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#x27;",
        "`": "&#x60;"
    }, I = s.invert(H), J = function(a) {
        var b = function(b) {
            return a[b];
        }, c = "(?:" + s.keys(a).join("|") + ")", d = RegExp(c), e = RegExp(c, "g");
        return function(a) {
            return a = null == a ? "" : "" + a, d.test(a) ? a.replace(e, b) : a;
        };
    };
    s.escape = J(H), s.unescape = J(I), s.result = function(a, b, c) {
        var d = null == a ? void 0 : a[b];
        return void 0 === d && (d = c), s.isFunction(d) ? d.call(a) : d;
    };
    var K = 0;
    s.uniqueId = function(a) {
        var b = ++K + "";
        return a ? a + b : b;
    }, s.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var L = /(.)^/, M = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "\u2028": "u2028",
        "\u2029": "u2029"
    }, N = /\\|'|\r|\n|\u2028|\u2029/g, O = function(a) {
        return "\\" + M[a];
    };
    s.template = function(a, b, c) {
        !b && c && (b = c), b = s.defaults({}, b, s.templateSettings);
        var d = RegExp([ (b.escape || L).source, (b.interpolate || L).source, (b.evaluate || L).source ].join("|") + "|$", "g"), e = 0, f = "__p+='";
        a.replace(d, function(b, c, d, g, h) {
            return f += a.slice(e, h).replace(N, O), e = h + b.length, c ? f += "'+\n((__t=(" + c + "))==null?'':_.escape(__t))+\n'" : d ? f += "'+\n((__t=(" + d + "))==null?'':__t)+\n'" : g && (f += "';\n" + g + "\n__p+='"), 
            b;
        }), f += "';\n", b.variable || (f = "with(obj||{}){\n" + f + "}\n"), f = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + f + "return __p;\n";
        try {
            var g = new Function(b.variable || "obj", "_", f);
        } catch (h) {
            throw h.source = f, h;
        }
        var i = function(a) {
            return g.call(this, a, s);
        }, j = b.variable || "obj";
        return i.source = "function(" + j + "){\n" + f + "}", i;
    }, s.chain = function(a) {
        var b = s(a);
        return b._chain = !0, b;
    };
    var P = function(a, b) {
        return a._chain ? s(b).chain() : b;
    };
    s.mixin = function(a) {
        s.each(s.functions(a), function(b) {
            var c = s[b] = a[b];
            s.prototype[b] = function() {
                var a = [ this._wrapped ];
                return j.apply(a, arguments), P(this, c.apply(s, a));
            };
        });
    }, s.mixin(s), s.each([ "pop", "push", "reverse", "shift", "sort", "splice", "unshift" ], function(a) {
        var b = g[a];
        s.prototype[a] = function() {
            var c = this._wrapped;
            return b.apply(c, arguments), "shift" !== a && "splice" !== a || 0 !== c.length || delete c[0], 
            P(this, c);
        };
    }), s.each([ "concat", "join", "slice" ], function(a) {
        var b = g[a];
        s.prototype[a] = function() {
            return P(this, b.apply(this._wrapped, arguments));
        };
    }), s.prototype.value = function() {
        return this._wrapped;
    }, s.prototype.valueOf = s.prototype.toJSON = s.prototype.value, s.prototype.toString = function() {
        return "" + this._wrapped;
    }, "function" == typeof define && define.amd && define("underscore", [], function() {
        return s;
    });
}.call(this), /*global define: true */
function(a) {
    /*
     * PixelLab Resource Loader
     * Loads resources while providing progress updates.
     */
    function b(a) {
        // merge settings with defaults
        a = a || {}, this.settings = a, // how frequently we poll resources for progress
        null == a.statusInterval && (a.statusInterval = 5e3), // delay before logging since last progress change
        null == a.loggingDelay && (a.loggingDelay = 2e4), // stop waiting if no progress has been made in the moving time window
        null == a.noProgressTimeout && (a.noProgressTimeout = 1 / 0);
        var b, d = [], // holds resources to be loaded with their status
        e = [], f = Date.now(), g = {
            QUEUED: 0,
            WAITING: 1,
            LOADED: 2,
            ERROR: 3,
            TIMEOUT: 4
        }, h = function(a) {
            return null == a ? [] : Array.isArray(a) ? a : [ a ];
        };
        // add an entry to the list of resources to be loaded
        this.add = function(a) {
            // TODO: would be better to create a base class for all resources and
            // initialize the PxLoaderTags there rather than overwritting tags here
            a.tags = new c(a.tags), // ensure priority is set
            null == a.priority && (a.priority = 1 / 0), d.push({
                resource: a,
                status: g.QUEUED
            });
        }, this.addProgressListener = function(a, b) {
            e.push({
                callback: a,
                tags: new c(b)
            });
        }, this.addCompletionListener = function(a, b) {
            e.push({
                tags: new c(b),
                callback: function(b) {
                    b.completedCount === b.totalCount && a(b);
                }
            });
        };
        // creates a comparison function for resources
        var i = function(a) {
            // helper to get the top tag's order for a resource
            a = h(a);
            var b = function(b) {
                for (var c = b.resource, d = 1 / 0, e = 0; e < c.tags.length; e++) for (var f = 0; f < Math.min(a.length, d) && !(c.tags.all[e] === a[f] && d > f && (d = f, 
                0 === d)) && 0 !== d; f++) ;
                return d;
            };
            return function(a, c) {
                // check tag order first
                var d = b(a), e = b(c);
                // now check priority
                return e > d ? -1 : d > e ? 1 : a.priority < c.priority ? -1 : a.priority > c.priority ? 1 : 0;
            };
        };
        this.start = function(a) {
            b = Date.now();
            // first order the resources
            var c = i(a);
            d.sort(c);
            // trigger requests for each resource
            for (var e = 0, f = d.length; f > e; e++) {
                var h = d[e];
                h.status = g.WAITING, h.resource.start(this);
            }
            // do an initial status check soon since items may be loaded from the cache
            setTimeout(j, 100);
        };
        var j = function() {
            for (var b = !1, c = Date.now() - f, e = c >= a.noProgressTimeout, h = c >= a.loggingDelay, i = 0, k = d.length; k > i; i++) {
                var l = d[i];
                l.status === g.WAITING && (// see if the resource has loaded
                l.resource.checkStatus && l.resource.checkStatus(), // if still waiting, mark as timed out or make sure we check again
                l.status === g.WAITING && (e ? l.resource.onTimeout() : b = !0));
            }
            // log any resources that are still pending
            h && b && m(), b && setTimeout(j, a.statusInterval);
        };
        this.isBusy = function() {
            for (var a = 0, b = d.length; b > a; a++) if (d[a].status === g.QUEUED || d[a].status === g.WAITING) return !0;
            return !1;
        };
        var k = function(a, b) {
            var c, h, i, j, k, m = null;
            // find the entry for the resource    
            for (c = 0, h = d.length; h > c; c++) if (d[c].resource === a) {
                m = d[c];
                break;
            }
            // we have already updated the status of the resource
            if (null != m && m.status === g.WAITING) // fire callbacks for interested listeners
            for (m.status = b, f = Date.now(), i = a.tags.length, c = 0, h = e.length; h > c; c++) j = e[c], 
            // no tags specified so always tell the listener
            k = 0 === j.tags.length ? !0 : a.tags.intersects(j.tags), k && l(m, j);
        };
        this.onLoad = function(a) {
            k(a, g.LOADED);
        }, this.onError = function(a) {
            k(a, g.ERROR);
        }, this.onTimeout = function(a) {
            k(a, g.TIMEOUT);
        };
        // sends a progress report to a listener
        var l = function(a, b) {
            // find stats for all the resources the caller is interested in
            var c, e, f, h, i = 0, j = 0;
            for (c = 0, e = d.length; e > c; c++) f = d[c], h = !1, // no tags specified so always tell the listener
            h = 0 === b.tags.length ? !0 : f.resource.tags.intersects(b.tags), h && (j++, (f.status === g.LOADED || f.status === g.ERROR || f.status === g.TIMEOUT) && i++);
            b.callback({
                // info about the resource that changed
                resource: a.resource,
                // should we expose StatusType instead?
                loaded: a.status === g.LOADED,
                error: a.status === g.ERROR,
                timeout: a.status === g.TIMEOUT,
                // updated stats for all resources
                completedCount: i,
                totalCount: j
            });
        }, m = this.log = function(a) {
            if (window.console) {
                var c = Math.round((Date.now() - b) / 1e3);
                window.console.log("PxLoader elapsed: " + c + " sec");
                for (var e = 0, f = d.length; f > e; e++) {
                    var h = d[e];
                    if (a || h.status === g.WAITING) {
                        var i = "PxLoader: #" + e + " " + h.resource.getName();
                        switch (h.status) {
                          case g.QUEUED:
                            i += " (Not Started)";
                            break;

                          case g.WAITING:
                            i += " (Waiting)";
                            break;

                          case g.LOADED:
                            i += " (Loaded)";
                            break;

                          case g.ERROR:
                            i += " (Error)";
                            break;

                          case g.TIMEOUT:
                            i += " (Timeout)";
                        }
                        h.resource.tags.length > 0 && (i += " Tags: [" + h.resource.tags.all.join(",") + "]"), 
                        window.console.log(i);
                    }
                }
            }
        };
    }
    // Tag object to handle tag intersection; once created not meant to be changed
    // Performance rationale: http://jsperf.com/lists-indexof-vs-in-operator/3
    function c(a) {
        if (this.all = [], this.first = null, // cache the first value
        this.length = 0, // holds values as keys for quick lookup
        this.lookup = {}, a) {
            // first fill the array of all values
            if (Array.isArray(a)) // copy the array of values, just to be safe                
            this.all = a.slice(0); else if ("object" == typeof a) for (var b in a) a.hasOwnProperty(b) && this.all.push(b); else this.all.push(a);
            // cache the length and the first value
            this.length = this.all.length, this.length > 0 && (this.first = this.all[0]);
            // set values as object keys for quick lookup during intersection test
            for (var c = 0; c < this.length; c++) this.lookup[this.all[c]] = !0;
        }
    }
    // compare this object with another; return true if they share at least one value
    c.prototype.intersects = function(a) {
        // handle empty values case
        if (0 === this.length || 0 === a.length) return !1;
        // only a single value to compare?
        if (1 === this.length && 1 === a.length) return this.first === a.first;
        // better to loop through the smaller object
        if (a.length < this.length) return a.intersects(this);
        // loop through every key to see if there are any matches
        for (var b in this.lookup) if (a.lookup[b]) return !0;
        return !1;
    }, // AMD module support
    "function" == typeof define && define.amd && define("PxLoader", [], function() {
        return b;
    }), // exports
    a.PxLoader = b;
}(this), // Date.now() shim for older browsers
Date.now || (Date.now = function() {
    return new Date().getTime();
}), // shims to ensure we have newer Array utility methods
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/isArray
Array.isArray || (Array.isArray = function(a) {
    return "[object Array]" === Object.prototype.toString.call(a);
}), // add a convenience method to PxLoader for adding an image
PxLoader.prototype.addImage = function(a, b, c) {
    var d = new PxLoaderImage(a, b, c);
    // return the img element to the caller
    return this.add(d), d.img;
}, // AMD module support
"function" == typeof define && define.amd && define("PxLoaderImage", [], function() {
    return PxLoaderImage;
}), // add a convenience method to PxLoader for adding an image
PxLoader.prototype.addVideo = function(a, b, c) {
    var d = new PxLoaderVideo(a, b, c);
    // return the vid element to the caller
    return this.add(d), d.vid;
}, // AMD module support
"function" == typeof define && define.amd && define("PxLoaderVideo", [], function() {
    return PxLoaderVideo;
}), /*!
* @license SoundJS
* Visit http://createjs.com/ for documentation, updates and examples.
*
* Copyright (c) 2011-2015 gskinner.com, inc.
*
* Distributed under the terms of the MIT license.
* http://www.opensource.org/licenses/mit-license.html
*
* This notice shall be included in all copies or substantial portions of the Software.
*/
/**!
 * SoundJS FlashAudioPlugin also includes swfobject (http://code.google.com/p/swfobject/)
 */
this.createjs = this.createjs || {}, function() {
    var a = createjs.SoundJS = createjs.SoundJS || {};
    a.version = "0.6.1", a.buildDate = "Thu, 21 May 2015 16:17:37 GMT";
}(), this.createjs = this.createjs || {}, createjs.extend = function(a, b) {
    "use strict";
    function c() {
        this.constructor = a;
    }
    return c.prototype = b.prototype, a.prototype = new c();
}, this.createjs = this.createjs || {}, createjs.promote = function(a, b) {
    "use strict";
    var c = a.prototype, d = Object.getPrototypeOf && Object.getPrototypeOf(c) || c.__proto__;
    if (d) {
        c[(b += "_") + "constructor"] = d.constructor;
        for (var e in d) c.hasOwnProperty(e) && "function" == typeof d[e] && (c[b + e] = d[e]);
    }
    return a;
}, this.createjs = this.createjs || {}, createjs.indexOf = function(a, b) {
    "use strict";
    for (var c = 0, d = a.length; d > c; c++) if (b === a[c]) return c;
    return -1;
}, this.createjs = this.createjs || {}, function() {
    "use strict";
    createjs.proxy = function(a, b) {
        var c = Array.prototype.slice.call(arguments, 2);
        return function() {
            return a.apply(b, Array.prototype.slice.call(arguments, 0).concat(c));
        };
    };
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        throw "BrowserDetect cannot be instantiated";
    }
    var b = a.agent = window.navigator.userAgent;
    a.isWindowPhone = b.indexOf("IEMobile") > -1 || b.indexOf("Windows Phone") > -1, 
    a.isFirefox = b.indexOf("Firefox") > -1, a.isOpera = null != window.opera, a.isChrome = b.indexOf("Chrome") > -1, 
    a.isIOS = (b.indexOf("iPod") > -1 || b.indexOf("iPhone") > -1 || b.indexOf("iPad") > -1) && !a.isWindowPhone, 
    a.isAndroid = b.indexOf("Android") > -1 && !a.isWindowPhone, a.isBlackberry = b.indexOf("Blackberry") > -1, 
    createjs.BrowserDetect = a;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        this._listeners = null, this._captureListeners = null;
    }
    var b = a.prototype;
    a.initialize = function(a) {
        a.addEventListener = b.addEventListener, a.on = b.on, a.removeEventListener = a.off = b.removeEventListener, 
        a.removeAllEventListeners = b.removeAllEventListeners, a.hasEventListener = b.hasEventListener, 
        a.dispatchEvent = b.dispatchEvent, a._dispatchEvent = b._dispatchEvent, a.willTrigger = b.willTrigger;
    }, b.addEventListener = function(a, b, c) {
        var d;
        d = c ? this._captureListeners = this._captureListeners || {} : this._listeners = this._listeners || {};
        var e = d[a];
        return e && this.removeEventListener(a, b, c), e = d[a], e ? e.push(b) : d[a] = [ b ], 
        b;
    }, b.on = function(a, b, c, d, e, f) {
        return b.handleEvent && (c = c || b, b = b.handleEvent), c = c || this, this.addEventListener(a, function(a) {
            b.call(c, a, e), d && a.remove();
        }, f);
    }, b.removeEventListener = function(a, b, c) {
        var d = c ? this._captureListeners : this._listeners;
        if (d) {
            var e = d[a];
            if (e) for (var f = 0, g = e.length; g > f; f++) if (e[f] == b) {
                1 == g ? delete d[a] : e.splice(f, 1);
                break;
            }
        }
    }, b.off = b.removeEventListener, b.removeAllEventListeners = function(a) {
        a ? (this._listeners && delete this._listeners[a], this._captureListeners && delete this._captureListeners[a]) : this._listeners = this._captureListeners = null;
    }, b.dispatchEvent = function(a) {
        if ("string" == typeof a) {
            var b = this._listeners;
            if (!b || !b[a]) return !1;
            a = new createjs.Event(a);
        } else a.target && a.clone && (a = a.clone());
        try {
            a.target = this;
        } catch (c) {}
        if (a.bubbles && this.parent) {
            for (var d = this, e = [ d ]; d.parent; ) e.push(d = d.parent);
            var f, g = e.length;
            for (f = g - 1; f >= 0 && !a.propagationStopped; f--) e[f]._dispatchEvent(a, 1 + (0 == f));
            for (f = 1; g > f && !a.propagationStopped; f++) e[f]._dispatchEvent(a, 3);
        } else this._dispatchEvent(a, 2);
        return a.defaultPrevented;
    }, b.hasEventListener = function(a) {
        var b = this._listeners, c = this._captureListeners;
        return !!(b && b[a] || c && c[a]);
    }, b.willTrigger = function(a) {
        for (var b = this; b; ) {
            if (b.hasEventListener(a)) return !0;
            b = b.parent;
        }
        return !1;
    }, b.toString = function() {
        return "[EventDispatcher]";
    }, b._dispatchEvent = function(a, b) {
        var c, d = 1 == b ? this._captureListeners : this._listeners;
        if (a && d) {
            var e = d[a.type];
            if (!e || !(c = e.length)) return;
            try {
                a.currentTarget = this;
            } catch (f) {}
            try {
                a.eventPhase = b;
            } catch (f) {}
            a.removed = !1, e = e.slice();
            for (var g = 0; c > g && !a.immediatePropagationStopped; g++) {
                var h = e[g];
                h.handleEvent ? h.handleEvent(a) : h(a), a.removed && (this.off(a.type, h, 1 == b), 
                a.removed = !1);
            }
        }
    }, createjs.EventDispatcher = a;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c) {
        this.type = a, this.target = null, this.currentTarget = null, this.eventPhase = 0, 
        this.bubbles = !!b, this.cancelable = !!c, this.timeStamp = new Date().getTime(), 
        this.defaultPrevented = !1, this.propagationStopped = !1, this.immediatePropagationStopped = !1, 
        this.removed = !1;
    }
    var b = a.prototype;
    b.preventDefault = function() {
        this.defaultPrevented = this.cancelable && !0;
    }, b.stopPropagation = function() {
        this.propagationStopped = !0;
    }, b.stopImmediatePropagation = function() {
        this.immediatePropagationStopped = this.propagationStopped = !0;
    }, b.remove = function() {
        this.removed = !0;
    }, b.clone = function() {
        return new a(this.type, this.bubbles, this.cancelable);
    }, b.set = function(a) {
        for (var b in a) this[b] = a[b];
        return this;
    }, b.toString = function() {
        return "[Event (type=" + this.type + ")]";
    }, createjs.Event = a;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c) {
        this.Event_constructor("error"), this.title = a, this.message = b, this.data = c;
    }
    var b = createjs.extend(a, createjs.Event);
    b.clone = function() {
        return new createjs.ErrorEvent(this.title, this.message, this.data);
    }, createjs.ErrorEvent = createjs.promote(a, "Event");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b) {
        this.Event_constructor("progress"), this.loaded = a, this.total = null == b ? 1 : b, 
        this.progress = 0 == b ? 0 : this.loaded / this.total;
    }
    var b = createjs.extend(a, createjs.Event);
    b.clone = function() {
        return new createjs.ProgressEvent(this.loaded, this.total);
    }, createjs.ProgressEvent = createjs.promote(a, "Event");
}(window), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        this.src = null, this.type = null, this.id = null, this.maintainOrder = !1, this.callback = null, 
        this.data = null, this.method = createjs.LoadItem.GET, this.values = null, this.headers = null, 
        this.withCredentials = !1, this.mimeType = null, this.crossOrigin = null, this.loadTimeout = c.LOAD_TIMEOUT_DEFAULT;
    }
    var b = a.prototype = {}, c = a;
    c.LOAD_TIMEOUT_DEFAULT = 8e3, c.create = function(b) {
        if ("string" == typeof b) {
            var d = new a();
            return d.src = b, d;
        }
        if (b instanceof c) return b;
        if (b instanceof Object && b.src) return null == b.loadTimeout && (b.loadTimeout = c.LOAD_TIMEOUT_DEFAULT), 
        b;
        throw new Error("Type not recognized.");
    }, b.set = function(a) {
        for (var b in a) this[b] = a[b];
        return this;
    }, createjs.LoadItem = c;
}(), function() {
    var a = {};
    a.ABSOLUTE_PATT = /^(?:\w+:)?\/{2}/i, a.RELATIVE_PATT = /^[.\/]*?\//i, a.EXTENSION_PATT = /\/?[^\/]+\.(\w{1,5})$/i, 
    a.parseURI = function(b) {
        var c = {
            absolute: !1,
            relative: !1
        };
        if (null == b) return c;
        var d = b.indexOf("?");
        d > -1 && (b = b.substr(0, d));
        var e;
        return a.ABSOLUTE_PATT.test(b) ? c.absolute = !0 : a.RELATIVE_PATT.test(b) && (c.relative = !0), 
        (e = b.match(a.EXTENSION_PATT)) && (c.extension = e[1].toLowerCase()), c;
    }, a.formatQueryString = function(a, b) {
        if (null == a) throw new Error("You must specify data.");
        var c = [];
        for (var d in a) c.push(d + "=" + escape(a[d]));
        return b && (c = c.concat(b)), c.join("&");
    }, a.buildPath = function(a, b) {
        if (null == b) return a;
        var c = [], d = a.indexOf("?");
        if (-1 != d) {
            var e = a.slice(d + 1);
            c = c.concat(e.split("&"));
        }
        return -1 != d ? a.slice(0, d) + "?" + this._formatQueryString(b, c) : a + "?" + this._formatQueryString(b, c);
    }, a.isCrossDomain = function(a) {
        var b = document.createElement("a");
        b.href = a.src;
        var c = document.createElement("a");
        c.href = location.href;
        var d = "" != b.hostname && (b.port != c.port || b.protocol != c.protocol || b.hostname != c.hostname);
        return d;
    }, a.isLocal = function(a) {
        var b = document.createElement("a");
        return b.href = a.src, "" == b.hostname && "file:" == b.protocol;
    }, a.isBinary = function(a) {
        switch (a) {
          case createjs.AbstractLoader.IMAGE:
          case createjs.AbstractLoader.BINARY:
            return !0;

          default:
            return !1;
        }
    }, a.isImageTag = function(a) {
        return a instanceof HTMLImageElement;
    }, a.isAudioTag = function(a) {
        return window.HTMLAudioElement ? a instanceof HTMLAudioElement : !1;
    }, a.isVideoTag = function(a) {
        return window.HTMLVideoElement ? a instanceof HTMLVideoElement : !1;
    }, a.isText = function(a) {
        switch (a) {
          case createjs.AbstractLoader.TEXT:
          case createjs.AbstractLoader.JSON:
          case createjs.AbstractLoader.MANIFEST:
          case createjs.AbstractLoader.XML:
          case createjs.AbstractLoader.CSS:
          case createjs.AbstractLoader.SVG:
          case createjs.AbstractLoader.JAVASCRIPT:
          case createjs.AbstractLoader.SPRITESHEET:
            return !0;

          default:
            return !1;
        }
    }, a.getTypeByExtension = function(a) {
        if (null == a) return createjs.AbstractLoader.TEXT;
        switch (a.toLowerCase()) {
          case "jpeg":
          case "jpg":
          case "gif":
          case "png":
          case "webp":
          case "bmp":
            return createjs.AbstractLoader.IMAGE;

          case "ogg":
          case "mp3":
          case "webm":
            return createjs.AbstractLoader.SOUND;

          case "mp4":
          case "webm":
          case "ts":
            return createjs.AbstractLoader.VIDEO;

          case "json":
            return createjs.AbstractLoader.JSON;

          case "xml":
            return createjs.AbstractLoader.XML;

          case "css":
            return createjs.AbstractLoader.CSS;

          case "js":
            return createjs.AbstractLoader.JAVASCRIPT;

          case "svg":
            return createjs.AbstractLoader.SVG;

          default:
            return createjs.AbstractLoader.TEXT;
        }
    }, createjs.RequestUtils = a;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c) {
        this.EventDispatcher_constructor(), this.loaded = !1, this.canceled = !1, this.progress = 0, 
        this.type = c, this.resultFormatter = null, this._item = a ? createjs.LoadItem.create(a) : null, 
        this._preferXHR = b, this._result = null, this._rawResult = null, this._loadedItems = null, 
        this._tagSrcAttribute = null, this._tag = null;
    }
    var b = createjs.extend(a, createjs.EventDispatcher), c = a;
    c.POST = "POST", c.GET = "GET", c.BINARY = "binary", c.CSS = "css", c.IMAGE = "image", 
    c.JAVASCRIPT = "javascript", c.JSON = "json", c.JSONP = "jsonp", c.MANIFEST = "manifest", 
    c.SOUND = "sound", c.VIDEO = "video", c.SPRITESHEET = "spritesheet", c.SVG = "svg", 
    c.TEXT = "text", c.XML = "xml", b.getItem = function() {
        return this._item;
    }, b.getResult = function(a) {
        return a ? this._rawResult : this._result;
    }, b.getTag = function() {
        return this._tag;
    }, b.setTag = function(a) {
        this._tag = a;
    }, b.load = function() {
        this._createRequest(), this._request.on("complete", this, this), this._request.on("progress", this, this), 
        this._request.on("loadStart", this, this), this._request.on("abort", this, this), 
        this._request.on("timeout", this, this), this._request.on("error", this, this);
        var a = new createjs.Event("initialize");
        a.loader = this._request, this.dispatchEvent(a), this._request.load();
    }, b.cancel = function() {
        this.canceled = !0, this.destroy();
    }, b.destroy = function() {
        this._request && (this._request.removeAllEventListeners(), this._request.destroy()), 
        this._request = null, this._item = null, this._rawResult = null, this._result = null, 
        this._loadItems = null, this.removeAllEventListeners();
    }, b.getLoadedItems = function() {
        return this._loadedItems;
    }, b._createRequest = function() {
        this._request = this._preferXHR ? new createjs.XHRRequest(this._item) : new createjs.TagRequest(this._item, this._tag || this._createTag(), this._tagSrcAttribute);
    }, b._createTag = function() {
        return null;
    }, b._sendLoadStart = function() {
        this._isCanceled() || this.dispatchEvent("loadstart");
    }, b._sendProgress = function(a) {
        if (!this._isCanceled()) {
            var b = null;
            "number" == typeof a ? (this.progress = a, b = new createjs.ProgressEvent(this.progress)) : (b = a, 
            this.progress = a.loaded / a.total, b.progress = this.progress, (isNaN(this.progress) || 1 / 0 == this.progress) && (this.progress = 0)), 
            this.hasEventListener("progress") && this.dispatchEvent(b);
        }
    }, b._sendComplete = function() {
        if (!this._isCanceled()) {
            this.loaded = !0;
            var a = new createjs.Event("complete");
            a.rawResult = this._rawResult, null != this._result && (a.result = this._result), 
            this.dispatchEvent(a);
        }
    }, b._sendError = function(a) {
        !this._isCanceled() && this.hasEventListener("error") && (null == a && (a = new createjs.ErrorEvent("PRELOAD_ERROR_EMPTY")), 
        this.dispatchEvent(a));
    }, b._isCanceled = function() {
        return null == window.createjs || this.canceled ? !0 : !1;
    }, b.resultFormatter = null, b.handleEvent = function(a) {
        switch (a.type) {
          case "complete":
            this._rawResult = a.target._response;
            var b = this.resultFormatter && this.resultFormatter(this), c = this;
            b instanceof Function ? b(function(a) {
                c._result = a, c._sendComplete();
            }) : (this._result = b || this._rawResult, this._sendComplete());
            break;

          case "progress":
            this._sendProgress(a);
            break;

          case "error":
            this._sendError(a);
            break;

          case "loadstart":
            this._sendLoadStart();
            break;

          case "abort":
          case "timeout":
            this._isCanceled() || this.dispatchEvent(a.type);
        }
    }, b.buildPath = function(a, b) {
        return createjs.RequestUtils.buildPath(a, b);
    }, b.toString = function() {
        return "[PreloadJS AbstractLoader]";
    }, createjs.AbstractLoader = createjs.promote(a, "EventDispatcher");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c) {
        this.AbstractLoader_constructor(a, b, c), this.resultFormatter = this._formatResult, 
        this._tagSrcAttribute = "src";
    }
    var b = createjs.extend(a, createjs.AbstractLoader);
    b.load = function() {
        this._tag || (this._tag = this._createTag(this._item.src)), this._tag.preload = "auto", 
        this._tag.load(), this.AbstractLoader_load();
    }, b._createTag = function() {}, b._createRequest = function() {
        this._request = this._preferXHR ? new createjs.XHRRequest(this._item) : new createjs.MediaTagRequest(this._item, this._tag || this._createTag(), this._tagSrcAttribute);
    }, b._formatResult = function(a) {
        return this._tag.removeEventListener && this._tag.removeEventListener("canplaythrough", this._loadedHandler), 
        this._tag.onstalled = null, this._preferXHR && (a.getTag().src = a.getResult(!0)), 
        a.getTag();
    }, createjs.AbstractMediaLoader = createjs.promote(a, "AbstractLoader");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    var a = function(a) {
        this._item = a;
    }, b = createjs.extend(a, createjs.EventDispatcher);
    b.load = function() {}, b.destroy = function() {}, b.cancel = function() {}, createjs.AbstractRequest = createjs.promote(a, "EventDispatcher");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c) {
        this.AbstractRequest_constructor(a), this._tag = b, this._tagSrcAttribute = c, this._loadedHandler = createjs.proxy(this._handleTagComplete, this), 
        this._addedToDOM = !1, this._startTagVisibility = null;
    }
    var b = createjs.extend(a, createjs.AbstractRequest);
    b.load = function() {
        this._tag.onload = createjs.proxy(this._handleTagComplete, this), this._tag.onreadystatechange = createjs.proxy(this._handleReadyStateChange, this), 
        this._tag.onerror = createjs.proxy(this._handleError, this);
        var a = new createjs.Event("initialize");
        a.loader = this._tag, this.dispatchEvent(a), this._hideTag(), this._loadTimeout = setTimeout(createjs.proxy(this._handleTimeout, this), this._item.loadTimeout), 
        this._tag[this._tagSrcAttribute] = this._item.src, null == this._tag.parentNode && (window.document.body.appendChild(this._tag), 
        this._addedToDOM = !0);
    }, b.destroy = function() {
        this._clean(), this._tag = null, this.AbstractRequest_destroy();
    }, b._handleReadyStateChange = function() {
        clearTimeout(this._loadTimeout);
        var a = this._tag;
        ("loaded" == a.readyState || "complete" == a.readyState) && this._handleTagComplete();
    }, b._handleError = function() {
        this._clean(), this.dispatchEvent("error");
    }, b._handleTagComplete = function() {
        this._rawResult = this._tag, this._result = this.resultFormatter && this.resultFormatter(this) || this._rawResult, 
        this._clean(), this._showTag(), this.dispatchEvent("complete");
    }, b._handleTimeout = function() {
        this._clean(), this.dispatchEvent(new createjs.Event("timeout"));
    }, b._clean = function() {
        this._tag.onload = null, this._tag.onreadystatechange = null, this._tag.onerror = null, 
        this._addedToDOM && null != this._tag.parentNode && this._tag.parentNode.removeChild(this._tag), 
        clearTimeout(this._loadTimeout);
    }, b._hideTag = function() {
        this._startTagVisibility = this._tag.style.visibility, this._tag.style.visibility = "hidden";
    }, b._showTag = function() {
        this._tag.style.visibility = this._startTagVisibility;
    }, b._handleStalled = function() {}, createjs.TagRequest = createjs.promote(a, "AbstractRequest");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c) {
        this.AbstractRequest_constructor(a), this._tag = b, this._tagSrcAttribute = c, this._loadedHandler = createjs.proxy(this._handleTagComplete, this);
    }
    var b = createjs.extend(a, createjs.TagRequest);
    b.load = function() {
        var a = createjs.proxy(this._handleStalled, this);
        this._stalledCallback = a;
        var b = createjs.proxy(this._handleProgress, this);
        this._handleProgress = b, this._tag.addEventListener("stalled", a), this._tag.addEventListener("progress", b), 
        this._tag.addEventListener && this._tag.addEventListener("canplaythrough", this._loadedHandler, !1), 
        this.TagRequest_load();
    }, b._handleReadyStateChange = function() {
        clearTimeout(this._loadTimeout);
        var a = this._tag;
        ("loaded" == a.readyState || "complete" == a.readyState) && this._handleTagComplete();
    }, b._handleStalled = function() {}, b._handleProgress = function(a) {
        if (a && !(a.loaded > 0 && 0 == a.total)) {
            var b = new createjs.ProgressEvent(a.loaded, a.total);
            this.dispatchEvent(b);
        }
    }, b._clean = function() {
        this._tag.removeEventListener && this._tag.removeEventListener("canplaythrough", this._loadedHandler), 
        this._tag.removeEventListener("stalled", this._stalledCallback), this._tag.removeEventListener("progress", this._progressCallback), 
        this.TagRequest__clean();
    }, createjs.MediaTagRequest = createjs.promote(a, "TagRequest");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a) {
        this.AbstractRequest_constructor(a), this._request = null, this._loadTimeout = null, 
        this._xhrLevel = 1, this._response = null, this._rawResponse = null, this._canceled = !1, 
        this._handleLoadStartProxy = createjs.proxy(this._handleLoadStart, this), this._handleProgressProxy = createjs.proxy(this._handleProgress, this), 
        this._handleAbortProxy = createjs.proxy(this._handleAbort, this), this._handleErrorProxy = createjs.proxy(this._handleError, this), 
        this._handleTimeoutProxy = createjs.proxy(this._handleTimeout, this), this._handleLoadProxy = createjs.proxy(this._handleLoad, this), 
        this._handleReadyStateChangeProxy = createjs.proxy(this._handleReadyStateChange, this), 
        !this._createXHR(a);
    }
    var b = createjs.extend(a, createjs.AbstractRequest);
    a.ACTIVEX_VERSIONS = [ "Msxml2.XMLHTTP.6.0", "Msxml2.XMLHTTP.5.0", "Msxml2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP" ], 
    b.getResult = function(a) {
        return a && this._rawResponse ? this._rawResponse : this._response;
    }, b.cancel = function() {
        this.canceled = !0, this._clean(), this._request.abort();
    }, b.load = function() {
        if (null == this._request) return void this._handleError();
        this._request.addEventListener("loadstart", this._handleLoadStartProxy, !1), this._request.addEventListener("progress", this._handleProgressProxy, !1), 
        this._request.addEventListener("abort", this._handleAbortProxy, !1), this._request.addEventListener("error", this._handleErrorProxy, !1), 
        this._request.addEventListener("timeout", this._handleTimeoutProxy, !1), this._request.addEventListener("load", this._handleLoadProxy, !1), 
        this._request.addEventListener("readystatechange", this._handleReadyStateChangeProxy, !1), 
        1 == this._xhrLevel && (this._loadTimeout = setTimeout(createjs.proxy(this._handleTimeout, this), this._item.loadTimeout));
        try {
            this._item.values && this._item.method != createjs.AbstractLoader.GET ? this._item.method == createjs.AbstractLoader.POST && this._request.send(createjs.RequestUtils.formatQueryString(this._item.values)) : this._request.send();
        } catch (a) {
            this.dispatchEvent(new createjs.ErrorEvent("XHR_SEND", null, a));
        }
    }, b.setResponseType = function(a) {
        this._request.responseType = a;
    }, b.getAllResponseHeaders = function() {
        return this._request.getAllResponseHeaders instanceof Function ? this._request.getAllResponseHeaders() : null;
    }, b.getResponseHeader = function(a) {
        return this._request.getResponseHeader instanceof Function ? this._request.getResponseHeader(a) : null;
    }, b._handleProgress = function(a) {
        if (a && !(a.loaded > 0 && 0 == a.total)) {
            var b = new createjs.ProgressEvent(a.loaded, a.total);
            this.dispatchEvent(b);
        }
    }, b._handleLoadStart = function() {
        clearTimeout(this._loadTimeout), this.dispatchEvent("loadstart");
    }, b._handleAbort = function(a) {
        this._clean(), this.dispatchEvent(new createjs.ErrorEvent("XHR_ABORTED", null, a));
    }, b._handleError = function(a) {
        this._clean(), this.dispatchEvent(new createjs.ErrorEvent(a.message));
    }, b._handleReadyStateChange = function() {
        4 == this._request.readyState && this._handleLoad();
    }, b._handleLoad = function() {
        if (!this.loaded) {
            this.loaded = !0;
            var a = this._checkError();
            if (a) return void this._handleError(a);
            this._response = this._getResponse(), this._clean(), this.dispatchEvent(new createjs.Event("complete"));
        }
    }, b._handleTimeout = function(a) {
        this._clean(), this.dispatchEvent(new createjs.ErrorEvent("PRELOAD_TIMEOUT", null, a));
    }, b._checkError = function() {
        var a = parseInt(this._request.status);
        switch (a) {
          case 404:
          case 0:
            return new Error(a);
        }
        return null;
    }, b._getResponse = function() {
        if (null != this._response) return this._response;
        if (null != this._request.response) return this._request.response;
        try {
            if (null != this._request.responseText) return this._request.responseText;
        } catch (a) {}
        try {
            if (null != this._request.responseXML) return this._request.responseXML;
        } catch (a) {}
        return null;
    }, b._createXHR = function(a) {
        var b = createjs.RequestUtils.isCrossDomain(a), c = {}, d = null;
        if (window.XMLHttpRequest) d = new XMLHttpRequest(), b && void 0 === d.withCredentials && window.XDomainRequest && (d = new XDomainRequest()); else {
            for (var e = 0, f = s.ACTIVEX_VERSIONS.length; f > e; e++) {
                s.ACTIVEX_VERSIONS[e];
                try {
                    d = new ActiveXObject(axVersions);
                    break;
                } catch (g) {}
            }
            if (null == d) return !1;
        }
        null == a.mimeType && createjs.RequestUtils.isText(a.type) && (a.mimeType = "text/plain; charset=utf-8"), 
        a.mimeType && d.overrideMimeType && d.overrideMimeType(a.mimeType), this._xhrLevel = "string" == typeof d.responseType ? 2 : 1;
        var h = null;
        if (h = a.method == createjs.AbstractLoader.GET ? createjs.RequestUtils.buildPath(a.src, a.values) : a.src, 
        d.open(a.method || createjs.AbstractLoader.GET, h, !0), b && d instanceof XMLHttpRequest && 1 == this._xhrLevel && (c.Origin = location.origin), 
        a.values && a.method == createjs.AbstractLoader.POST && (c["Content-Type"] = "application/x-www-form-urlencoded"), 
        b || c["X-Requested-With"] || (c["X-Requested-With"] = "XMLHttpRequest"), a.headers) for (var i in a.headers) c[i] = a.headers[i];
        for (i in c) d.setRequestHeader(i, c[i]);
        return d instanceof XMLHttpRequest && void 0 !== a.withCredentials && (d.withCredentials = a.withCredentials), 
        this._request = d, !0;
    }, b._clean = function() {
        clearTimeout(this._loadTimeout), this._request.removeEventListener("loadstart", this._handleLoadStartProxy), 
        this._request.removeEventListener("progress", this._handleProgressProxy), this._request.removeEventListener("abort", this._handleAbortProxy), 
        this._request.removeEventListener("error", this._handleErrorProxy), this._request.removeEventListener("timeout", this._handleTimeoutProxy), 
        this._request.removeEventListener("load", this._handleLoadProxy), this._request.removeEventListener("readystatechange", this._handleReadyStateChangeProxy);
    }, b.toString = function() {
        return "[PreloadJS XHRRequest]";
    }, createjs.XHRRequest = createjs.promote(a, "AbstractRequest");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b) {
        this.AbstractMediaLoader_constructor(a, b, createjs.AbstractLoader.SOUND), createjs.RequestUtils.isAudioTag(a) ? this._tag = a : createjs.RequestUtils.isAudioTag(a.src) ? this._tag = a : createjs.RequestUtils.isAudioTag(a.tag) && (this._tag = createjs.RequestUtils.isAudioTag(a) ? a : a.src), 
        null != this._tag && (this._preferXHR = !1);
    }
    var b = createjs.extend(a, createjs.AbstractMediaLoader), c = a;
    c.canLoadItem = function(a) {
        return a.type == createjs.AbstractLoader.SOUND;
    }, b._createTag = function(a) {
        var b = document.createElement("audio");
        return b.autoplay = !1, b.preload = "none", b.src = a, b;
    }, createjs.SoundLoader = createjs.promote(a, "AbstractMediaLoader");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    var a = function() {
        this.interrupt = null, this.delay = null, this.offset = null, this.loop = null, 
        this.volume = null, this.pan = null, this.startTime = null, this.duration = null;
    }, b = a.prototype = {}, c = a;
    c.create = function(a) {
        if (a instanceof c || a instanceof Object) {
            var b = new createjs.PlayPropsConfig();
            return b.set(a), b;
        }
        throw new Error("Type not recognized.");
    }, b.set = function(a) {
        for (var b in a) this[b] = a[b];
        return this;
    }, b.toString = function() {
        return "[PlayPropsConfig]";
    }, createjs.PlayPropsConfig = c;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        throw "Sound cannot be instantiated";
    }
    function b(a, b) {
        this.init(a, b);
    }
    var c = a;
    c.INTERRUPT_ANY = "any", c.INTERRUPT_EARLY = "early", c.INTERRUPT_LATE = "late", 
    c.INTERRUPT_NONE = "none", c.PLAY_INITED = "playInited", c.PLAY_SUCCEEDED = "playSucceeded", 
    c.PLAY_INTERRUPTED = "playInterrupted", c.PLAY_FINISHED = "playFinished", c.PLAY_FAILED = "playFailed", 
    c.SUPPORTED_EXTENSIONS = [ "mp3", "ogg", "opus", "mpeg", "wav", "m4a", "mp4", "aiff", "wma", "mid" ], 
    c.EXTENSION_MAP = {
        m4a: "mp4"
    }, c.FILE_PATTERN = /^(?:(\w+:)\/{2}(\w+(?:\.\w+)*\/?))?([\/.]*?(?:[^?]+)?\/)?((?:[^\/?]+)\.(\w+))(?:\?(\S+)?)?$/, 
    c.defaultInterruptBehavior = c.INTERRUPT_NONE, c.alternateExtensions = [], c.activePlugin = null, 
    c._masterVolume = 1, Object.defineProperty(c, "volume", {
        get: function() {
            return this._masterVolume;
        },
        set: function(a) {
            if (null == Number(a)) return !1;
            if (a = Math.max(0, Math.min(1, a)), c._masterVolume = a, !this.activePlugin || !this.activePlugin.setVolume || !this.activePlugin.setVolume(a)) for (var b = this._instances, d = 0, e = b.length; e > d; d++) b[d].setMasterVolume(a);
        }
    }), c._masterMute = !1, Object.defineProperty(c, "muted", {
        get: function() {
            return this._masterMute;
        },
        set: function(a) {
            if (null == a) return !1;
            if (this._masterMute = a, !this.activePlugin || !this.activePlugin.setMute || !this.activePlugin.setMute(a)) for (var b = this._instances, c = 0, d = b.length; d > c; c++) b[c].setMasterMute(a);
            return !0;
        }
    }), Object.defineProperty(c, "capabilities", {
        get: function() {
            return null == c.activePlugin ? null : c.activePlugin._capabilities;
        },
        set: function() {
            return !1;
        }
    }), c._pluginsRegistered = !1, c._lastID = 0, c._instances = [], c._idHash = {}, 
    c._preloadHash = {}, c._defaultPlayPropsHash = {}, c.addEventListener = null, c.removeEventListener = null, 
    c.removeAllEventListeners = null, c.dispatchEvent = null, c.hasEventListener = null, 
    c._listeners = null, createjs.EventDispatcher.initialize(c), c.getPreloadHandlers = function() {
        return {
            callback: createjs.proxy(c.initLoad, c),
            types: [ "sound" ],
            extensions: c.SUPPORTED_EXTENSIONS
        };
    }, c._handleLoadComplete = function(a) {
        var b = a.target.getItem().src;
        if (c._preloadHash[b]) for (var d = 0, e = c._preloadHash[b].length; e > d; d++) {
            var f = c._preloadHash[b][d];
            if (c._preloadHash[b][d] = !0, c.hasEventListener("fileload")) {
                var a = new createjs.Event("fileload");
                a.src = f.src, a.id = f.id, a.data = f.data, a.sprite = f.sprite, c.dispatchEvent(a);
            }
        }
    }, c._handleLoadError = function(a) {
        var b = a.target.getItem().src;
        if (c._preloadHash[b]) for (var d = 0, e = c._preloadHash[b].length; e > d; d++) {
            var f = c._preloadHash[b][d];
            if (c._preloadHash[b][d] = !1, c.hasEventListener("fileerror")) {
                var a = new createjs.Event("fileerror");
                a.src = f.src, a.id = f.id, a.data = f.data, a.sprite = f.sprite, c.dispatchEvent(a);
            }
        }
    }, c._registerPlugin = function(a) {
        return a.isSupported() ? (c.activePlugin = new a(), !0) : !1;
    }, c.registerPlugins = function(a) {
        c._pluginsRegistered = !0;
        for (var b = 0, d = a.length; d > b; b++) if (c._registerPlugin(a[b])) return !0;
        return !1;
    }, c.initializeDefaultPlugins = function() {
        return null != c.activePlugin ? !0 : c._pluginsRegistered ? !1 : c.registerPlugins([ createjs.WebAudioPlugin, createjs.HTMLAudioPlugin ]) ? !0 : !1;
    }, c.isReady = function() {
        return null != c.activePlugin;
    }, c.getCapabilities = function() {
        return null == c.activePlugin ? null : c.activePlugin._capabilities;
    }, c.getCapability = function(a) {
        return null == c.activePlugin ? null : c.activePlugin._capabilities[a];
    }, c.initLoad = function(a) {
        return c._registerSound(a);
    }, c._registerSound = function(a) {
        if (!c.initializeDefaultPlugins()) return !1;
        var d;
        if (a.src instanceof Object ? (d = c._parseSrc(a.src), d.src = a.path + d.src) : d = c._parsePath(a.src), 
        null == d) return !1;
        a.src = d.src, a.type = "sound";
        var e = a.data, f = null;
        if (null != e && (isNaN(e.channels) ? isNaN(e) || (f = parseInt(e)) : f = parseInt(e.channels), 
        e.audioSprite)) for (var g, h = e.audioSprite.length; h--; ) g = e.audioSprite[h], 
        c._idHash[g.id] = {
            src: a.src,
            startTime: parseInt(g.startTime),
            duration: parseInt(g.duration)
        }, g.defaultPlayProps && (c._defaultPlayPropsHash[g.id] = createjs.PlayPropsConfig.create(g.defaultPlayProps));
        null != a.id && (c._idHash[a.id] = {
            src: a.src
        });
        var i = c.activePlugin.register(a);
        return b.create(a.src, f), null != e && isNaN(e) ? a.data.channels = f || b.maxPerChannel() : a.data = f || b.maxPerChannel(), 
        i.type && (a.type = i.type), a.defaultPlayProps && (c._defaultPlayPropsHash[a.src] = createjs.PlayPropsConfig.create(a.defaultPlayProps)), 
        i;
    }, c.registerSound = function(a, b, d, e, f) {
        var g = {
            src: a,
            id: b,
            data: d,
            defaultPlayProps: f
        };
        a instanceof Object && a.src && (e = b, g = a), g = createjs.LoadItem.create(g), 
        g.path = e, null == e || g.src instanceof Object || (g.src = e + a);
        var h = c._registerSound(g);
        if (!h) return !1;
        if (c._preloadHash[g.src] || (c._preloadHash[g.src] = []), c._preloadHash[g.src].push(g), 
        1 == c._preloadHash[g.src].length) h.on("complete", createjs.proxy(this._handleLoadComplete, this)), 
        h.on("error", createjs.proxy(this._handleLoadError, this)), c.activePlugin.preload(h); else if (1 == c._preloadHash[g.src][0]) return !0;
        return g;
    }, c.registerSounds = function(a, b) {
        var c = [];
        a.path && (b ? b += a.path : b = a.path, a = a.manifest);
        for (var d = 0, e = a.length; e > d; d++) c[d] = createjs.Sound.registerSound(a[d].src, a[d].id, a[d].data, b, a[d].defaultPlayProps);
        return c;
    }, c.removeSound = function(a, d) {
        if (null == c.activePlugin) return !1;
        a instanceof Object && a.src && (a = a.src);
        var e;
        if (a instanceof Object ? e = c._parseSrc(a) : (a = c._getSrcById(a).src, e = c._parsePath(a)), 
        null == e) return !1;
        a = e.src, null != d && (a = d + a);
        for (var f in c._idHash) c._idHash[f].src == a && delete c._idHash[f];
        return b.removeSrc(a), delete c._preloadHash[a], c.activePlugin.removeSound(a), 
        !0;
    }, c.removeSounds = function(a, b) {
        var c = [];
        a.path && (b ? b += a.path : b = a.path, a = a.manifest);
        for (var d = 0, e = a.length; e > d; d++) c[d] = createjs.Sound.removeSound(a[d].src, b);
        return c;
    }, c.removeAllSounds = function() {
        c._idHash = {}, c._preloadHash = {}, b.removeAll(), c.activePlugin && c.activePlugin.removeAllSounds();
    }, c.loadComplete = function(a) {
        if (!c.isReady()) return !1;
        var b = c._parsePath(a);
        return a = b ? c._getSrcById(b.src).src : c._getSrcById(a).src, void 0 == c._preloadHash[a] ? !1 : 1 == c._preloadHash[a][0];
    }, c._parsePath = function(a) {
        "string" != typeof a && (a = a.toString());
        var b = a.match(c.FILE_PATTERN);
        if (null == b) return !1;
        for (var d = b[4], e = b[5], f = c.capabilities, g = 0; !f[e]; ) if (e = c.alternateExtensions[g++], 
        g > c.alternateExtensions.length) return null;
        a = a.replace("." + b[5], "." + e);
        var h = {
            name: d,
            src: a,
            extension: e
        };
        return h;
    }, c._parseSrc = function(a) {
        var b = {
            name: void 0,
            src: void 0,
            extension: void 0
        }, d = c.capabilities;
        for (var e in a) if (a.hasOwnProperty(e) && d[e]) {
            b.src = a[e], b.extension = e;
            break;
        }
        if (!b.src) return !1;
        var f = b.src.lastIndexOf("/");
        return b.name = -1 != f ? b.src.slice(f + 1) : b.src, b;
    }, c.play = function(a, b, d, e, f, g, h, i, j) {
        var k;
        k = createjs.PlayPropsConfig.create(b instanceof Object || b instanceof createjs.PlayPropsConfig ? b : {
            interrupt: b,
            delay: d,
            offset: e,
            loop: f,
            volume: g,
            pan: h,
            startTime: i,
            duration: j
        });
        var l = c.createInstance(a, k.startTime, k.duration), m = c._playInstance(l, k);
        return m || l._playFailed(), l;
    }, c.createInstance = function(a, d, e) {
        if (!c.initializeDefaultPlugins()) return new createjs.DefaultSoundInstance(a, d, e);
        var f = c._defaultPlayPropsHash[a];
        a = c._getSrcById(a);
        var g = c._parsePath(a.src), h = null;
        return null != g && null != g.src ? (b.create(g.src), null == d && (d = a.startTime), 
        h = c.activePlugin.create(g.src, d, e || a.duration), f = f || c._defaultPlayPropsHash[g.src], 
        f && h.applyPlayProps(f)) : h = new createjs.DefaultSoundInstance(a, d, e), h.uniqueId = c._lastID++, 
        h;
    }, c.stop = function() {
        for (var a = this._instances, b = a.length; b--; ) a[b].stop();
    }, c.setVolume = function(a) {
        if (null == Number(a)) return !1;
        if (a = Math.max(0, Math.min(1, a)), c._masterVolume = a, !this.activePlugin || !this.activePlugin.setVolume || !this.activePlugin.setVolume(a)) for (var b = this._instances, d = 0, e = b.length; e > d; d++) b[d].setMasterVolume(a);
    }, c.getVolume = function() {
        return this._masterVolume;
    }, c.setMute = function(a) {
        if (null == a) return !1;
        if (this._masterMute = a, !this.activePlugin || !this.activePlugin.setMute || !this.activePlugin.setMute(a)) for (var b = this._instances, c = 0, d = b.length; d > c; c++) b[c].setMasterMute(a);
        return !0;
    }, c.getMute = function() {
        return this._masterMute;
    }, c.setDefaultPlayProps = function(a, b) {
        a = c._getSrcById(a), c._defaultPlayPropsHash[c._parsePath(a.src).src] = createjs.PlayPropsConfig.create(b);
    }, c.getDefaultPlayProps = function(a) {
        return a = c._getSrcById(a), c._defaultPlayPropsHash[c._parsePath(a.src).src];
    }, c._playInstance = function(a, b) {
        var d = c._defaultPlayPropsHash[a.src] || {};
        if (null == b.interrupt && (b.interrupt = d.interrupt || c.defaultInterruptBehavior), 
        null == b.delay && (b.delay = d.delay || 0), null == b.offset && (b.offset = a.getPosition()), 
        null == b.loop && (b.loop = a.loop), null == b.volume && (b.volume = a.volume), 
        null == b.pan && (b.pan = a.pan), 0 == b.delay) {
            var e = c._beginPlaying(a, b);
            if (!e) return !1;
        } else {
            var f = setTimeout(function() {
                c._beginPlaying(a, b);
            }, b.delay);
            a.delayTimeoutId = f;
        }
        return this._instances.push(a), !0;
    }, c._beginPlaying = function(a, c) {
        if (!b.add(a, c.interrupt)) return !1;
        var d = a._beginPlaying(c);
        if (!d) {
            var e = createjs.indexOf(this._instances, a);
            return e > -1 && this._instances.splice(e, 1), !1;
        }
        return !0;
    }, c._getSrcById = function(a) {
        return c._idHash[a] || {
            src: a
        };
    }, c._playFinished = function(a) {
        b.remove(a);
        var c = createjs.indexOf(this._instances, a);
        c > -1 && this._instances.splice(c, 1);
    }, createjs.Sound = a, b.channels = {}, b.create = function(a, c) {
        var d = b.get(a);
        return null == d ? (b.channels[a] = new b(a, c), !0) : !1;
    }, b.removeSrc = function(a) {
        var c = b.get(a);
        return null == c ? !1 : (c._removeAll(), delete b.channels[a], !0);
    }, b.removeAll = function() {
        for (var a in b.channels) b.channels[a]._removeAll();
        b.channels = {};
    }, b.add = function(a, c) {
        var d = b.get(a.src);
        return null == d ? !1 : d._add(a, c);
    }, b.remove = function(a) {
        var c = b.get(a.src);
        return null == c ? !1 : (c._remove(a), !0);
    }, b.maxPerChannel = function() {
        return d.maxDefault;
    }, b.get = function(a) {
        return b.channels[a];
    };
    var d = b.prototype;
    d.constructor = b, d.src = null, d.max = null, d.maxDefault = 100, d.length = 0, 
    d.init = function(a, b) {
        this.src = a, this.max = b || this.maxDefault, -1 == this.max && (this.max = this.maxDefault), 
        this._instances = [];
    }, d._get = function(a) {
        return this._instances[a];
    }, d._add = function(a, b) {
        return this._getSlot(b, a) ? (this._instances.push(a), this.length++, !0) : !1;
    }, d._remove = function(a) {
        var b = createjs.indexOf(this._instances, a);
        return -1 == b ? !1 : (this._instances.splice(b, 1), this.length--, !0);
    }, d._removeAll = function() {
        for (var a = this.length - 1; a >= 0; a--) this._instances[a].stop();
    }, d._getSlot = function(b) {
        var c, d;
        if (b != a.INTERRUPT_NONE && (d = this._get(0), null == d)) return !0;
        for (var e = 0, f = this.max; f > e; e++) {
            if (c = this._get(e), null == c) return !0;
            if (c.playState == a.PLAY_FINISHED || c.playState == a.PLAY_INTERRUPTED || c.playState == a.PLAY_FAILED) {
                d = c;
                break;
            }
            b != a.INTERRUPT_NONE && (b == a.INTERRUPT_EARLY && c.getPosition() < d.getPosition() || b == a.INTERRUPT_LATE && c.getPosition() > d.getPosition()) && (d = c);
        }
        return null != d ? (d._interrupt(), this._remove(d), !0) : !1;
    }, d.toString = function() {
        return "[Sound SoundChannel]";
    };
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    var a = function(a, b, c, d) {
        this.EventDispatcher_constructor(), this.src = a, this.uniqueId = -1, this.playState = null, 
        this.delayTimeoutId = null, this._volume = 1, Object.defineProperty(this, "volume", {
            get: this.getVolume,
            set: this.setVolume
        }), this._pan = 0, Object.defineProperty(this, "pan", {
            get: this.getPan,
            set: this.setPan
        }), this._startTime = Math.max(0, b || 0), Object.defineProperty(this, "startTime", {
            get: this.getStartTime,
            set: this.setStartTime
        }), this._duration = Math.max(0, c || 0), Object.defineProperty(this, "duration", {
            get: this.getDuration,
            set: this.setDuration
        }), this._playbackResource = null, Object.defineProperty(this, "playbackResource", {
            get: this.getPlaybackResource,
            set: this.setPlaybackResource
        }), d !== !1 && d !== !0 && this.setPlaybackResource(d), this._position = 0, Object.defineProperty(this, "position", {
            get: this.getPosition,
            set: this.setPosition
        }), this._loop = 0, Object.defineProperty(this, "loop", {
            get: this.getLoop,
            set: this.setLoop
        }), this._muted = !1, Object.defineProperty(this, "muted", {
            get: this.getMuted,
            set: this.setMuted
        }), this._paused = !1, Object.defineProperty(this, "paused", {
            get: this.getPaused,
            set: this.setPaused
        });
    }, b = createjs.extend(a, createjs.EventDispatcher);
    b.play = function(a, b, c, d, e, f) {
        var g;
        return g = createjs.PlayPropsConfig.create(a instanceof Object || a instanceof createjs.PlayPropsConfig ? a : {
            interrupt: a,
            delay: b,
            offset: c,
            loop: d,
            volume: e,
            pan: f
        }), this.playState == createjs.Sound.PLAY_SUCCEEDED ? (this.applyPlayProps(g), void (this._paused && this.setPaused(!1))) : (this._cleanUp(), 
        createjs.Sound._playInstance(this, g), this);
    }, b.stop = function() {
        return this._position = 0, this._paused = !1, this._handleStop(), this._cleanUp(), 
        this.playState = createjs.Sound.PLAY_FINISHED, this;
    }, b.destroy = function() {
        this._cleanUp(), this.src = null, this.playbackResource = null, this.removeAllEventListeners();
    }, b.applyPlayProps = function(a) {
        return null != a.offset && this.setPosition(a.offset), null != a.loop && this.setLoop(a.loop), 
        null != a.volume && this.setVolume(a.volume), null != a.pan && this.setPan(a.pan), 
        null != a.startTime && (this.setStartTime(a.startTime), this.setDuration(a.duration)), 
        this;
    }, b.toString = function() {
        return "[AbstractSoundInstance]";
    }, b.getPaused = function() {
        return this._paused;
    }, b.setPaused = function(a) {
        return a !== !0 && a !== !1 || this._paused == a || 1 == a && this.playState != createjs.Sound.PLAY_SUCCEEDED ? void 0 : (this._paused = a, 
        a ? this._pause() : this._resume(), clearTimeout(this.delayTimeoutId), this);
    }, b.setVolume = function(a) {
        return a == this._volume ? this : (this._volume = Math.max(0, Math.min(1, a)), this._muted || this._updateVolume(), 
        this);
    }, b.getVolume = function() {
        return this._volume;
    }, b.setMuted = function(a) {
        return a === !0 || a === !1 ? (this._muted = a, this._updateVolume(), this) : void 0;
    }, b.getMuted = function() {
        return this._muted;
    }, b.setPan = function(a) {
        return a == this._pan ? this : (this._pan = Math.max(-1, Math.min(1, a)), this._updatePan(), 
        this);
    }, b.getPan = function() {
        return this._pan;
    }, b.getPosition = function() {
        return this._paused || this.playState != createjs.Sound.PLAY_SUCCEEDED || (this._position = this._calculateCurrentPosition()), 
        this._position;
    }, b.setPosition = function(a) {
        return this._position = Math.max(0, a), this.playState == createjs.Sound.PLAY_SUCCEEDED && this._updatePosition(), 
        this;
    }, b.getStartTime = function() {
        return this._startTime;
    }, b.setStartTime = function(a) {
        return a == this._startTime ? this : (this._startTime = Math.max(0, a || 0), this._updateStartTime(), 
        this);
    }, b.getDuration = function() {
        return this._duration;
    }, b.setDuration = function(a) {
        return a == this._duration ? this : (this._duration = Math.max(0, a || 0), this._updateDuration(), 
        this);
    }, b.setPlaybackResource = function(a) {
        return this._playbackResource = a, 0 == this._duration && this._setDurationFromSource(), 
        this;
    }, b.getPlaybackResource = function() {
        return this._playbackResource;
    }, b.getLoop = function() {
        return this._loop;
    }, b.setLoop = function(a) {
        null != this._playbackResource && (0 != this._loop && 0 == a ? this._removeLooping(a) : 0 == this._loop && 0 != a && this._addLooping(a)), 
        this._loop = a;
    }, b._sendEvent = function(a) {
        var b = new createjs.Event(a);
        this.dispatchEvent(b);
    }, b._cleanUp = function() {
        clearTimeout(this.delayTimeoutId), this._handleCleanUp(), this._paused = !1, createjs.Sound._playFinished(this);
    }, b._interrupt = function() {
        this._cleanUp(), this.playState = createjs.Sound.PLAY_INTERRUPTED, this._sendEvent("interrupted");
    }, b._beginPlaying = function(a) {
        return this.setPosition(a.offset), this.setLoop(a.loop), this.setVolume(a.volume), 
        this.setPan(a.pan), null != a.startTime && (this.setStartTime(a.startTime), this.setDuration(a.duration)), 
        null != this._playbackResource && this._position < this._duration ? (this._paused = !1, 
        this._handleSoundReady(), this.playState = createjs.Sound.PLAY_SUCCEEDED, this._sendEvent("succeeded"), 
        !0) : (this._playFailed(), !1);
    }, b._playFailed = function() {
        this._cleanUp(), this.playState = createjs.Sound.PLAY_FAILED, this._sendEvent("failed");
    }, b._handleSoundComplete = function() {
        return this._position = 0, 0 != this._loop ? (this._loop--, this._handleLoop(), 
        void this._sendEvent("loop")) : (this._cleanUp(), this.playState = createjs.Sound.PLAY_FINISHED, 
        void this._sendEvent("complete"));
    }, b._handleSoundReady = function() {}, b._updateVolume = function() {}, b._updatePan = function() {}, 
    b._updateStartTime = function() {}, b._updateDuration = function() {}, b._setDurationFromSource = function() {}, 
    b._calculateCurrentPosition = function() {}, b._updatePosition = function() {}, 
    b._removeLooping = function() {}, b._addLooping = function() {}, b._pause = function() {}, 
    b._resume = function() {}, b._handleStop = function() {}, b._handleCleanUp = function() {}, 
    b._handleLoop = function() {}, createjs.AbstractSoundInstance = createjs.promote(a, "EventDispatcher"), 
    createjs.DefaultSoundInstance = createjs.AbstractSoundInstance;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    var a = function() {
        this._capabilities = null, this._loaders = {}, this._audioSources = {}, this._soundInstances = {}, 
        this._volume = 1, this._loaderClass, this._soundInstanceClass;
    }, b = a.prototype;
    a._capabilities = null, a.isSupported = function() {
        return !0;
    }, b.register = function(a) {
        var b = this._loaders[a.src];
        return b && !b.canceled ? this._loaders[a.src] : (this._audioSources[a.src] = !0, 
        this._soundInstances[a.src] = [], b = new this._loaderClass(a), b.on("complete", createjs.proxy(this._handlePreloadComplete, this)), 
        this._loaders[a.src] = b, b);
    }, b.preload = function(a) {
        a.on("error", createjs.proxy(this._handlePreloadError, this)), a.load();
    }, b.isPreloadStarted = function(a) {
        return null != this._audioSources[a];
    }, b.isPreloadComplete = function(a) {
        return !(null == this._audioSources[a] || 1 == this._audioSources[a]);
    }, b.removeSound = function(a) {
        if (this._soundInstances[a]) {
            for (var b = this._soundInstances[a].length; b--; ) {
                var c = this._soundInstances[a][b];
                c.destroy();
            }
            delete this._soundInstances[a], delete this._audioSources[a], this._loaders[a] && this._loaders[a].destroy(), 
            delete this._loaders[a];
        }
    }, b.removeAllSounds = function() {
        for (var a in this._audioSources) this.removeSound(a);
    }, b.create = function(a, b, c) {
        this.isPreloadStarted(a) || this.preload(this.register(a));
        var d = new this._soundInstanceClass(a, b, c, this._audioSources[a]);
        return this._soundInstances[a].push(d), d;
    }, b.setVolume = function(a) {
        return this._volume = a, this._updateVolume(), !0;
    }, b.getVolume = function() {
        return this._volume;
    }, b.setMute = function() {
        return this._updateVolume(), !0;
    }, b.toString = function() {
        return "[AbstractPlugin]";
    }, b._handlePreloadComplete = function(a) {
        var b = a.target.getItem().src;
        this._audioSources[b] = a.result;
        for (var c = 0, d = this._soundInstances[b].length; d > c; c++) {
            var e = this._soundInstances[b][c];
            e.setPlaybackResource(this._audioSources[b]);
        }
    }, b._handlePreloadError = function() {}, b._updateVolume = function() {}, createjs.AbstractPlugin = a;
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a) {
        this.AbstractLoader_constructor(a, !0, createjs.AbstractLoader.SOUND);
    }
    var b = createjs.extend(a, createjs.AbstractLoader);
    a.context = null, b.toString = function() {
        return "[WebAudioLoader]";
    }, b._createRequest = function() {
        this._request = new createjs.XHRRequest(this._item, !1), this._request.setResponseType("arraybuffer");
    }, b._sendComplete = function() {
        a.context.decodeAudioData(this._rawResult, createjs.proxy(this._handleAudioDecoded, this), createjs.proxy(this._sendError, this));
    }, b._handleAudioDecoded = function(a) {
        this._result = a, this.AbstractLoader__sendComplete();
    }, createjs.WebAudioLoader = createjs.promote(a, "AbstractLoader");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, d, e) {
        this.AbstractSoundInstance_constructor(a, b, d, e), this.gainNode = c.context.createGain(), 
        this.panNode = c.context.createPanner(), this.panNode.panningModel = c._panningModel, 
        this.panNode.connect(this.gainNode), this.sourceNode = null, this._soundCompleteTimeout = null, 
        this._sourceNodeNext = null, this._playbackStartTime = 0, this._endedHandler = createjs.proxy(this._handleSoundComplete, this);
    }
    var b = createjs.extend(a, createjs.AbstractSoundInstance), c = a;
    c.context = null, c.destinationNode = null, c._panningModel = "equalpower", b.destroy = function() {
        this.AbstractSoundInstance_destroy(), this.panNode.disconnect(0), this.panNode = null, 
        this.gainNode.disconnect(0), this.gainNode = null;
    }, b.toString = function() {
        return "[WebAudioSoundInstance]";
    }, b._updatePan = function() {
        this.panNode.setPosition(this._pan, 0, -.5);
    }, b._removeLooping = function() {
        this._sourceNodeNext = this._cleanUpAudioNode(this._sourceNodeNext);
    }, b._addLooping = function() {
        this.playState == createjs.Sound.PLAY_SUCCEEDED && (this._sourceNodeNext = this._createAndPlayAudioNode(this._playbackStartTime, 0));
    }, b._setDurationFromSource = function() {
        this._duration = 1e3 * this.playbackResource.duration;
    }, b._handleCleanUp = function() {
        this.sourceNode && this.playState == createjs.Sound.PLAY_SUCCEEDED && (this.sourceNode = this._cleanUpAudioNode(this.sourceNode), 
        this._sourceNodeNext = this._cleanUpAudioNode(this._sourceNodeNext)), 0 != this.gainNode.numberOfOutputs && this.gainNode.disconnect(0), 
        clearTimeout(this._soundCompleteTimeout), this._playbackStartTime = 0;
    }, b._cleanUpAudioNode = function(a) {
        return a && (a.stop(0), a.disconnect(0), a = null), a;
    }, b._handleSoundReady = function() {
        this.gainNode.connect(c.destinationNode);
        var a = .001 * this._duration, b = .001 * this._position;
        b > a && (b = a), this.sourceNode = this._createAndPlayAudioNode(c.context.currentTime - a, b), 
        this._playbackStartTime = this.sourceNode.startTime - b, this._soundCompleteTimeout = setTimeout(this._endedHandler, 1e3 * (a - b)), 
        0 != this._loop && (this._sourceNodeNext = this._createAndPlayAudioNode(this._playbackStartTime, 0));
    }, b._createAndPlayAudioNode = function(a, b) {
        var d = c.context.createBufferSource();
        d.buffer = this.playbackResource, d.connect(this.panNode);
        var e = .001 * this._duration;
        return d.startTime = a + e, d.start(d.startTime, b + .001 * this._startTime, e - b), 
        d;
    }, b._pause = function() {
        this._position = 1e3 * (c.context.currentTime - this._playbackStartTime), this.sourceNode = this._cleanUpAudioNode(this.sourceNode), 
        this._sourceNodeNext = this._cleanUpAudioNode(this._sourceNodeNext), 0 != this.gainNode.numberOfOutputs && this.gainNode.disconnect(0), 
        clearTimeout(this._soundCompleteTimeout);
    }, b._resume = function() {
        this._handleSoundReady();
    }, b._updateVolume = function() {
        var a = this._muted ? 0 : this._volume;
        a != this.gainNode.gain.value && (this.gainNode.gain.value = a);
    }, b._calculateCurrentPosition = function() {
        return 1e3 * (c.context.currentTime - this._playbackStartTime);
    }, b._updatePosition = function() {
        this.sourceNode = this._cleanUpAudioNode(this.sourceNode), this._sourceNodeNext = this._cleanUpAudioNode(this._sourceNodeNext), 
        clearTimeout(this._soundCompleteTimeout), this._paused || this._handleSoundReady();
    }, b._handleLoop = function() {
        this._cleanUpAudioNode(this.sourceNode), this.sourceNode = this._sourceNodeNext, 
        this._playbackStartTime = this.sourceNode.startTime, this._sourceNodeNext = this._createAndPlayAudioNode(this._playbackStartTime, 0), 
        this._soundCompleteTimeout = setTimeout(this._endedHandler, this._duration);
    }, b._updateDuration = function() {
        this.playState == createjs.Sound.PLAY_SUCCEEDED && (this._pause(), this._resume());
    }, createjs.WebAudioSoundInstance = createjs.promote(a, "AbstractSoundInstance");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        this.AbstractPlugin_constructor(), this._panningModel = c._panningModel, this.context = c.context, 
        this.dynamicsCompressorNode = this.context.createDynamicsCompressor(), this.dynamicsCompressorNode.connect(this.context.destination), 
        this.gainNode = this.context.createGain(), this.gainNode.connect(this.dynamicsCompressorNode), 
        createjs.WebAudioSoundInstance.destinationNode = this.gainNode, this._capabilities = c._capabilities, 
        this._loaderClass = createjs.WebAudioLoader, this._soundInstanceClass = createjs.WebAudioSoundInstance, 
        this._addPropsToClasses();
    }
    var b = createjs.extend(a, createjs.AbstractPlugin), c = a;
    c._capabilities = null, c._panningModel = "equalpower", c.context = null, c.isSupported = function() {
        var a = createjs.BrowserDetect.isIOS || createjs.BrowserDetect.isAndroid || createjs.BrowserDetect.isBlackberry;
        return "file:" != location.protocol || a || this._isFileXHRSupported() ? (c._generateCapabilities(), 
        null == c.context ? !1 : !0) : !1;
    }, c.playEmptySound = function() {
        if (null != c.context) {
            var a = c.context.createBufferSource();
            a.buffer = c.context.createBuffer(1, 1, 22050), a.connect(c.context.destination), 
            a.start(0, 0, 0);
        }
    }, c._isFileXHRSupported = function() {
        var a = !0, b = new XMLHttpRequest();
        try {
            b.open("GET", "WebAudioPluginTest.fail", !1);
        } catch (c) {
            return a = !1;
        }
        b.onerror = function() {
            a = !1;
        }, b.onload = function() {
            a = 404 == this.status || 200 == this.status || 0 == this.status && "" != this.response;
        };
        try {
            b.send();
        } catch (c) {
            a = !1;
        }
        return a;
    }, c._generateCapabilities = function() {
        if (null == c._capabilities) {
            var a = document.createElement("audio");
            if (null == a.canPlayType) return null;
            if (null == c.context) if (window.AudioContext) c.context = new AudioContext(); else {
                if (!window.webkitAudioContext) return null;
                c.context = new webkitAudioContext();
            }
            c._compatibilitySetUp(), c.playEmptySound(), c._capabilities = {
                panning: !0,
                volume: !0,
                tracks: -1
            };
            for (var b = createjs.Sound.SUPPORTED_EXTENSIONS, d = createjs.Sound.EXTENSION_MAP, e = 0, f = b.length; f > e; e++) {
                var g = b[e], h = d[g] || g;
                c._capabilities[g] = "no" != a.canPlayType("audio/" + g) && "" != a.canPlayType("audio/" + g) || "no" != a.canPlayType("audio/" + h) && "" != a.canPlayType("audio/" + h);
            }
            c.context.destination.numberOfChannels < 2 && (c._capabilities.panning = !1);
        }
    }, c._compatibilitySetUp = function() {
        if (c._panningModel = "equalpower", !c.context.createGain) {
            c.context.createGain = c.context.createGainNode;
            var a = c.context.createBufferSource();
            a.__proto__.start = a.__proto__.noteGrainOn, a.__proto__.stop = a.__proto__.noteOff, 
            c._panningModel = 0;
        }
    }, b.toString = function() {
        return "[WebAudioPlugin]";
    }, b._addPropsToClasses = function() {
        var a = this._soundInstanceClass;
        a.context = this.context, a.destinationNode = this.gainNode, a._panningModel = this._panningModel, 
        this._loaderClass.context = this.context;
    }, b._updateVolume = function() {
        var a = createjs.Sound._masterMute ? 0 : this._volume;
        a != this.gainNode.gain.value && (this.gainNode.gain.value = a);
    }, createjs.WebAudioPlugin = createjs.promote(a, "AbstractPlugin");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        throw "HTMLAudioTagPool cannot be instantiated";
    }
    function b() {
        this._tags = [];
    }
    var c = a;
    c._tags = {}, c._tagPool = new b(), c._tagUsed = {}, c.get = function(a) {
        var b = c._tags[a];
        return null == b ? (b = c._tags[a] = c._tagPool.get(), b.src = a) : c._tagUsed[a] ? (b = c._tagPool.get(), 
        b.src = a) : c._tagUsed[a] = !0, b;
    }, c.set = function(a, b) {
        b == c._tags[a] ? c._tagUsed[a] = !1 : c._tagPool.set(b);
    }, c.remove = function(a) {
        var b = c._tags[a];
        return null == b ? !1 : (c._tagPool.set(b), delete c._tags[a], delete c._tagUsed[a], 
        !0);
    }, c.getDuration = function(a) {
        var b = c._tags[a];
        return null == b ? 0 : 1e3 * b.duration;
    }, createjs.HTMLAudioTagPool = a;
    var d = b.prototype;
    d.constructor = b, d.get = function() {
        var a;
        return a = 0 == this._tags.length ? this._createTag() : this._tags.pop(), null == a.parentNode && document.body.appendChild(a), 
        a;
    }, d.set = function(a) {
        var b = createjs.indexOf(this._tags, a);
        -1 == b && (this._tags.src = null, this._tags.push(a));
    }, d.toString = function() {
        return "[TagPool]";
    }, d._createTag = function() {
        var a = document.createElement("audio");
        return a.autoplay = !1, a.preload = "none", a;
    };
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a(a, b, c, d) {
        this.AbstractSoundInstance_constructor(a, b, c, d), this._audioSpriteStopTime = null, 
        this._delayTimeoutId = null, this._endedHandler = createjs.proxy(this._handleSoundComplete, this), 
        this._readyHandler = createjs.proxy(this._handleTagReady, this), this._stalledHandler = createjs.proxy(this._playFailed, this), 
        this._audioSpriteEndHandler = createjs.proxy(this._handleAudioSpriteLoop, this), 
        this._loopHandler = createjs.proxy(this._handleSoundComplete, this), c ? this._audioSpriteStopTime = .001 * (b + c) : this._duration = createjs.HTMLAudioTagPool.getDuration(this.src);
    }
    var b = createjs.extend(a, createjs.AbstractSoundInstance);
    b.setMasterVolume = function() {
        this._updateVolume();
    }, b.setMasterMute = function() {
        this._updateVolume();
    }, b.toString = function() {
        return "[HTMLAudioSoundInstance]";
    }, b._removeLooping = function() {
        null != this._playbackResource && (this._playbackResource.loop = !1, this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1));
    }, b._addLooping = function() {
        null == this._playbackResource || this._audioSpriteStopTime || (this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1), 
        this._playbackResource.loop = !0);
    }, b._handleCleanUp = function() {
        var a = this._playbackResource;
        if (null != a) {
            a.pause(), a.loop = !1, a.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_ENDED, this._endedHandler, !1), 
            a.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_READY, this._readyHandler, !1), 
            a.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_STALLED, this._stalledHandler, !1), 
            a.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1), 
            a.removeEventListener(createjs.HTMLAudioPlugin._TIME_UPDATE, this._audioSpriteEndHandler, !1);
            try {
                a.currentTime = this._startTime;
            } catch (b) {}
            createjs.HTMLAudioTagPool.set(this.src, a), this._playbackResource = null;
        }
    }, b._beginPlaying = function(a) {
        return this._playbackResource = createjs.HTMLAudioTagPool.get(this.src), this.AbstractSoundInstance__beginPlaying(a);
    }, b._handleSoundReady = function() {
        if (4 !== this._playbackResource.readyState) {
            var a = this._playbackResource;
            return a.addEventListener(createjs.HTMLAudioPlugin._AUDIO_READY, this._readyHandler, !1), 
            a.addEventListener(createjs.HTMLAudioPlugin._AUDIO_STALLED, this._stalledHandler, !1), 
            a.preload = "auto", void a.load();
        }
        this._updateVolume(), this._playbackResource.currentTime = .001 * (this._startTime + this._position), 
        this._audioSpriteStopTime ? this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._TIME_UPDATE, this._audioSpriteEndHandler, !1) : (this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._AUDIO_ENDED, this._endedHandler, !1), 
        0 != this._loop && (this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1), 
        this._playbackResource.loop = !0)), this._playbackResource.play();
    }, b._handleTagReady = function() {
        this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_READY, this._readyHandler, !1), 
        this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_STALLED, this._stalledHandler, !1), 
        this._handleSoundReady();
    }, b._pause = function() {
        this._playbackResource.pause();
    }, b._resume = function() {
        this._playbackResource.play();
    }, b._updateVolume = function() {
        if (null != this._playbackResource) {
            var a = this._muted || createjs.Sound._masterMute ? 0 : this._volume * createjs.Sound._masterVolume;
            a != this._playbackResource.volume && (this._playbackResource.volume = a);
        }
    }, b._calculateCurrentPosition = function() {
        return 1e3 * this._playbackResource.currentTime - this._startTime;
    }, b._updatePosition = function() {
        this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1), 
        this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._handleSetPositionSeek, !1);
        try {
            this._playbackResource.currentTime = .001 * (this._position + this._startTime);
        } catch (a) {
            this._handleSetPositionSeek(null);
        }
    }, b._handleSetPositionSeek = function() {
        null != this._playbackResource && (this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._handleSetPositionSeek, !1), 
        this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1));
    }, b._handleAudioSpriteLoop = function() {
        this._playbackResource.currentTime <= this._audioSpriteStopTime || (this._playbackResource.pause(), 
        0 == this._loop ? this._handleSoundComplete(null) : (this._position = 0, this._loop--, 
        this._playbackResource.currentTime = .001 * this._startTime, this._paused || this._playbackResource.play(), 
        this._sendEvent("loop")));
    }, b._handleLoop = function() {
        0 == this._loop && (this._playbackResource.loop = !1, this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_SEEKED, this._loopHandler, !1));
    }, b._updateStartTime = function() {
        this._audioSpriteStopTime = .001 * (this._startTime + this._duration), this.playState == createjs.Sound.PLAY_SUCCEEDED && (this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_ENDED, this._endedHandler, !1), 
        this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._TIME_UPDATE, this._audioSpriteEndHandler, !1));
    }, b._updateDuration = function() {
        this._audioSpriteStopTime = .001 * (this._startTime + this._duration), this.playState == createjs.Sound.PLAY_SUCCEEDED && (this._playbackResource.removeEventListener(createjs.HTMLAudioPlugin._AUDIO_ENDED, this._endedHandler, !1), 
        this._playbackResource.addEventListener(createjs.HTMLAudioPlugin._TIME_UPDATE, this._audioSpriteEndHandler, !1));
    }, createjs.HTMLAudioSoundInstance = createjs.promote(a, "AbstractSoundInstance");
}(), this.createjs = this.createjs || {}, function() {
    "use strict";
    function a() {
        this.AbstractPlugin_constructor(), this.defaultNumChannels = 2, this._capabilities = c._capabilities, 
        this._loaderClass = createjs.SoundLoader, this._soundInstanceClass = createjs.HTMLAudioSoundInstance;
    }
    var b = createjs.extend(a, createjs.AbstractPlugin), c = a;
    c.MAX_INSTANCES = 30, c._AUDIO_READY = "canplaythrough", c._AUDIO_ENDED = "ended", 
    c._AUDIO_SEEKED = "seeked", c._AUDIO_STALLED = "stalled", c._TIME_UPDATE = "timeupdate", 
    c._capabilities = null, c.isSupported = function() {
        return c._generateCapabilities(), null != c._capabilities;
    }, c._generateCapabilities = function() {
        if (null == c._capabilities) {
            var a = document.createElement("audio");
            if (null == a.canPlayType) return null;
            c._capabilities = {
                panning: !1,
                volume: !0,
                tracks: -1
            };
            for (var b = createjs.Sound.SUPPORTED_EXTENSIONS, d = createjs.Sound.EXTENSION_MAP, e = 0, f = b.length; f > e; e++) {
                var g = b[e], h = d[g] || g;
                c._capabilities[g] = "no" != a.canPlayType("audio/" + g) && "" != a.canPlayType("audio/" + g) || "no" != a.canPlayType("audio/" + h) && "" != a.canPlayType("audio/" + h);
            }
        }
    }, b.register = function(a) {
        var b = createjs.HTMLAudioTagPool.get(a.src), c = this.AbstractPlugin_register(a);
        return c.setTag(b), c;
    }, b.removeSound = function(a) {
        this.AbstractPlugin_removeSound(a), createjs.HTMLAudioTagPool.remove(a);
    }, b.create = function(a, b, c) {
        var d = this.AbstractPlugin_create(a, b, c);
        return d.setPlaybackResource(null), d;
    }, b.toString = function() {
        return "[HTMLAudioPlugin]";
    }, b.setVolume = b.getVolume = b.setMute = null, createjs.HTMLAudioPlugin = createjs.promote(a, "AbstractPlugin");
}();
//# sourceMappingURL=packages.js.map