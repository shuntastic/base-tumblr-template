var settings = {

    dates: [
        {
            date: '2015-11-25',
            el: '.theaterStatus h2.releaseStatus, .overwrapper .theaterStatus .releaseStatus',
            label: '<span>Now Playing</span>',
            style: {
                'font-size': '18px'
            },
            mobileStyle: {
                'font-size': '16px'
            },
            visible:true
        },
        {
            date: '2015-11-24',
            el: '.theaterStatus h2.releaseStatus, .overwrapper .theaterStatus .releaseStatus',
            label: '<span>Tomorrow</span>',
            style: {
                'font-size': '22px'
            },
            mobileStyle: {
                'font-size': '20px'
            },
            visible:true
        },
        {
            date: 'default',
            el: '.theaterStatus h2.releaseStatus, .overwrapper .theaterStatus .releaseStatus',
            label: '<span>Thanksgiving</span>',
            style: '',
            visible:true
        }
    ],
    desktopPreloadFiles: [
        'images/framework/title_lockup.png',
        'sounds/loop.ogg',
        'images/main/1970sBG.jpg',
        'images/main/1980sBG.jpg',
        'images/main/1990sBG.jpg',
        'images/main/2000sBG.jpg',
        'images/main/vertical_1970sBG.jpg',
        'images/main/vertical_1980sBG.jpg',
        'images/main/vertical_1990sBG.jpg',
        'images/main/vertical_2000sBG.jpg'
    ],
    modalText: [
        {
            el:'.preText h1', 
            label:'It’s not the streets you’re from. It’s the road you take',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        {
            el:'.preText h2', 
            label:'that defines you.',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        {
            el:'.preText p', 
            label:'Follow the epic saga of Rocky Balboa from his days on the streets of Philadelphia to the heights of glory in and out of the ring to his new role as mentor to Adonis, the son of his greatest opponent and friend, Apollo Creed.<br /><br />Relive all the heart-pounding moments of Rocky’s legacy and explore the Timeline of Greatness to see photos, GIFs and videos from this incredible journey of courage, loyalty and passion.',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        {
            el:'.preload-browser .cta',
            label:'<span class="desktop-only">Click</span><span class="mobile-only">Tap</span> To Explore The Timeline',
            style: {
                'font-size': 'default'
            },
            visible: true
        }
    ],
    clickPrompt:'<span class="desktop-only">Click</span><span class="mobile-only">Tap</span> to Explore ', /*PROMPT FOR OVERLAY LAUNCH BUTTON (DIAMOND WITH PLUS)*/
    landscapeModePrompt:[{
        el:'.landscape-content h1.prompt', 
        label:'please rotate your device<br />for a knockout experience',
        style: {
            'font-size': 'default'
        },
        visible: true

    }
    ],
    topMenu: [
        {
            el:'#topMenu .connect', 
            label:'connect',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        {
            el:'#topMenu .share', 
            label:'share',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        {
            el:'#topMenu .dashboard', 
            label:'dashboard',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        {
            el:'#topMenu .follow', 
            label:'follow',
            selected_label:'unfollow',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
    ],
    quote:{
        quote_2000:{
            line1:'<span class="open-quote">“</span><span class="highlight">YOU GOT SPECIAL GENES,</span><br /><span class="lowlight">but that don’t mean you’re special yet.</span=><span class="closed-quote">”</span>',
            byline:'Rocky Balboa',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        quote_1990:{
            line1:'<span class="open-quote">“</span><span class="lowlight">The difference between a hero and a coward<br />is the hero’s willing</span> <span class="highlight">to go for it.</span><span class="closed-quote">”</span>',
            byline:'Rocky Balboa',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        quote_1980:{
            line1:'<span class="open-quote">“</span><span class="lowlight">if he dies...</span><span class="highlight">he dies.</span><span class="closed-quote">”</span>',
            byline:'Ivan Drago',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        quote_1970:{
            line1:'<span class="open-quote">“</span><span class="lowlight">there ain’t gonna be</span><br /><span class="highlight">no rematch!</span><span class="closed-quote">”</span>',
            byline:'Apollo Creed',
            style: {
                'font-size': 'default'
            },
            visible: true
        }
    },
    mainMenu:[
            {
                el:'.decadeMenu .decNav1970',
                label:'1970s',
                label_mobile:'70s',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'.decadeMenu .decNav1980',
                label:'1980s',
                label_mobile:'80s',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'.decadeMenu .decNav1990',
                label:'1990s',
                label_mobile:'90s',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'.decadeMenu .decNav2000',
                label:'2000s',
                label_mobile:'00s',
                style: {
                    'font-size': 'default'
                },
                visible: true
            }
    ],
    overlaySocial: [
    {
        el:'.mediaactions .icon-icon_like .customLike',
        label:'like',
        style: {
            'font-size': 'default'
        },
        visible: true
    }
    ],
    socialMenu: {
        visible: true,
        links: [
            {
                el: '#connectDrawer .icon-instagram',
                properties: {
                    id: 'instagram'
                },
                url: 'https://instagram.com/creedmovie/',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el: '#connectDrawer .icon-fb',
                properties: {
                    id: 'facebook'
                },
                url: 'https://www.facebook.com/creedmovie',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el: '#connectDrawer .icon-twitter',
                properties: {
                    id: 'twitter'
                },
                url: 'https://twitter.com/creedmovie',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el: '#connectDrawer .icon-g',
                properties: {
                    id: 'google-plus',
                    rel: 'publisher'
                },
                url: 'https://plus.google.com/',
                // url: 'https://plus.google.com/u/1/b/103532667227475039390/103532667227475039390/',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el: '#connectDrawer .icon-pinterest',
                properties: {
                    id: 'pinterest'
                },
                url: 'http://www.pinterest.com/creedmovie',
                style: {
                    'font-size': 'default'
                },
                visible: true
            }
        ]
    },
    filterTitle: [
            {
                el:'.overlayMenu h2',
                label:'Filter By:',
                style:{
                    'font-size':'default'
                },
                visible:true
            }
    ],
    filterMenu: [
            {
                el:'.filterby.image',
                label:'IMAGES',
                selected_label:'ALL IMAGES',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'.filterby.gifs',
                label:'GIFS',
                selected_label:'ALL GIFS',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'.filterby.video',
                label:'VIDEOS',
                selected_label:'ALL VIDEOS',
                style: {
                    'font-size': 'default'
                },
                visible: true
            }
    ],

    shareMenu: {
        visible: true,
        openButton: {
        id: 'shareOpenBtn',
        label: 'SHARE',
            style: {
                'font-size': 'default'
            },
            visible: true
        },
        facebook: {
            visible: true,
            // NO LINK IN THE SETTINGS - IT IS SET VIA THE TUMBLR THEME
        },
        twitter: {
            visible: true
            // NO LINK IN THE SETTINGS - IT IS SET VIA THE TUMBLR THEME
        },
        google: {
            visible: true
            // NO LINK IN THE SETTINGS - IT IS SET VIA THE TUMBLR THEME
        }
    },
    footerMenu: {
        promo: [
        {
            el:'.promo a',
            label: 'Buy the complete Rocky™ Collection. Now Available on Blu-Ray™, DVD and all digital platforms.',
            url:'http://apple.co/1M7ZNT5',
            style: {
                'font-size': 'default'
            },
            visible: true
        }
        ],
        ticketsBtn: [
            {
                el:'.ticketsCTA',
                label: 'GET TICKETS',
                url: 'http://www.fandango.com/creed_181117/movieoverview',
                style: {
                    'font-size': 'default'
                },
                visible: true
            }
        ],
        legalBtn: [
            {
                el:'.legalToggle a',
                label:'legal',
                url:'',
                style: {
                    'font-size': 'default'
                },
                visible: true
            }
        ]
    },


    legal: {

        legalLine: [
        {
            el:'#legalBtns .copyright',
            label:'ROCKY™ IS A REGISTERED TRADEMARK OF METRO-GOLDWYN-MAYER STUDIOS INC. ALL RIGHTS RESERVED.<br />&copy; 2015 Metro-Goldwyn-Mayer Pictures Inc. and Warner Bros. Entertainment Inc. All Rights Reserved',
            style: {
                'font-size': 'default'
            },
            visible: true
        }
        ],
        legalLinks:[
            {
                el:'#legalBtns .privacy',
                label:'Privacy Policy',
                url:'http://www.warnerbros.com/privacy/policy.html',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },

            {
                el:'#legalBtns .terms',
                label:'Terms of Use',
                url:'http://www.warnerbros.com/privacy/terms.html',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'#legalBtns .adchoices',
                label:'Ad Choices',
                url:'http://www.warnerbros.com/privacy-center-wb-privacy-policy#p5',
            style: {
                'font-size': 'default'
            },
            visible: true
            },

            {
                el:'#legalBtns .filmratings',
                label:'Film Ratings',
                url:'http://www.filmratings.com',
            style: {
                'font-size': 'default'
            },
            visible: true
            },

            {
                el:'#legalBtns .parentalguide',
                label:'Parental Guide',
                url:'http://www.parentalguide.org',
            style: {
                'font-size': 'default'
            },
            visible: true
            },

            {
                el:'#legalBtns .mpaa',
                label:'MPAA',
                url:'http://www.mpaa.org',
            style: {
                'font-size': 'default'
            },
            visible: true
            }
        ],
        rating: [
            {
                visible: true,
                src: 'images/framework/billing.png',
                popupSrc: 'images/framework/rating.png'
            }
        ],
        legalLinksUK:[
            {
                el:'#legalBtns .privacy',
                label:'Privacy Policy',
                url:'http://www.warnerbros.co.uk/#/privacy',
                style: {
                    'font-size': 'default'
                },
                visible: true
            },
            {
                el:'#legalBtns .terms',
                label:'Terms of Use',
                url:'http://www.warnerbros.co.uk/#/terms',
                style: {
                    'font-size': 'default'
                },
                visible: true
            }
        ]

    },


    /* video types: 'youtube' or 'videofile'
     * for youtube videos the link should be the youtube video id
     * for nonyoutube videos the link should be a full path to the video and
     * should have both a mp4 & webm video named the same. Use the mp4 in the link name
    */
    videos: {
        links: [
            // {
            //     label: 'TRAILER 2',
            //     videoType: 'youtube',
            //     link: 'f6dKhzYgksc',
            //     visible: true
            // },
        ]
    },


    /* locations for sharing --------------------------------- */
    shareUrls: {
        facebook: 'http://www.facebook.com/sharer/sharer.php?u=%URL%',
        twitter: 'https://twitter.com/intent/tweet?text=Experience the #RockyLegacy at http://rockylegacy.creedthemovie.com&url=%URL%',
        google: 'https://plus.google.com/share?url=%IMAGE_URL%',
        pinterest: 'http://www.pinterest.com/pin/create/button/?description=%DESC%&url=%URL%&media=%IMAGE_URL%',
        tumblr: 'http://www.tumblr.com/share/photo?source=%IMAGE_URL%&caption=%DESC%&click_thru=%URL%'
    },
    /* Sounds ---------------------------------- */

    soundPath: presets.assetPath+'sounds/',
    sounds: [
        {
            "id": "music",
            "src": "loop.ogg",
            "data": 1
        }
    ],
    /* Upgrade page notice ------------------------ */
    upgrade: 'Please upgrade your browser to enjoy this site.'
};


